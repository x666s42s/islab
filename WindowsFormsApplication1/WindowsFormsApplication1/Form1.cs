﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Configuration;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {

        string currentDate = DateTime.Now.ToString("yyyy/MM/dd");

        public Form1()
        {
            InitializeComponent();

            getMSCI();
            getfgbuy();
            getfgsell();

            

        }


        private string GetWebContent(string Url)

        {

            string strResult = "";

            try

            {

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);

                //聲明一個HttpWebRequest請求

                request.Timeout = 30000;

                //設置連接逾時時間

                request.Headers.Set("Accept-Language", "zh-TW");

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                Stream streamReceive = response.GetResponseStream();

                Encoding encoding = Encoding.GetEncoding("utf-8");

                StreamReader streamReader = new StreamReader(streamReceive, encoding);

                strResult = streamReader.ReadToEnd();

            }

            catch

            {

                MessageBox.Show("出錯"); //HTTP://ike.126.com

            }

            return strResult;

        }
        private string GetWebContentBig5(string Url)

        {

            string strResult = "";

            try

            {

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);

                //聲明一個HttpWebRequest請求

                request.Timeout = 30000;

                //設置連接逾時時間

                request.Headers.Set("Accept-Language", "zh-TW");

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                Stream streamReceive = response.GetResponseStream();

                Encoding encoding = Encoding.GetEncoding("big5");

                StreamReader streamReader = new StreamReader(streamReceive, encoding);

                strResult = streamReader.ReadToEnd();

            }

            catch

            {

                MessageBox.Show("出錯"); //HTTP://ike.126.com

            }

            return strResult;

        }
        private void button1_Click(object sender, EventArgs e)
        {
            DateTime time_start = DateTime.Now;

            getMSCI();
            getfgbuy();
            getfgsell();

            //textBox2.Text = Convert.ToDateTime(currentDate).AddDays(-5).ToString("yyyy-MM-dd");

            DateTime time_end = DateTime.Now;

            textBox2.Text = (time_end - time_start).ToString("%s") + "秒";

            //textBox1.Text = count.ToString();
        }

        private Boolean checkAnnoDominiDate(string webDate)
        {
            String year = currentDate.Substring(0, 4);
            String month = currentDate.Substring(5, 2);
            String day = currentDate.Substring(8, 2);

            Boolean result = webDate == year+"-"+month+"-"+day;
            return result;
        }

        private Boolean checkRepublicEraDate(string webDate)
        {
            String year = (Convert.ToInt32(currentDate.Substring(0, 4)) - 1911).ToString();
            String month = currentDate.Substring(5, 2);
            String day = currentDate.Substring(8, 2);

            Boolean result = webDate == year + "/" + month + "/" + day;
            return result;
        }

        private void getMSCI()
        {
            //要抓取的URL位址

            string Url = "https://histock.tw/stock/mscitaiwan.aspx";


            string strWebContent = GetWebContent(Url);

            int iBodyStart = strWebContent.IndexOf("<body", 0);
            int iStart = strWebContent.IndexOf("更新日期", iBodyStart);
            int iEnd = strWebContent.IndexOf("MSCI摩根台指成分股權重表",iStart);
            int iTableStart = strWebContent.IndexOf("<table", iEnd);
            int iTableEnd = strWebContent.IndexOf("</table>", iTableStart);

            string updateDate = strWebContent.Substring(iStart + 5, iEnd - iStart- 16);


            if (checkAnnoDominiDate(updateDate))
            {
                string strWeb = strWebContent.Substring(iTableStart, iTableEnd - iTableStart + 8);

                WebBrowser webb = new WebBrowser();

                webb.Navigate("about:blank");

                HtmlDocument htmldoc = webb.Document.OpenNew(true);

                htmldoc.Write(strWeb);

                HtmlElementCollection htmlTR = htmldoc.GetElementsByTagName("tr");

                int count = 0;

                ArrayList tempData = new ArrayList();

                DataTable msciDt = new DataTable();

                DataRow msciRow;

                msciDt.Columns.Add("MSCI_ID", typeof(String));

                msciDt.Columns.Add("Date", typeof(String));

                msciDt.Columns.Add("StockID", typeof(Int32));

                msciDt.Columns.Add("StockName", typeof(String));
                msciDt.Columns.Add("StockPrice", typeof(Double));
                msciDt.Columns.Add("UpAndDown", typeof(String));
                msciDt.Columns.Add("UADRate", typeof(String));
                msciDt.Columns.Add("Price", typeof(String));
                msciDt.Columns.Add("Weight", typeof(Double));


                foreach (HtmlElement tr in htmlTR)
                {
                    HtmlElementCollection htmlTD = tr.GetElementsByTagName("TD");

                    foreach (HtmlElement td in htmlTD)
                    {
                        tempData.Add(td.InnerText);
                    }

                    if (count > 0)
                    {
                        msciRow = msciDt.NewRow();
                        msciRow["MSCI_ID"] = currentDate + tempData[0];
                        msciRow["Date"] = currentDate;
                        msciRow["StockID"] = tempData[0];
                        msciRow["StockName"] = tempData[1];
                        msciRow["StockPrice"] = tempData[2];
                        msciRow["UpAndDown"] = tempData[3];
                        msciRow["UADRate"] = tempData[4];
                        msciRow["Price"] = tempData[5];
                        msciRow["Weight"] = tempData[6];

                        msciDt.Rows.Add(msciRow);

                        //textBox1.Text += currentDate + " " +tempData[0] + " " + tempData[1] + " " + tempData[2] + " " + tempData[3] + " " + tempData[4] + " " + tempData[5] + " " + tempData[6] + Environment.NewLine;
                    }

                    count++;
                    tempData.Clear();
                    //string stockNO = tr.GetElementsByTagName("td")[0].InnerText;
                    //string stockName = tr.GetElementsByTagName("td")[1].InnerText;
                    //string stockPrice = tr.GetElementsByTagName("td")[2].InnerText;
                    //string stockUpAndDown = tr.GetElementsByTagName("td")[3].InnerText;
                    //string stockUpAndDownRate = tr.GetElementsByTagName("td")[4].InnerText;
                    //string Price = tr.GetElementsByTagName("td")[5].InnerText;
                    //string Weights = tr.GetElementsByTagName("td")[6].InnerText;

                    //textBox1.Text += stockNO + " " + stockName + " " + stockPrice + " " + stockUpAndDown + " " + stockUpAndDownRate + " " + Price + " " + Weights;


                }

                if (msciDt.Rows.Count > 0)
                {
                    string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
                    using (SqlConnection con = new SqlConnection(consString))
                    {
                        using (SqlCommand cmd = new SqlCommand("Insert_MSCI"))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Connection = con;
                            cmd.Parameters.AddWithValue("@insMSCI", msciDt);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                }

                dataGridView1.DataSource = msciDt;

            }

            
        }

        private void getfgbuy()
        {
            string Url = "https://tw.stock.yahoo.com/d/i/fgbuy_tse.html";

            string strWebContent = GetWebContentBig5(Url);

            //int iBodyStart = strWebContent.IndexOf("<body", 0);
            int iStart = strWebContent.IndexOf("content start", 0);

            int iTableStart = strWebContent.IndexOf("<table", iStart);
            int iTableEnd = strWebContent.IndexOf("</table>", iStart);

            int iEnd = strWebContent.IndexOf("content end", iStart);

            int iLastTableStart = strWebContent.IndexOf("<table", iTableEnd);
            int iLastTableEnd = strWebContent.IndexOf("</table>", iLastTableStart);

            int iLLastTableStart = strWebContent.IndexOf("<table", iLastTableEnd);
            int iLLastTableEnd = strWebContent.IndexOf("</table>", iLLastTableStart);

            int iDateStart = strWebContent.IndexOf("資料日期：", iStart);
            int iDateEnd = strWebContent.IndexOf("</td>", iDateStart);

            string updateDate = strWebContent.Substring(iDateStart + 5, iDateEnd - iDateStart - 5);



            //textBox2.Text = checkRepublicEraDate(updateDate).ToString();

            if (checkRepublicEraDate(updateDate))
            {
                string strWeb = strWebContent.Substring(iLLastTableStart, iLLastTableEnd - iLLastTableStart);


                WebBrowser webb = new WebBrowser();

                webb.Navigate("about:blank");

                HtmlDocument htmldoc = webb.Document.OpenNew(true);

                htmldoc.Write(strWeb);

                HtmlElementCollection htmlTR = htmldoc.GetElementsByTagName("tr");

                int count = 0;

                ArrayList tempData = new ArrayList();

                DataTable fgbuyDt = new DataTable();

                DataRow fgbuyRow;

                fgbuyDt.Columns.Add("fgbuy_ID", typeof(String));
                fgbuyDt.Columns.Add("Date", typeof(String));
                fgbuyDt.Columns.Add("Rank", typeof(String));
                fgbuyDt.Columns.Add("StockID", typeof(String));
                fgbuyDt.Columns.Add("StockName", typeof(String));
                fgbuyDt.Columns.Add("StrikePrice", typeof(String));
                fgbuyDt.Columns.Add("UpAndDown", typeof(String));
                fgbuyDt.Columns.Add("OverboughtCount", typeof(String));
                fgbuyDt.Columns.Add("ForeignHoldingCount", typeof(String));
                fgbuyDt.Columns.Add("ForeignHoldingWeight", typeof(Double));

                foreach (HtmlElement tr in htmlTR)
                {
                    HtmlElementCollection htmlTD = tr.GetElementsByTagName("TD");

                    foreach (HtmlElement td in htmlTD)
                    {
                        tempData.Add(td.InnerText);
                    }
                    if (count > 0)
                    {

                        fgbuyRow = fgbuyDt.NewRow();
                        fgbuyRow["fgbuy_ID"] = currentDate + tempData[1].ToString().Substring(0, 4);
                        fgbuyRow["Date"] = currentDate;
                        fgbuyRow["Rank"] = tempData[0];
                        fgbuyRow["StockID"] = tempData[1].ToString().Substring(0, 4);
                        fgbuyRow["StockName"] = tempData[1].ToString().Substring(4);
                        fgbuyRow["StrikePrice"] = tempData[2];
                        fgbuyRow["UpAndDown"] = tempData[3];
                        fgbuyRow["OverboughtCount"] = tempData[4];
                        fgbuyRow["ForeignHoldingCount"] = tempData[5];
                        fgbuyRow["ForeignHoldingWeight"] = tempData[6].ToString().Substring(0, tempData[6].ToString().Length - 1);

                        fgbuyDt.Rows.Add(fgbuyRow);

                    }


                    count++;
                    tempData.Clear();

                }

                if (fgbuyDt.Rows.Count > 0)
                {
                    string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
                    using (SqlConnection con = new SqlConnection(consString))
                    {
                        using (SqlCommand cmd = new SqlCommand("Insert_FGBuy"))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Connection = con;
                            cmd.Parameters.AddWithValue("@insFGBuy", fgbuyDt);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                }

                dataGridView2.DataSource = fgbuyDt;

            }


        }

        private void getfgsell()
        {
            string Url = "https://tw.stock.yahoo.com/d/i/fgsell_tse.html";

            string strWebContent = GetWebContentBig5(Url);

            //int iBodyStart = strWebContent.IndexOf("<body", 0);
            int iStart = strWebContent.IndexOf("content start", 0);

            int iTableStart = strWebContent.IndexOf("<table", iStart);
            int iTableEnd = strWebContent.IndexOf("</table>", iStart);

            int iEnd = strWebContent.IndexOf("content end", iStart);

            int iLastTableStart = strWebContent.IndexOf("<table", iTableEnd);
            int iLastTableEnd = strWebContent.IndexOf("</table>", iLastTableStart);

            int iLLastTableStart = strWebContent.IndexOf("<table", iLastTableEnd);
            int iLLastTableEnd = strWebContent.IndexOf("</table>", iLLastTableStart);

            int iDateStart = strWebContent.IndexOf("資料日期：", iStart);
            int iDateEnd = strWebContent.IndexOf("</td>", iDateStart);

            string updateDate = strWebContent.Substring(iDateStart + 5, iDateEnd - iDateStart - 5);

            //textBox2.Text = checkRepublicEraDate(updateDate).ToString();

            if (checkRepublicEraDate(updateDate))
            {
                string strWeb = strWebContent.Substring(iLLastTableStart, iLLastTableEnd - iLLastTableStart);


                WebBrowser webb = new WebBrowser();

                webb.Navigate("about:blank");

                HtmlDocument htmldoc = webb.Document.OpenNew(true);

                htmldoc.Write(strWeb);

                HtmlElementCollection htmlTR = htmldoc.GetElementsByTagName("tr");

                int count = 0;

                ArrayList tempData = new ArrayList();

                DataTable fgsellDt = new DataTable();

                DataRow fgsellRow;

                fgsellDt.Columns.Add("fgsell_ID", typeof(String));
                fgsellDt.Columns.Add("Date", typeof(String));
                fgsellDt.Columns.Add("Rank", typeof(String));
                fgsellDt.Columns.Add("StockID", typeof(String));
                fgsellDt.Columns.Add("StockName", typeof(String));
                fgsellDt.Columns.Add("StrikePrice", typeof(String));
                fgsellDt.Columns.Add("UpAndDown", typeof(String));
                fgsellDt.Columns.Add("OversoldCount", typeof(String));
                fgsellDt.Columns.Add("ForeignHoldingCount", typeof(String));
                fgsellDt.Columns.Add("ForeignHoldingWeight", typeof(Double));

                foreach (HtmlElement tr in htmlTR)
                {
                    HtmlElementCollection htmlTD = tr.GetElementsByTagName("TD");

                    foreach (HtmlElement td in htmlTD)
                    {
                        tempData.Add(td.InnerText);
                    }
                    if (count > 0)
                    {
                        fgsellRow = fgsellDt.NewRow();
                        fgsellRow["fgsell_ID"] = currentDate + tempData[1].ToString().Substring(0, 4);
                        fgsellRow["Date"] = currentDate;
                        fgsellRow["Rank"] = tempData[0];
                        fgsellRow["StockID"] = tempData[1].ToString().Substring(0, 4);
                        fgsellRow["StockName"] = tempData[1].ToString().Substring(4);
                        fgsellRow["StrikePrice"] = tempData[2];
                        fgsellRow["UpAndDown"] = tempData[3];
                        fgsellRow["OversoldCount"] = tempData[4];
                        fgsellRow["ForeignHoldingCount"] = tempData[5];
                        fgsellRow["ForeignHoldingWeight"] = tempData[6].ToString().Substring(0, tempData[6].ToString().Length - 1);

                        fgsellDt.Rows.Add(fgsellRow);
                    }


                    count++;
                    tempData.Clear();

                }

                if (fgsellDt.Rows.Count > 0)
                {
                    string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
                    using (SqlConnection con = new SqlConnection(consString))
                    {
                        using (SqlCommand cmd = new SqlCommand("Insert_FGSell"))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Connection = con;
                            cmd.Parameters.AddWithValue("@insFGSell", fgsellDt);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                }

                dataGridView3.DataSource = fgsellDt;

            }

            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string Url = "http://www.cnyes.com/twstock/index2real.aspx?PageIndex=1&amp;itype=TW50&amp;stitle=%u81fa%u7063%u4e94%u5341%u6307%u6578%u6210%u4efd%u80a1";


            string strWebContent = GetWebContent(Url);

            int iBodyStart = strWebContent.IndexOf("<body", 0);
            int iStart = strWebContent.IndexOf("main2", iBodyStart);
            int iTableStart = strWebContent.IndexOf("<table", iStart);
            int iTableEnd = strWebContent.IndexOf("</table>", iTableStart);

            string strWeb = strWebContent.Substring(iTableStart, iTableEnd - iTableStart + 8);

            WebBrowser webb = new WebBrowser();

            webb.Navigate("about:blank");

            HtmlDocument htmldoc = webb.Document.OpenNew(true);

            htmldoc.Write(strWeb);

            HtmlElementCollection htmlTR = htmldoc.GetElementsByTagName("tr");

            int count = 0;

            ArrayList tempData = new ArrayList();

            foreach (HtmlElement tr in htmlTR)
            {
                HtmlElementCollection htmlTD = tr.GetElementsByTagName("TD");

                foreach (HtmlElement td in htmlTD)
                {
                    tempData.Add(td.InnerText);
                }
                if (count > 0)
                {
                    textBox2.Text += tempData[0] + " " + tempData[1] + " " + tempData[2] + " " + tempData[3]
                        + " " + tempData[4] + " " + tempData[5] + " " + tempData[6] + " " + tempData[7] + " "
                        + tempData[8] + " " + tempData[9] + " " + tempData[10] + " " + tempData[11] + " "
                        + tempData[12] + " " + tempData[13] + Environment.NewLine;
                }


                count++;
                tempData.Clear();

            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {

            getMSCI();
            getfgbuy();
            getfgsell();

            textBox2.Text = "hello";

        }

        //private void button3_Click(object sender, EventArgs e)
        //{
        //    dataGridView2.DataSource = getfgbuy();
        //}

        //private void button4_Click(object sender, EventArgs e)
        //{
        //    //string Url = "https://tw.stock.yahoo.com/d/i/fgsell_tse.html";

        //    //string strWebContent = GetWebContentBig5(Url);

        //    ////int iBodyStart = strWebContent.IndexOf("<body", 0);
        //    //int iStart = strWebContent.IndexOf("content start", 0);

        //    //int iTableStart = strWebContent.IndexOf("<table", iStart);
        //    //int iTableEnd = strWebContent.IndexOf("</table>", iStart);

        //    //int iEnd = strWebContent.IndexOf("content end", iStart);

        //    //int iLastTableStart = strWebContent.IndexOf("<table", iTableEnd);
        //    //int iLastTableEnd = strWebContent.IndexOf("</table>", iLastTableStart);

        //    //int iLLastTableStart = strWebContent.IndexOf("<table", iLastTableEnd);
        //    //int iLLastTableEnd = strWebContent.IndexOf("</table>", iLLastTableStart);


        //    //string strWeb = strWebContent.Substring(iLLastTableStart, iLLastTableEnd - iLLastTableStart);


        //    //WebBrowser webb = new WebBrowser();

        //    //webb.Navigate("about:blank");

        //    //HtmlDocument htmldoc = webb.Document.OpenNew(true);

        //    //htmldoc.Write(strWeb);

        //    //HtmlElementCollection htmlTR = htmldoc.GetElementsByTagName("tr");

        //    //int count = 0;

        //    //ArrayList tempData = new ArrayList();

        //    //foreach (HtmlElement tr in htmlTR)
        //    //{
        //    //    HtmlElementCollection htmlTD = tr.GetElementsByTagName("TD");

        //    //    foreach (HtmlElement td in htmlTD)
        //    //    {
        //    //        tempData.Add(td.InnerText);
        //    //    }
        //    //    if (count > 0)
        //    //    {
        //    //        textBox3.Text += tempData[0] + " " + tempData[1] + " " + tempData[2] + " " + tempData[3] + " " + tempData[4] + " " + tempData[5] + " " + tempData[6] + Environment.NewLine;
        //    //    }


        //    //    count++;
        //    //    tempData.Clear();

        //    //}
        //    dataGridView3.DataSource = getfgsell();

        //}
    }



}
