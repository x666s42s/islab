USE [StockOperator]
GO

/****** Object:  Table [dbo].[FGBuy]    Script Date: 2017/5/22 �U�� 09:53:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FGBuy](
	[fgbuy_ID] [nvarchar](20) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Rank] [nvarchar](10) NOT NULL,
	[StockID] [nvarchar](10) NOT NULL,
	[StockName] [nvarchar](20) NOT NULL,
	[StrikePrice] [nvarchar](20) NOT NULL,
	[UpAndDown] [nvarchar](20) NOT NULL,
	[OverboughtCount] [nvarchar](20) NOT NULL,
	[ForeignHoldingCount] [nvarchar](20) NOT NULL,
	[ForeignHoldingWeight] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_FGBuy] PRIMARY KEY CLUSTERED 
(
	[fgbuy_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

