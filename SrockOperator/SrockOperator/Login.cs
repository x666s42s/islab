﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SrockOperator;
using System.Configuration;
using System.Data.SqlClient;
using System.Net.NetworkInformation;
namespace SrockOperator
{
    public partial class Login : Form
    {

        StockOperator temp = new StockOperator();

        public Login(StockOperator mainForm)
        {
            InitializeComponent();
            temp = mainForm;
            Remember_Account();
        }

        private void Remember_Account()
        {
            List<string> macList = new List<string>();
            foreach (var nic in NetworkInterface.GetAllNetworkInterfaces())
                if (nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet) macList.Add(nic.GetPhysicalAddress().ToString());
            string mac = macList[0].ToString();
            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand("User_Remember"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@MacAddress", mac);
                    con.Open();
                    using (var dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            AccountText.Text = dr[0].ToString();
                            PassText.Text = dr[1].ToString();
                        }
                        
                    }
                    con.Close();
                }
            }
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoginBtn_Click(object sender, EventArgs e)
        {

            //if (AccountText.Text == "x666s42s" && PassText.Text == "123")
            //{
            //    temp.CheckLoginLabel(AccountText.Text);

            //    this.Close();
            //}
            //else
            //{
            //    MessageBox.Show("帳號或密碼有誤");

            //    this.Close();
            //}


            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand("User_Login"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@UserID", AccountText.Text);
                    cmd.Parameters.AddWithValue("@Password", PassText.Text);
                    con.Open();

                    #region Insert Mac
                    if (checkBox1.Checked == true)
                    {
                        List<string> macList = new List<string>();
                        foreach (var nic in NetworkInterface.GetAllNetworkInterfaces())
                            if (nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet) macList.Add(nic.GetPhysicalAddress().ToString());
                        string mac = macList[0].ToString();
                        String strSQL = "UPDATE StockUser SET StockUser.MacAddress='" + mac + "' WHERE UserID='" + AccountText.Text + "'";
                        SqlCommand sqlcommand = new SqlCommand(strSQL, con);
                        sqlcommand.ExecuteNonQuery();
                    }
                    #endregion

                    using ( var dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            temp.CheckLoginLabel(AccountText.Text);

                            temp.Load_StockCombobox();

                            temp.Load_SellingShortComboBox();
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("帳號或密碼有誤");
                            this.Close();
                        }
                    }
                    
                    con.Close();
                }
            }

        }

        #region Windows Form 控制項
        /// <summary>
        /// 控制畫面大小比例
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private float X;//當前窗體的寬度
        private float Y;//當前窗體的高度
        private void Login_Load(object sender, EventArgs e)
        {
            X = this.Width;//獲取窗體的寬度
            Y = this.Height;//獲取窗體的高度
            setTag(this);//調用方法
        }
        private void Login_Resize(object sender, EventArgs e)
        {
            float newx = (this.Width) / X; //窗體寬度縮放比例
            float newy = (this.Height) / Y;//窗體高度縮放比例
            setControls(newx, newy, this);//隨窗體改變控制項大小
        }
        private void setTag(Control cons)
        {
            foreach (Control con in cons.Controls)
            {
                con.Tag = con.Width + ":" + con.Height + ":" + con.Left + ":" + con.Top + ":" + con.Font.Size;
                if (con.Controls.Count > 0)
                    setTag(con);
            }
        }
        private void setControls(float newx, float newy, Control cons)
        {
            //遍歷窗體中的控制項，重新設置控制項的值
            foreach (Control con in cons.Controls)
            {
                if (con.Tag != null)
                {
                    var mytag = con.Tag.ToString().Split(':');//獲取控制項的Tag屬性值，並分割後存儲字元串數組
                    float a = System.Convert.ToSingle(mytag[0]) * newx;//根據窗體縮放比例確定控制項的值，寬度
                    con.Width = (int)a;//寬度
                    a = System.Convert.ToSingle(mytag[1]) * newy;//高度
                    con.Height = (int)(a);
                    a = System.Convert.ToSingle(mytag[2]) * newx;//左邊距離
                    con.Left = (int)(a);
                    a = System.Convert.ToSingle(mytag[3]) * newy;//上邊緣距離
                    con.Top = (int)(a);
                    Single currentSize = System.Convert.ToSingle(mytag[4]) * newy;//字體大小
                    con.Font = new Font(con.Font.Name, currentSize, con.Font.Style, con.Font.Unit);
                    if (con.Controls.Count > 0)
                    {
                        setControls(newx, newy, con);
                    }
                }
            }
        }

        #endregion

       
    }
}
