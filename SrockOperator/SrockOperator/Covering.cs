﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace SrockOperator
{
    public partial class Covering : Form
    {
        StockOperator tempMain = null;

        DataGridViewRow temp = null;

        DataTable dt = null;

        public Covering(StockOperator main, DataGridViewRow row)
        {
            InitializeComponent();

            tempMain = main;
            temp = row;
        }

        private void UpdateSellingShort_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(temp.Cells[0].Value + " " + temp.Cells[1].Value + " " + temp.Cells[2].Value + " " + temp.Cells[3].Value + " " + temp.Cells[4].Value);
            dt = new DataTable();

            dt.Columns.Add("UserID");
            dt.Columns.Add("StockID");
            dt.Columns.Add("StockName");
            dt.Columns.Add("SellingShortDate");
            dt.Columns.Add("SellingShortStrikePrice");
            dt.Columns.Add("SellingShortCount");
            dt.Columns.Add("ShortCoveringDate");
            dt.Columns.Add("ShortCoveringStrikePrice");
            dt.Columns.Add("ShortCoveringCount");
            dt.Columns.Add("ProfitRate");


            dt.Rows.Add(tempMain.ReturnUserID(), temp.Cells[0].Value, temp.Cells[1].Value, Convert.ToDateTime(temp.Cells[2].Value).ToString("yyyy/MM/dd"), temp.Cells[3].Value, temp.Cells[4].Value, SellStockDate.Value.ToString("yyyy/MM/dd"), SellStrikePrice.Text, SellStockCount.Value, null);

            string result = "";

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                result += dt.Rows[0][i] + " ";
            }

            MessageBox.Show(result);

            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand("Insert_SellingShortRecord"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@dt", dt);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }

            tempMain.showUserSellingShortRecord();

            this.Close();
        }

        private void CoveringCancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
