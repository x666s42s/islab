﻿namespace SrockOperator
{
    partial class Covering
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CoveringCancelBtn = new System.Windows.Forms.Button();
            this.UpdateSellingShort = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.SellStockCount = new System.Windows.Forms.NumericUpDown();
            this.SellStrikePrice = new System.Windows.Forms.TextBox();
            this.SellStockDate = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.SellStockCount)).BeginInit();
            this.SuspendLayout();
            // 
            // CoveringCancelBtn
            // 
            this.CoveringCancelBtn.Location = new System.Drawing.Point(186, 201);
            this.CoveringCancelBtn.Margin = new System.Windows.Forms.Padding(2);
            this.CoveringCancelBtn.Name = "CoveringCancelBtn";
            this.CoveringCancelBtn.Size = new System.Drawing.Size(56, 26);
            this.CoveringCancelBtn.TabIndex = 40;
            this.CoveringCancelBtn.Text = "取消";
            this.CoveringCancelBtn.UseVisualStyleBackColor = true;
            this.CoveringCancelBtn.Click += new System.EventHandler(this.CoveringCancelBtn_Click);
            // 
            // UpdateSellingShort
            // 
            this.UpdateSellingShort.Location = new System.Drawing.Point(73, 201);
            this.UpdateSellingShort.Margin = new System.Windows.Forms.Padding(2);
            this.UpdateSellingShort.Name = "UpdateSellingShort";
            this.UpdateSellingShort.Size = new System.Drawing.Size(56, 26);
            this.UpdateSellingShort.TabIndex = 39;
            this.UpdateSellingShort.Text = "送出";
            this.UpdateSellingShort.UseVisualStyleBackColor = true;
            this.UpdateSellingShort.Click += new System.EventHandler(this.UpdateSellingShort_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(71, 75);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 12);
            this.label6.TabIndex = 38;
            this.label6.Text = "回補股票日期";
            // 
            // SellStockCount
            // 
            this.SellStockCount.Location = new System.Drawing.Point(149, 107);
            this.SellStockCount.Margin = new System.Windows.Forms.Padding(2);
            this.SellStockCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.SellStockCount.Name = "SellStockCount";
            this.SellStockCount.Size = new System.Drawing.Size(94, 22);
            this.SellStockCount.TabIndex = 36;
            this.SellStockCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // SellStrikePrice
            // 
            this.SellStrikePrice.Location = new System.Drawing.Point(149, 142);
            this.SellStrikePrice.Margin = new System.Windows.Forms.Padding(2);
            this.SellStrikePrice.Name = "SellStrikePrice";
            this.SellStrikePrice.Size = new System.Drawing.Size(95, 22);
            this.SellStrikePrice.TabIndex = 37;
            // 
            // SellStockDate
            // 
            this.SellStockDate.CustomFormat = "yyyy/MM/dd";
            this.SellStockDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SellStockDate.Location = new System.Drawing.Point(149, 69);
            this.SellStockDate.Margin = new System.Windows.Forms.Padding(2);
            this.SellStockDate.Name = "SellStockDate";
            this.SellStockDate.Size = new System.Drawing.Size(95, 22);
            this.SellStockDate.TabIndex = 35;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(83, 144);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 12);
            this.label11.TabIndex = 34;
            this.label11.Text = "回補成交價";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(71, 109);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 12);
            this.label10.TabIndex = 33;
            this.label10.Text = "回補股票張數";
            // 
            // Covering
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(314, 297);
            this.ControlBox = false;
            this.Controls.Add(this.CoveringCancelBtn);
            this.Controls.Add(this.UpdateSellingShort);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.SellStockCount);
            this.Controls.Add(this.SellStrikePrice);
            this.Controls.Add(this.SellStockDate);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Name = "Covering";
            this.Text = "回補";
            ((System.ComponentModel.ISupportInitialize)(this.SellStockCount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CoveringCancelBtn;
        private System.Windows.Forms.Button UpdateSellingShort;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown SellStockCount;
        private System.Windows.Forms.TextBox SellStrikePrice;
        private System.Windows.Forms.DateTimePicker SellStockDate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
    }
}