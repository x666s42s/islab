﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SrockOperator
{
    public partial class Offset : Form
    {
        StockOperator tempMain = null;

        DataGridViewRow temp = null;

        DataTable dt = null;

        public Offset(StockOperator main, DataGridViewRow row)
        {
            InitializeComponent();

            tempMain = main;
            temp = row;
        }

        private void UpdateStock_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(temp.Cells[0].Value + " " + temp.Cells[1].Value + " " + temp.Cells[2].Value + " " + temp.Cells[3].Value + " " + temp.Cells[4].Value);
            dt = new DataTable();

            dt.Columns.Add("UserID");
            dt.Columns.Add("StockID");
            dt.Columns.Add("StockName");
            dt.Columns.Add("BuyDate");
            dt.Columns.Add("BuyStrikePrice");
            dt.Columns.Add("BuyCount");
            dt.Columns.Add("SellDate");
            dt.Columns.Add("SellStrikePrice");
            dt.Columns.Add("SellCount");
            dt.Columns.Add("ProfitRate");


            dt.Rows.Add(tempMain.ReturnUserID(), temp.Cells[0].Value, temp.Cells[1].Value, Convert.ToDateTime(temp.Cells[2].Value).ToString("yyyy/MM/dd"), temp.Cells[3].Value, temp.Cells[4].Value, SellStockDate.Value.ToString("yyyy/MM/dd"), SellStrikePrice.Text, SellStockCount.Value, null);

            

            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand("Insert_StockRecord"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@dt", dt);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }

            if(tempMain.getTaiExMA() > -50)
            {
                tempMain.showUserStockRecord();
            }
            else
            {
                tempMain.showAllUserStockRecord();
            }
            

            MessageBox.Show("平倉成功");

            this.Close();
        }


        #region Windows Form 控制項
        /// <summary>
        /// 控制畫面大小比例
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private float X;//當前窗體的寬度
        private float Y;//當前窗體的高度
        private void Offset_Load(object sender, EventArgs e)
        {
            X = this.Width;//獲取窗體的寬度
            Y = this.Height;//獲取窗體的高度
            setTag(this);//調用方法
        }
        private void Offset_Resize(object sender, EventArgs e)
        {
            float newx = (this.Width) / X; //窗體寬度縮放比例
            float newy = (this.Height) / Y;//窗體高度縮放比例
            setControls(newx, newy, this);//隨窗體改變控制項大小
        }
        private void setTag(Control cons)
        {
            foreach (Control con in cons.Controls)
            {
                con.Tag = con.Width + ":" + con.Height + ":" + con.Left + ":" + con.Top + ":" + con.Font.Size;
                if (con.Controls.Count > 0)
                    setTag(con);
            }
        }
        private void setControls(float newx, float newy, Control cons)
        {
            //遍歷窗體中的控制項，重新設置控制項的值
            foreach (Control con in cons.Controls)
            {
                if (con.Tag != null)
                {
                    var mytag = con.Tag.ToString().Split(':');//獲取控制項的Tag屬性值，並分割後存儲字元串數組
                    float a = System.Convert.ToSingle(mytag[0]) * newx;//根據窗體縮放比例確定控制項的值，寬度
                    con.Width = (int)a;//寬度
                    a = System.Convert.ToSingle(mytag[1]) * newy;//高度
                    con.Height = (int)(a);
                    a = System.Convert.ToSingle(mytag[2]) * newx;//左邊距離
                    con.Left = (int)(a);
                    a = System.Convert.ToSingle(mytag[3]) * newy;//上邊緣距離
                    con.Top = (int)(a);
                    Single currentSize = System.Convert.ToSingle(mytag[4]) * newy;//字體大小
                    con.Font = new Font(con.Font.Name, currentSize, con.Font.Style, con.Font.Unit);
                    if (con.Controls.Count > 0)
                    {
                        setControls(newx, newy, con);
                    }
                }
            }
        }


        #endregion

        private void OffsetCancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
