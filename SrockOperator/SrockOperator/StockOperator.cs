﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace SrockOperator
{
    public partial class StockOperator : Form
    {


        DataTable threeDaysBuyOver = null;

        DataTable FGSellChangeIntoFGBuy = null;

        DataTable threeDaysSellOver = null;

        DataTable FGBuyChangeIntoFGSell = null;

        DataGridViewButtonColumn OffsetBtn = null;

        DataGridViewButtonColumn CoveringBtn = null;

        public StockOperator()
        {
            InitializeComponent();
            getThreeDaysBuyOverData();
            getThreeDaysSellOverData();
            getFGBuyChangeIntoFGSellData();
            getFGSellChangeIntoFGBuyData();
            getThreeDaysBuyOverDataCrossT86();
            getThreeDaysSellOverDataCrossT86();
            showTaiExMA();
        }

        private void getThreeDaysBuyOverData()
        {
            threeDaysBuyOver = new DataTable();

            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand("Search_3Days_FGBuy"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@dt", dateTimePicker1.Value);
                    con.Open();
                    threeDaysBuyOver.Load(cmd.ExecuteReader());
                    con.Close();
                }
            }

            threeDaysBuyOver.Columns[3].ColumnName = Convert.ToDateTime(threeDaysBuyOver.Rows[1][10]).ToString("yyyy/MM/dd") + "\n外資買超張數";
            threeDaysBuyOver.Columns[4].ColumnName = Convert.ToDateTime(threeDaysBuyOver.Rows[1][9]).ToString("yyyy/MM/dd") + "\n外資買超張數";
            threeDaysBuyOver.Columns[5].ColumnName = Convert.ToDateTime(threeDaysBuyOver.Rows[1][8]).ToString("yyyy/MM/dd") + "\n外資買超張數";

            threeDaysBuyOver.Columns.Remove("Date");
            threeDaysBuyOver.Columns.Remove("Date1");
            threeDaysBuyOver.Columns.Remove("Date2");

            dataGridView1.DataSource = threeDaysBuyOver;
        }

        private void getThreeDaysSellOverData()
        {
            threeDaysSellOver = new DataTable();

            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand("Search_3Days_FGSell"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@dt", dateTimePicker1.Value);
                    con.Open();
                    threeDaysSellOver.Load(cmd.ExecuteReader());
                    con.Close();
                }
            }

            threeDaysSellOver.Columns[3].ColumnName = Convert.ToDateTime(threeDaysSellOver.Rows[1][10]).ToString("yyyy/MM/dd") + "\n外資賣超張數";
            threeDaysSellOver.Columns[4].ColumnName = Convert.ToDateTime(threeDaysSellOver.Rows[1][9]).ToString("yyyy/MM/dd") + "\n外資賣超張數";
            threeDaysSellOver.Columns[5].ColumnName = Convert.ToDateTime(threeDaysSellOver.Rows[1][8]).ToString("yyyy/MM/dd") + "\n外資賣超張數";

            threeDaysSellOver.Columns.Remove("Date");
            threeDaysSellOver.Columns.Remove("Date1");
            threeDaysSellOver.Columns.Remove("Date2");

            dataGridView2.DataSource = threeDaysSellOver;
        }

        
        private void getFGBuyChangeIntoFGSellData()
        {
            FGBuyChangeIntoFGSell = new DataTable();

            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand("Search_Change_FGBuyIntoFGSell"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@dt", dateTimePicker2.Value);
                    con.Open();
                    FGBuyChangeIntoFGSell.Load(cmd.ExecuteReader());
                    con.Close();
                }
            }
            dataGridView4.DataSource = FGBuyChangeIntoFGSell;
        }

        private void getFGSellChangeIntoFGBuyData()
        {
            FGSellChangeIntoFGBuy = new DataTable();

            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand("Search_Change_FGSellIntoFGBuy"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@dt", dateTimePicker2.Value);
                    con.Open();
                    FGSellChangeIntoFGBuy.Load(cmd.ExecuteReader());
                    con.Close();
                }
            }
            dataGridView3.DataSource = FGSellChangeIntoFGBuy;
        }

        private void getThreeDaysBuyOverDataCrossT86()
        {
            

            DataTable threeDaysBuyOverCrossT86 = new DataTable();

            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand("Search_3Days_FGBuy_JOIN_T86"))
                {              
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@date", dateTimePicker3.Value);
                    con.Open();
                    threeDaysBuyOverCrossT86.Load(cmd.ExecuteReader());
                    con.Close();
                }
            }



            T86BuyGridView.DataSource = threeDaysBuyOverCrossT86;

        }

        private void getThreeDaysSellOverDataCrossT86()
        {
            DataTable threeDaysSellOverCrossT86 = new DataTable();

            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand("Search_3Days_FGSell_JOIN_T86"))
                {                    
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@date", dateTimePicker3.Value);                   
                    con.Open();
                    threeDaysSellOverCrossT86.Load(cmd.ExecuteReader());
                    con.Close();
                }
            }



            T86SellGridView.DataSource = threeDaysSellOverCrossT86;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            getThreeDaysBuyOverData();
            getThreeDaysSellOverData();
            dateTimePicker2.Value = dateTimePicker1.Value;
            dateTimePicker3.Value = dateTimePicker1.Value;

        }
        
        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            getFGBuyChangeIntoFGSellData();
            getFGSellChangeIntoFGBuyData();
            dateTimePicker1.Value = dateTimePicker2.Value;
            dateTimePicker3.Value = dateTimePicker2.Value;

        }


        private void dateTimePicker3_ValueChanged(object sender, EventArgs e)
        {
            getThreeDaysBuyOverDataCrossT86();
            getThreeDaysSellOverDataCrossT86();
            dateTimePicker1.Value = dateTimePicker3.Value;
            dateTimePicker2.Value = dateTimePicker3.Value;

        }

        #region Windows Form 控制項
        /// <summary>
        /// 控制畫面大小比例
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private float X;//當前窗體的寬度
        private float Y;//當前窗體的高度
        private void StockOperator_Load(object sender, EventArgs e)
        {
            X = this.Width;//獲取窗體的寬度
            Y = this.Height;//獲取窗體的高度
            setTag(this);//調用方法
        }
        private void StockOperator_Resize(object sender, EventArgs e)
        {
            float newx = (this.Width) / X; //窗體寬度縮放比例
            float newy = (this.Height) / Y;//窗體高度縮放比例
            setControls(newx, newy, this);//隨窗體改變控制項大小
        }
        private void setTag(Control cons)
        {
            foreach (Control con in cons.Controls)
            {
                con.Tag = con.Width + ":" + con.Height + ":" + con.Left + ":" + con.Top + ":" + con.Font.Size;
                if (con.Controls.Count > 0)
                    setTag(con);
            }
        }
        private void setControls(float newx, float newy, Control cons)
        {
            //遍歷窗體中的控制項，重新設置控制項的值
            foreach (Control con in cons.Controls)
            {
                if (con.Tag != null)
                {
                    var mytag = con.Tag.ToString().Split(':');//獲取控制項的Tag屬性值，並分割後存儲字元串數組
                    float a = System.Convert.ToSingle(mytag[0]) * newx;//根據窗體縮放比例確定控制項的值，寬度
                    con.Width = (int)a;//寬度
                    a = System.Convert.ToSingle(mytag[1]) * newy;//高度
                    con.Height = (int)(a);
                    a = System.Convert.ToSingle(mytag[2]) * newx;//左邊距離
                    con.Left = (int)(a);
                    a = System.Convert.ToSingle(mytag[3]) * newy;//上邊緣距離
                    con.Top = (int)(a);
                    Single currentSize = System.Convert.ToSingle(mytag[4]) * newy;//字體大小
                    con.Font = new Font(con.Font.Name, currentSize, con.Font.Style, con.Font.Unit);
                    if (con.Controls.Count > 0)
                    {
                        setControls(newx, newy, con);
                    }
                }
            }
        }
        #endregion

        public void LoginBtn_Click(object sender, EventArgs e)
        {
            if (CheckLogin.Text == "未登入")
            {
                Login loginForm = new Login(this);
                loginForm.ShowDialog();
            }
            else if (CallLoginBtn.Text == "登出")
            {
                StockRecordView.Columns.Clear();
                SellingShortView.Columns.Clear();
                StockCombobox.Items.Clear();
                StockCombobox.Text = "";
                SellingShortComboBox.Items.Clear();
                SellingShortComboBox.Text = "";
                CheckLogin.Text = "未登入";
                CallLoginBtn.Text = "登入";
                MessageBox.Show("已登出");
            }

        }

        public void CheckLoginLabel(string UserID)
        {
            CheckLogin.Text = UserID;

            CallLoginBtn.Text = "登出";
        }

        public string ReturnUserID()
        {
            return CheckLogin.Text;
        }

        private void InsertStock_Click(object sender, EventArgs e)
        {

            if (CheckLogin.Text != "未登入")
            {
                DataTable dt = new DataTable();

                if (BuyStockID.Text != "" && BuyStockName.Text != "" && BuyStrikePrice.Text != "")
                {

                    string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
                    using (SqlConnection con = new SqlConnection(consString))
                    {
                        using (SqlCommand cmd = new SqlCommand("Insert_StockRecord"))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Connection = con;

                            dt.Columns.Add("UserID");
                            dt.Columns.Add("StockID");
                            dt.Columns.Add("StockName");
                            dt.Columns.Add("BuyDate");
                            dt.Columns.Add("BuyStrikePrice");
                            dt.Columns.Add("BuyCount");
                            dt.Columns.Add("SellDate");
                            dt.Columns.Add("SellStrikePrice");
                            dt.Columns.Add("SellCount");
                            dt.Columns.Add("ProfitRate");
                            dt.Rows.Add(CheckLogin.Text, BuyStockID.Text, BuyStockName.Text, BuyStockDate.Value.ToString("yyyy/MM/dd"), BuyStrikePrice.Text, BuyStockCount.Value, null, null, 0, null);


                            cmd.Parameters.AddWithValue("@dt", dt);

                            con.Open();

                            cmd.ExecuteNonQuery();

                            con.Close();

                            showUserStockRecord();

                            MessageBox.Show("匯入成功");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("請填寫完整");
                }
            }
            else
            {
                MessageBox.Show("請登入");

                Login loginForm = new Login(this);
                loginForm.ShowDialog();
            }

        }

        public void showUserStockRecord()
        {
            ClearStockRecord();

            DataTable userStockRecord = new DataTable();

            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand("Show_UserStockRecord"))
                {
                    DataTable tempThreeDaysBuyOver = new DataTable();

                    tempThreeDaysBuyOver.Merge(threeDaysBuyOver);

                    DataTable tempFGSellChangeIntoFGBuy = new DataTable();

                    tempFGSellChangeIntoFGBuy.Merge(FGSellChangeIntoFGBuy);

                    DataTable tempFGBuyChangeIntoFGSell = new DataTable();

                    tempFGBuyChangeIntoFGSell.Merge(FGBuyChangeIntoFGSell);

                    //MessageBox.Show(threeDaysBuyOver.Columns.Count.ToString() + FGSellChangeIntoFGBuy.Columns.Count.ToString());

                    //if (tempThreeDaysBuyOver.Columns.Count == 11 && tempFGSellChangeIntoFGBuy.Columns.Count == 8)
                    //{
                    //    tempThreeDaysBuyOver.Columns.Remove("外資比重");
                    //    tempThreeDaysBuyOver.Columns.Remove("Date");
                    //    tempThreeDaysBuyOver.Columns.Remove("Date1");
                    //    tempThreeDaysBuyOver.Columns.Remove("Date2");
                    //    tempFGSellChangeIntoFGBuy.Columns.Remove("外資比重");
                    //    tempFGBuyChangeIntoFGSell.Columns.Remove("外資比重");
                    //}

                    //MessageBox.Show(threeDaysBuyOver.Columns.Count.ToString() + FGSellChangeIntoFGBuy.Columns.Count.ToString());
                    //MessageBox.Show(threeDaysBuyOver.Columns.Count.ToString() + FGSellChangeIntoFGBuy.Columns.Count.ToString());

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@date1", dateTimePicker1.Value);
                    cmd.Parameters.AddWithValue("@User", CheckLogin.Text);
                    //cmd.Parameters.AddWithValue("@dt1", tempThreeDaysBuyOver);
                    //cmd.Parameters.AddWithValue("@dt2", tempFGSellChangeIntoFGBuy);
                    //cmd.Parameters.AddWithValue("@dt3", tempFGBuyChangeIntoFGSell);
                    con.Open();
                    userStockRecord.Load(cmd.ExecuteReader());
                    con.Close();
                }
            }



            StockRecordView.DataSource = userStockRecord;

            #region 列表平倉按鈕
            OffsetBtn = new DataGridViewButtonColumn();
            OffsetBtn.Name = "Offset";
            OffsetBtn.UseColumnTextForButtonValue = true;
            OffsetBtn.Text = "平倉";
            OffsetBtn.Width = 30;
            StockRecordView.Columns.Add(OffsetBtn);
            #endregion


        }

        public void showAllUserStockRecord()
        {
            ClearStockRecord();

            DataTable userStockRecord = new DataTable();

            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand("Show_AllUserStockRecord"))
                {
                    DataTable tempThreeDaysBuyOver = new DataTable();

                    tempThreeDaysBuyOver.Merge(threeDaysBuyOver);

                    DataTable tempFGSellChangeIntoFGBuy = new DataTable();

                    tempFGSellChangeIntoFGBuy.Merge(FGSellChangeIntoFGBuy);

                    DataTable tempFGBuyChangeIntoFGSell = new DataTable();

                    tempFGBuyChangeIntoFGSell.Merge(FGBuyChangeIntoFGSell);

                    //MessageBox.Show(threeDaysBuyOver.Columns.Count.ToString() + FGSellChangeIntoFGBuy.Columns.Count.ToString());

                    if (tempThreeDaysBuyOver.Columns.Count == 9 && tempFGSellChangeIntoFGBuy.Columns.Count == 8)
                    {
                        tempThreeDaysBuyOver.Columns.Remove("外資比重");
                        tempFGSellChangeIntoFGBuy.Columns.Remove("外資比重");
                        tempFGBuyChangeIntoFGSell.Columns.Remove("外資比重");
                    }

                    //MessageBox.Show(threeDaysBuyOver.Columns.Count.ToString() + FGSellChangeIntoFGBuy.Columns.Count.ToString());
                    //MessageBox.Show(threeDaysBuyOver.Columns.Count.ToString() + FGSellChangeIntoFGBuy.Columns.Count.ToString());

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@User", CheckLogin.Text);

                    con.Open();
                    userStockRecord.Load(cmd.ExecuteReader());
                    con.Close();
                }
            }



            StockRecordView.DataSource = userStockRecord;

            #region 列表平倉按鈕
            OffsetBtn = new DataGridViewButtonColumn();
            OffsetBtn.Name = "Offset";
            OffsetBtn.UseColumnTextForButtonValue = true;
            OffsetBtn.Text = "平倉";
            OffsetBtn.Width = 30;
            StockRecordView.Columns.Add(OffsetBtn);
            #endregion

        }

        public void ClearStockRecord()
        {
            StockRecordView.Columns.Clear();
        }

        private void OffsetClick(object sender, DataGridViewCellEventArgs e)
        {


            if (e.RowIndex >= 0 && StockRecordView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value == "平倉")
            {
                DataGridViewButtonCell clickedButtonCell = (DataGridViewButtonCell)StockRecordView.Rows[e.RowIndex].Cells[e.ColumnIndex];
                //MessageBox.Show(dataGridView5.Rows[e.RowIndex].Cells[2].Value.ToString());

                Offset offsetForm = new Offset(this, StockRecordView.Rows[e.RowIndex]);
                offsetForm.ShowDialog();
            }


        }

        private void InsertSellingShort_Click(object sender, EventArgs e)
        {
            if (CheckLogin.Text != "未登入")
            {
                DataTable dt = new DataTable();

                if (SellingShortStockID.Text != "" && SellingShortStockName.Text != "" && SellingShortStrikePrice.Text != "")
                {

                    string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
                    using (SqlConnection con = new SqlConnection(consString))
                    {
                        using (SqlCommand cmd = new SqlCommand("Insert_SellingShortRecord"))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Connection = con;

                            dt.Columns.Add("UserID");
                            dt.Columns.Add("StockID");
                            dt.Columns.Add("StockName");
                            dt.Columns.Add("SellingShortDate");
                            dt.Columns.Add("SellingShortStrikePrice");
                            dt.Columns.Add("SellingShortCount");
                            dt.Columns.Add("ShortCoveringDate");
                            dt.Columns.Add("ShortCoveringStrikePrice");
                            dt.Columns.Add("ShortCoveringCount");
                            dt.Columns.Add("ProfitRate");
                            dt.Rows.Add(CheckLogin.Text, SellingShortStockID.Text, SellingShortStockName.Text, SellingShortDate.Value.ToString("yyyy/MM/dd"), SellingShortStrikePrice.Text, SellingShortCount.Value, null, null, 0, null);


                            cmd.Parameters.AddWithValue("@dt", dt);

                            con.Open();

                            cmd.ExecuteNonQuery();

                            con.Close();

                            showUserSellingShortRecord();

                            MessageBox.Show("匯入成功");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("請填寫完整");
                }
            }
            else
            {
                MessageBox.Show("請登入");

                Login loginForm = new Login(this);
                loginForm.ShowDialog();
            }
        }

        public void showUserSellingShortRecord()
        {

            ClearSellingShortRecord();

            DataTable userStockRecord = new DataTable();

            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand("Show_UserSellShortRecord"))
                {

                    DataTable tempThreeDaysSellOver = new DataTable();

                    tempThreeDaysSellOver.Merge(threeDaysSellOver);

                    DataTable tempFGBuyChangeIntoFGSell = new DataTable();

                    tempFGBuyChangeIntoFGSell.Merge(FGBuyChangeIntoFGSell);

                    if (tempThreeDaysSellOver.Columns.Count == 9 && tempFGBuyChangeIntoFGSell.Columns.Count == 8)
                    {
                        tempThreeDaysSellOver.Columns.Remove("外資比重");
                        tempFGBuyChangeIntoFGSell.Columns.Remove("外資比重");
                    }

                    //MessageBox.Show(threeDaysBuyOver.Columns.Count.ToString() + FGSellChangeIntoFGBuy.Columns.Count.ToString());

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@User", CheckLogin.Text);
                    cmd.Parameters.AddWithValue("@dt1", tempThreeDaysSellOver);
                    cmd.Parameters.AddWithValue("@dt2", tempFGBuyChangeIntoFGSell);
                    con.Open();
                    userStockRecord.Load(cmd.ExecuteReader());
                    con.Close();
                }
            }

            SellingShortView.DataSource = userStockRecord;

            #region 列表回補按鈕
            CoveringBtn = new DataGridViewButtonColumn();
            CoveringBtn.Name = "Cover";
            CoveringBtn.UseColumnTextForButtonValue = true;
            CoveringBtn.Text = "回補";
            CoveringBtn.Width = 30;
            SellingShortView.Columns.Add(CoveringBtn);
            #endregion
        }

        public void showAllUserSellingShortRecord()
        {
            
            ClearSellingShortRecord();

            DataTable userStockRecord = new DataTable();

            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand("Show_AllUserSellingShortRecord"))
                {

                    DataTable tempThreeDaysSellOver = new DataTable();

                    tempThreeDaysSellOver.Merge(threeDaysSellOver);

                    DataTable tempFGBuyChangeIntoFGSell = new DataTable();

                    tempFGBuyChangeIntoFGSell.Merge(FGBuyChangeIntoFGSell);

                    if (tempThreeDaysSellOver.Columns.Count == 9 && tempFGBuyChangeIntoFGSell.Columns.Count == 8)
                    {
                        tempThreeDaysSellOver.Columns.Remove("外資比重");
                        tempFGBuyChangeIntoFGSell.Columns.Remove("外資比重");
                    }

                    //MessageBox.Show(threeDaysBuyOver.Columns.Count.ToString() + FGSellChangeIntoFGBuy.Columns.Count.ToString());

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@User", CheckLogin.Text);
                    con.Open();
                    userStockRecord.Load(cmd.ExecuteReader());
                    con.Close();
                }
            }

            SellingShortView.DataSource = userStockRecord;

            #region 列表回補按鈕
            CoveringBtn = new DataGridViewButtonColumn();
            CoveringBtn.Name = "Cover";
            CoveringBtn.UseColumnTextForButtonValue = true;
            CoveringBtn.Text = "回補";
            CoveringBtn.Width = 30;
            SellingShortView.Columns.Add(CoveringBtn);
            #endregion
        }

        public void ClearSellingShortRecord()
        {
            SellingShortView.Columns.Clear();
        }

        private void CoveringClick(object sender, DataGridViewCellEventArgs e)
        {


            if (e.RowIndex >= 0 && SellingShortView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value == "回補")
            {
                DataGridViewButtonCell clickedButtonCell = (DataGridViewButtonCell)SellingShortView.Rows[e.RowIndex].Cells[e.ColumnIndex];
                //MessageBox.Show(dataGridView5.Rows[e.RowIndex].Cells[2].Value.ToString());

                Covering coveringForm = new Covering(this, SellingShortView.Rows[e.RowIndex]);
                coveringForm.ShowDialog();
            }


        }

        private void SellingShortStrikePrice_TextChanged(object sender, EventArgs e)
        {
            string previousInput = "";
            Regex r = new Regex(@"^[+-]?\d*[.]?\d*$"); 
            Match m = r.Match(SellingShortStrikePrice.Text);
            if (m.Success)
            {
                previousInput = SellingShortStrikePrice.Text;
            }
            else
            {
                SellingShortStrikePrice.Text = previousInput;
            }
        }

        private void BuyStrikePrice_TextChanged(object sender, EventArgs e)
        {
            string previousInput = "";
            Regex r = new Regex(@"^[+-]?\d*[.]?\d*$"); 
            Match m = r.Match(BuyStrikePrice.Text);
            if (m.Success)
            {
                previousInput = BuyStrikePrice.Text;
            }
            else
            {
                BuyStrikePrice.Text = previousInput;
            }
        }

        private void SellingShortStockID_TextChanged(object sender, EventArgs e)
        {
            string previousInput = "";
            Regex r = new Regex(@"^\+?[1-9][0-9]*$"); 
            Match m = r.Match(SellingShortStockID.Text);
            if (ConfigurationManager.AppSettings[SellingShortStockID.Text] != null)
            {
                SellingShortStockName.Text = ConfigurationManager.AppSettings[SellingShortStockID.Text];
            }
            if (SellingShortStockID.TextLength < 4)
            {
                SellingShortStockName.Text = "";
            }
            if (m.Success)
            {
                previousInput = SellingShortStockID.Text;
            }
            else
            {
                SellingShortStockID.Text = previousInput;
            }
        }

        private void BuyStockID_TextChanged(object sender, EventArgs e)
        {
            string previousInput = "";
            Regex r = new Regex(@"^\+?[1-9][0-9]*$"); 
            Match m = r.Match(BuyStockID.Text);
            if (ConfigurationManager.AppSettings[BuyStockID.Text]!=null)
            {
                BuyStockName.Text = ConfigurationManager.AppSettings[BuyStockID.Text];
            }
            if (BuyStockID.TextLength < 4)
            {
                BuyStockName.Text = "";
            }
            if (m.Success)
            {
                previousInput = BuyStockID.Text;
            }
            else
            {
                BuyStockID.Text = previousInput;
            }
        }

        private void showTaiExMA()
        {
            DateTime date = dateTimePicker1.Value;

            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand("Show_TaiExMA"))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    con.Open();
                    DataTable tempTaiEx = new DataTable();

                    
                    string dateS = date.ToString("yyyyMMdd");

                    cmd.Parameters.AddWithValue("@date1", dateS);
                                                
                    tempTaiEx.Load(cmd.ExecuteReader());
                                                
                    con.Close();

                    ArrayList listTaiEx = new ArrayList();

                    foreach (DataRow tempRow in tempTaiEx.Rows)
                    {
                        listTaiEx.Add(Convert.ToDouble(tempRow["TaiEx"]));                        
                    }

                    double todayMA = 0;
                    double lastdayMA = 0;

                    for(int i = 0; i < 5; i++)
                    {
                        todayMA += Convert.ToDouble(listTaiEx[i]);

                        lastdayMA += Convert.ToDouble(listTaiEx[i+1]);
                    }

                    double todayTaiEx = Convert.ToDouble(listTaiEx[0]);

                    double lastdayTaiEx = Convert.ToDouble(listTaiEx[1]);

                    TaiMA1.Text = dateS + " 大盤漲跌幅：" + (todayTaiEx - lastdayTaiEx).ToString(".00") + " 大盤MA趨勢：" + ((todayMA / 5) - (lastdayMA / 5)).ToString(".00");

                    TaiMA2.Text = dateS + " 大盤漲跌幅：" + (todayTaiEx - lastdayTaiEx).ToString(".00") + " 大盤MA趨勢：" + ((todayMA / 5) - (lastdayMA / 5)).ToString(".00");

                    TaiMA3.Text = dateS + " 大盤漲跌幅：" + (todayTaiEx - lastdayTaiEx).ToString(".00") + " 大盤MA趨勢：" + ((todayMA / 5) - (lastdayMA / 5)).ToString(".00");


                }
            }
        }

        public double getTaiExMA()
        {
            DateTime date = dateTimePicker1.Value;

            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand("Show_TaiExMA"))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    con.Open();
                    DataTable tempTaiEx = new DataTable();


                    string dateS = date.ToString("yyyyMMdd");

                    cmd.Parameters.AddWithValue("@date1", dateS);

                    tempTaiEx.Load(cmd.ExecuteReader());

                    con.Close();

                    ArrayList listTaiEx = new ArrayList();

                    foreach (DataRow tempRow in tempTaiEx.Rows)
                    {
                        listTaiEx.Add(Convert.ToDouble(tempRow["TaiEx"]));
                    }

                    double todayTaiEx = Convert.ToDouble(listTaiEx[0]);

                    double lastdayTaiEx = Convert.ToDouble(listTaiEx[1]);

                    return todayTaiEx - lastdayTaiEx;
                }

            }
        }

        public void Load_StockCombobox()
        {
            StockCombobox.Items.Add("持有股票列表");
            StockCombobox.Items.Add("建議賣出股票列表");

            StockCombobox.SelectedIndex = 0;

            showAllUserStockRecord();
        }

        private void StockCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (StockCombobox.SelectedIndex)
            {
                case 0 :
                    showAllUserStockRecord();
                    break;
                case 1:

                    if (getTaiExMA() < -50)
                    {
                        showAllUserStockRecord();
                    }
                    else
                    {
                        showUserStockRecord();
                    }
                    break;
            }

        }

        public void Load_SellingShortComboBox()
        {
            SellingShortComboBox.Items.Add("持有融券列表");
            SellingShortComboBox.Items.Add("建議回補融券列表");

            SellingShortComboBox.SelectedIndex = 0;

            showAllUserSellingShortRecord();
        }

        private void SellingShortComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

            switch (SellingShortComboBox.SelectedIndex)
            {
                case 0:
                    showAllUserSellingShortRecord();
                    break;
                case 1:

                    if (getTaiExMA() > 50)
                    {
                        showAllUserSellingShortRecord();
                    }
                    else
                    {
                        showUserSellingShortRecord();
                    }
                    break;
            }
        }

        
    }
}
