﻿namespace SrockOperator
{
    partial class Offset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.SellStockCount = new System.Windows.Forms.NumericUpDown();
            this.SellStrikePrice = new System.Windows.Forms.TextBox();
            this.SellStockDate = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.OffsetCancelBtn = new System.Windows.Forms.Button();
            this.UpdateStock = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.SellStockCount)).BeginInit();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(68, 78);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 12);
            this.label6.TabIndex = 30;
            this.label6.Text = "賣出股票日期";
            // 
            // SellStockCount
            // 
            this.SellStockCount.Location = new System.Drawing.Point(146, 110);
            this.SellStockCount.Margin = new System.Windows.Forms.Padding(2);
            this.SellStockCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.SellStockCount.Name = "SellStockCount";
            this.SellStockCount.Size = new System.Drawing.Size(94, 22);
            this.SellStockCount.TabIndex = 28;
            this.SellStockCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // SellStrikePrice
            // 
            this.SellStrikePrice.Location = new System.Drawing.Point(146, 145);
            this.SellStrikePrice.Margin = new System.Windows.Forms.Padding(2);
            this.SellStrikePrice.Name = "SellStrikePrice";
            this.SellStrikePrice.Size = new System.Drawing.Size(95, 22);
            this.SellStrikePrice.TabIndex = 29;
            // 
            // SellStockDate
            // 
            this.SellStockDate.CustomFormat = "yyyy/MM/dd";
            this.SellStockDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SellStockDate.Location = new System.Drawing.Point(146, 72);
            this.SellStockDate.Margin = new System.Windows.Forms.Padding(2);
            this.SellStockDate.Name = "SellStockDate";
            this.SellStockDate.Size = new System.Drawing.Size(95, 22);
            this.SellStockDate.TabIndex = 27;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(80, 147);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 12);
            this.label11.TabIndex = 26;
            this.label11.Text = "賣出成交價";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(68, 112);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 12);
            this.label10.TabIndex = 25;
            this.label10.Text = "賣出股票張數";
            // 
            // OffsetCancelBtn
            // 
            this.OffsetCancelBtn.Location = new System.Drawing.Point(183, 204);
            this.OffsetCancelBtn.Margin = new System.Windows.Forms.Padding(2);
            this.OffsetCancelBtn.Name = "OffsetCancelBtn";
            this.OffsetCancelBtn.Size = new System.Drawing.Size(56, 26);
            this.OffsetCancelBtn.TabIndex = 32;
            this.OffsetCancelBtn.Text = "取消";
            this.OffsetCancelBtn.UseVisualStyleBackColor = true;
            this.OffsetCancelBtn.Click += new System.EventHandler(this.OffsetCancelBtn_Click);
            // 
            // UpdateStock
            // 
            this.UpdateStock.Location = new System.Drawing.Point(70, 204);
            this.UpdateStock.Margin = new System.Windows.Forms.Padding(2);
            this.UpdateStock.Name = "UpdateStock";
            this.UpdateStock.Size = new System.Drawing.Size(56, 26);
            this.UpdateStock.TabIndex = 31;
            this.UpdateStock.Text = "送出";
            this.UpdateStock.UseVisualStyleBackColor = true;
            this.UpdateStock.Click += new System.EventHandler(this.UpdateStock_Click);
            // 
            // Offset
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(314, 297);
            this.ControlBox = false;
            this.Controls.Add(this.OffsetCancelBtn);
            this.Controls.Add(this.UpdateStock);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.SellStockCount);
            this.Controls.Add(this.SellStrikePrice);
            this.Controls.Add(this.SellStockDate);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Offset";
            this.Text = "平倉";
            this.Load += new System.EventHandler(this.Offset_Load);
            this.Resize += new System.EventHandler(this.Offset_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.SellStockCount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown SellStockCount;
        private System.Windows.Forms.TextBox SellStrikePrice;
        private System.Windows.Forms.DateTimePicker SellStockDate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button OffsetCancelBtn;
        private System.Windows.Forms.Button UpdateStock;
    }
}