﻿namespace SrockOperator
{
    partial class StockOperator
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.TaiMA3 = new System.Windows.Forms.Label();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.label16 = new System.Windows.Forms.Label();
            this.T86BuyGridView = new System.Windows.Forms.DataGridView();
            this.label17 = new System.Windows.Forms.Label();
            this.T86SellGridView = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.SellingShortComboBox = new System.Windows.Forms.ComboBox();
            this.StockCombobox = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.InsertSellingShort = new System.Windows.Forms.Button();
            this.SellingShortCount = new System.Windows.Forms.NumericUpDown();
            this.SellingShortStrikePrice = new System.Windows.Forms.TextBox();
            this.SellingShortDate = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.SellingShortStockID = new System.Windows.Forms.TextBox();
            this.SellingShortStockName = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.SellingShortView = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.InsertStock = new System.Windows.Forms.Button();
            this.BuyStockCount = new System.Windows.Forms.NumericUpDown();
            this.BuyStrikePrice = new System.Windows.Forms.TextBox();
            this.BuyStockDate = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.BuyStockID = new System.Windows.Forms.TextBox();
            this.BuyStockName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.CallLoginBtn = new System.Windows.Forms.Button();
            this.CheckLogin = new System.Windows.Forms.Label();
            this.StockRecordView = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.TaiMA2 = new System.Windows.Forms.Label();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.TaiMA1 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.T86BuyGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T86SellGridView)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SellingShortCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellingShortView)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BuyStockCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StockRecordView)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.Gainsboro;
            this.tabPage5.Controls.Add(this.TaiMA3);
            this.tabPage5.Controls.Add(this.dateTimePicker3);
            this.tabPage5.Controls.Add(this.label16);
            this.tabPage5.Controls.Add(this.T86BuyGridView);
            this.tabPage5.Controls.Add(this.label17);
            this.tabPage5.Controls.Add(this.T86SellGridView);
            this.tabPage5.Font = new System.Drawing.Font("新細明體", 10F);
            this.tabPage5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tabPage5.Location = new System.Drawing.Point(4, 25);
            this.tabPage5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage5.Size = new System.Drawing.Size(1213, 706);
            this.tabPage5.TabIndex = 3;
            this.tabPage5.Text = "三大法人比較";
            // 
            // TaiMA3
            // 
            this.TaiMA3.AutoSize = true;
            this.TaiMA3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.TaiMA3.Dock = System.Windows.Forms.DockStyle.Right;
            this.TaiMA3.Location = new System.Drawing.Point(1120, 2);
            this.TaiMA3.Name = "TaiMA3";
            this.TaiMA3.Size = new System.Drawing.Size(90, 17);
            this.TaiMA3.TabIndex = 6;
            this.TaiMA3.Text = "MAVALUE";
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dateTimePicker3.Location = new System.Drawing.Point(51, 35);
            this.dateTimePicker3.Margin = new System.Windows.Forms.Padding(4);
            this.dateTimePicker3.MinDate = new System.DateTime(2017, 6, 2, 0, 0, 0, 0);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(249, 27);
            this.dateTimePicker3.TabIndex = 0;
            this.dateTimePicker3.ValueChanged += new System.EventHandler(this.dateTimePicker3_ValueChanged);
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(51, 90);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(178, 17);
            this.label16.TabIndex = 5;
            this.label16.Text = "外資買超三大法人比較";
            // 
            // T86BuyGridView
            // 
            this.T86BuyGridView.AllowUserToAddRows = false;
            this.T86BuyGridView.AllowUserToDeleteRows = false;
            this.T86BuyGridView.AllowUserToResizeColumns = false;
            this.T86BuyGridView.AllowUserToResizeRows = false;
            this.T86BuyGridView.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.T86BuyGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.T86BuyGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.T86BuyGridView.Location = new System.Drawing.Point(51, 110);
            this.T86BuyGridView.Margin = new System.Windows.Forms.Padding(4);
            this.T86BuyGridView.MaximumSize = new System.Drawing.Size(3000, 1000);
            this.T86BuyGridView.Name = "T86BuyGridView";
            this.T86BuyGridView.ReadOnly = true;
            this.T86BuyGridView.RowTemplate.Height = 24;
            this.T86BuyGridView.Size = new System.Drawing.Size(1085, 265);
            this.T86BuyGridView.TabIndex = 2;
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(51, 400);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(178, 17);
            this.label17.TabIndex = 4;
            this.label17.Text = "外資賣超三大法人比較";
            // 
            // T86SellGridView
            // 
            this.T86SellGridView.AllowUserToAddRows = false;
            this.T86SellGridView.AllowUserToDeleteRows = false;
            this.T86SellGridView.AllowUserToResizeColumns = false;
            this.T86SellGridView.AllowUserToResizeRows = false;
            this.T86SellGridView.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.T86SellGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.T86SellGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.T86SellGridView.Location = new System.Drawing.Point(51, 420);
            this.T86SellGridView.Margin = new System.Windows.Forms.Padding(4);
            this.T86SellGridView.MaximumSize = new System.Drawing.Size(3000, 1000);
            this.T86SellGridView.Name = "T86SellGridView";
            this.T86SellGridView.ReadOnly = true;
            this.T86SellGridView.RowTemplate.Height = 24;
            this.T86SellGridView.Size = new System.Drawing.Size(1085, 265);
            this.T86SellGridView.TabIndex = 3;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.SellingShortComboBox);
            this.tabPage3.Controls.Add(this.StockCombobox);
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Controls.Add(this.SellingShortView);
            this.tabPage3.Controls.Add(this.groupBox1);
            this.tabPage3.Controls.Add(this.CallLoginBtn);
            this.tabPage3.Controls.Add(this.CheckLogin);
            this.tabPage3.Controls.Add(this.StockRecordView);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1213, 706);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "股票/融券";
            // 
            // SellingShortComboBox
            // 
            this.SellingShortComboBox.FormattingEnabled = true;
            this.SellingShortComboBox.Location = new System.Drawing.Point(635, 46);
            this.SellingShortComboBox.Name = "SellingShortComboBox";
            this.SellingShortComboBox.Size = new System.Drawing.Size(240, 23);
            this.SellingShortComboBox.TabIndex = 29;
            this.SellingShortComboBox.SelectedIndexChanged += new System.EventHandler(this.SellingShortComboBox_SelectedIndexChanged);
            // 
            // StockCombobox
            // 
            this.StockCombobox.FormattingEnabled = true;
            this.StockCombobox.Location = new System.Drawing.Point(47, 47);
            this.StockCombobox.Name = "StockCombobox";
            this.StockCombobox.Size = new System.Drawing.Size(237, 23);
            this.StockCombobox.TabIndex = 28;
            this.StockCombobox.SelectedIndexChanged += new System.EventHandler(this.StockCombobox_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.InsertSellingShort);
            this.groupBox2.Controls.Add(this.SellingShortCount);
            this.groupBox2.Controls.Add(this.SellingShortStrikePrice);
            this.groupBox2.Controls.Add(this.SellingShortDate);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.SellingShortStockID);
            this.groupBox2.Controls.Add(this.SellingShortStockName);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Location = new System.Drawing.Point(635, 396);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(552, 232);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "融券資訊";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 54);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 15);
            this.label7.TabIndex = 24;
            this.label7.Text = "融券股票日期";
            // 
            // InsertSellingShort
            // 
            this.InsertSellingShort.Location = new System.Drawing.Point(393, 178);
            this.InsertSellingShort.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.InsertSellingShort.Name = "InsertSellingShort";
            this.InsertSellingShort.Size = new System.Drawing.Size(96, 34);
            this.InsertSellingShort.TabIndex = 23;
            this.InsertSellingShort.Text = "登入融券";
            this.InsertSellingShort.UseVisualStyleBackColor = true;
            this.InsertSellingShort.Click += new System.EventHandler(this.InsertSellingShort_Click);
            // 
            // SellingShortCount
            // 
            this.SellingShortCount.Location = new System.Drawing.Point(371, 48);
            this.SellingShortCount.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SellingShortCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.SellingShortCount.Name = "SellingShortCount";
            this.SellingShortCount.Size = new System.Drawing.Size(125, 25);
            this.SellingShortCount.TabIndex = 21;
            this.SellingShortCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // SellingShortStrikePrice
            // 
            this.SellingShortStrikePrice.Location = new System.Drawing.Point(371, 112);
            this.SellingShortStrikePrice.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SellingShortStrikePrice.Name = "SellingShortStrikePrice";
            this.SellingShortStrikePrice.Size = new System.Drawing.Size(125, 25);
            this.SellingShortStrikePrice.TabIndex = 22;
            this.SellingShortStrikePrice.TextChanged += new System.EventHandler(this.SellingShortStrikePrice_TextChanged);
            // 
            // SellingShortDate
            // 
            this.SellingShortDate.CustomFormat = "yyyy/MM/dd";
            this.SellingShortDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.SellingShortDate.Location = new System.Drawing.Point(115, 48);
            this.SellingShortDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SellingShortDate.Name = "SellingShortDate";
            this.SellingShortDate.Size = new System.Drawing.Size(125, 25);
            this.SellingShortDate.TabIndex = 18;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(264, 118);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(82, 15);
            this.label12.TabIndex = 17;
            this.label12.Text = "融券成交價";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(11, 118);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(97, 15);
            this.label13.TabIndex = 14;
            this.label13.Text = "融券股票代號";
            // 
            // SellingShortStockID
            // 
            this.SellingShortStockID.Location = new System.Drawing.Point(115, 112);
            this.SellingShortStockID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SellingShortStockID.MaxLength = 4;
            this.SellingShortStockID.Name = "SellingShortStockID";
            this.SellingShortStockID.Size = new System.Drawing.Size(125, 25);
            this.SellingShortStockID.TabIndex = 19;
            this.SellingShortStockID.TextChanged += new System.EventHandler(this.SellingShortStockID_TextChanged);
            // 
            // SellingShortStockName
            // 
            this.SellingShortStockName.Location = new System.Drawing.Point(115, 176);
            this.SellingShortStockName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SellingShortStockName.Name = "SellingShortStockName";
            this.SellingShortStockName.Size = new System.Drawing.Size(125, 25);
            this.SellingShortStockName.TabIndex = 20;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(264, 54);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(97, 15);
            this.label14.TabIndex = 16;
            this.label14.Text = "融券股票張數";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(11, 182);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(97, 15);
            this.label15.TabIndex = 15;
            this.label15.Text = "融券股票名稱";
            // 
            // SellingShortView
            // 
            this.SellingShortView.AllowUserToAddRows = false;
            this.SellingShortView.AllowUserToDeleteRows = false;
            this.SellingShortView.AllowUserToResizeColumns = false;
            this.SellingShortView.AllowUserToResizeRows = false;
            this.SellingShortView.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SellingShortView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.SellingShortView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SellingShortView.Location = new System.Drawing.Point(635, 76);
            this.SellingShortView.Margin = new System.Windows.Forms.Padding(4);
            this.SellingShortView.MaximumSize = new System.Drawing.Size(3000, 1000);
            this.SellingShortView.Name = "SellingShortView";
            this.SellingShortView.ReadOnly = true;
            this.SellingShortView.RowTemplate.Height = 24;
            this.SellingShortView.Size = new System.Drawing.Size(552, 265);
            this.SellingShortView.TabIndex = 25;
            this.SellingShortView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.CoveringClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.InsertStock);
            this.groupBox1.Controls.Add(this.BuyStockCount);
            this.groupBox1.Controls.Add(this.BuyStrikePrice);
            this.groupBox1.Controls.Add(this.BuyStockDate);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.BuyStockID);
            this.groupBox1.Controls.Add(this.BuyStockName);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Location = new System.Drawing.Point(44, 396);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(552, 232);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "購入股票的資訊";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 54);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 15);
            this.label6.TabIndex = 24;
            this.label6.Text = "買入股票日期";
            // 
            // InsertStock
            // 
            this.InsertStock.Location = new System.Drawing.Point(393, 178);
            this.InsertStock.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.InsertStock.Name = "InsertStock";
            this.InsertStock.Size = new System.Drawing.Size(96, 34);
            this.InsertStock.TabIndex = 23;
            this.InsertStock.Text = "登入股票";
            this.InsertStock.UseVisualStyleBackColor = true;
            this.InsertStock.Click += new System.EventHandler(this.InsertStock_Click);
            // 
            // BuyStockCount
            // 
            this.BuyStockCount.Location = new System.Drawing.Point(371, 48);
            this.BuyStockCount.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BuyStockCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.BuyStockCount.Name = "BuyStockCount";
            this.BuyStockCount.Size = new System.Drawing.Size(125, 25);
            this.BuyStockCount.TabIndex = 21;
            this.BuyStockCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // BuyStrikePrice
            // 
            this.BuyStrikePrice.Location = new System.Drawing.Point(371, 112);
            this.BuyStrikePrice.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BuyStrikePrice.Name = "BuyStrikePrice";
            this.BuyStrikePrice.Size = new System.Drawing.Size(125, 25);
            this.BuyStrikePrice.TabIndex = 22;
            this.BuyStrikePrice.TextChanged += new System.EventHandler(this.BuyStrikePrice_TextChanged);
            // 
            // BuyStockDate
            // 
            this.BuyStockDate.CustomFormat = "yyyy/MM/dd";
            this.BuyStockDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.BuyStockDate.Location = new System.Drawing.Point(115, 48);
            this.BuyStockDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BuyStockDate.Name = "BuyStockDate";
            this.BuyStockDate.Size = new System.Drawing.Size(125, 25);
            this.BuyStockDate.TabIndex = 18;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(264, 118);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 15);
            this.label11.TabIndex = 17;
            this.label11.Text = "買入成交價";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 118);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 15);
            this.label8.TabIndex = 14;
            this.label8.Text = "買入股票代號";
            // 
            // BuyStockID
            // 
            this.BuyStockID.Location = new System.Drawing.Point(115, 112);
            this.BuyStockID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BuyStockID.MaxLength = 4;
            this.BuyStockID.Name = "BuyStockID";
            this.BuyStockID.Size = new System.Drawing.Size(125, 25);
            this.BuyStockID.TabIndex = 19;
            this.BuyStockID.TextChanged += new System.EventHandler(this.BuyStockID_TextChanged);
            // 
            // BuyStockName
            // 
            this.BuyStockName.Location = new System.Drawing.Point(115, 176);
            this.BuyStockName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BuyStockName.Name = "BuyStockName";
            this.BuyStockName.Size = new System.Drawing.Size(125, 25);
            this.BuyStockName.TabIndex = 20;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(264, 54);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(97, 15);
            this.label10.TabIndex = 16;
            this.label10.Text = "買入股票張數";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(11, 182);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(97, 15);
            this.label9.TabIndex = 15;
            this.label9.Text = "買入股票名稱";
            // 
            // CallLoginBtn
            // 
            this.CallLoginBtn.Location = new System.Drawing.Point(1091, 16);
            this.CallLoginBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CallLoginBtn.Name = "CallLoginBtn";
            this.CallLoginBtn.Size = new System.Drawing.Size(96, 34);
            this.CallLoginBtn.TabIndex = 11;
            this.CallLoginBtn.Text = "登入";
            this.CallLoginBtn.UseVisualStyleBackColor = true;
            this.CallLoginBtn.Click += new System.EventHandler(this.LoginBtn_Click);
            // 
            // CheckLogin
            // 
            this.CheckLogin.AutoSize = true;
            this.CheckLogin.Location = new System.Drawing.Point(44, 26);
            this.CheckLogin.Name = "CheckLogin";
            this.CheckLogin.Size = new System.Drawing.Size(52, 15);
            this.CheckLogin.TabIndex = 10;
            this.CheckLogin.Text = "未登入";
            // 
            // StockRecordView
            // 
            this.StockRecordView.AllowUserToAddRows = false;
            this.StockRecordView.AllowUserToDeleteRows = false;
            this.StockRecordView.AllowUserToResizeColumns = false;
            this.StockRecordView.AllowUserToResizeRows = false;
            this.StockRecordView.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.StockRecordView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.StockRecordView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.StockRecordView.Location = new System.Drawing.Point(47, 76);
            this.StockRecordView.Margin = new System.Windows.Forms.Padding(4);
            this.StockRecordView.MaximumSize = new System.Drawing.Size(3000, 1000);
            this.StockRecordView.Name = "StockRecordView";
            this.StockRecordView.ReadOnly = true;
            this.StockRecordView.RowTemplate.Height = 24;
            this.StockRecordView.Size = new System.Drawing.Size(552, 265);
            this.StockRecordView.TabIndex = 6;
            this.StockRecordView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.OffsetClick);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Gainsboro;
            this.tabPage2.Controls.Add(this.TaiMA2);
            this.tabPage2.Controls.Add(this.dataGridView4);
            this.tabPage2.Controls.Add(this.dataGridView3);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.dateTimePicker2);
            this.tabPage2.Font = new System.Drawing.Font("新細明體", 10F);
            this.tabPage2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Size = new System.Drawing.Size(1213, 706);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "外資買超->賣超/賣超->買超";
            // 
            // TaiMA2
            // 
            this.TaiMA2.AutoSize = true;
            this.TaiMA2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.TaiMA2.Dock = System.Windows.Forms.DockStyle.Right;
            this.TaiMA2.Location = new System.Drawing.Point(1120, 2);
            this.TaiMA2.Name = "TaiMA2";
            this.TaiMA2.Size = new System.Drawing.Size(90, 17);
            this.TaiMA2.TabIndex = 10;
            this.TaiMA2.Text = "MAVALUE";
            // 
            // dataGridView4
            // 
            this.dataGridView4.AllowUserToAddRows = false;
            this.dataGridView4.AllowUserToDeleteRows = false;
            this.dataGridView4.AllowUserToResizeColumns = false;
            this.dataGridView4.AllowUserToResizeRows = false;
            this.dataGridView4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dataGridView4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Location = new System.Drawing.Point(51, 420);
            this.dataGridView4.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView4.MaximumSize = new System.Drawing.Size(3000, 1000);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.ReadOnly = true;
            this.dataGridView4.RowTemplate.Height = 24;
            this.dataGridView4.Size = new System.Drawing.Size(1085, 265);
            this.dataGridView4.TabIndex = 9;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AllowUserToDeleteRows = false;
            this.dataGridView3.AllowUserToResizeColumns = false;
            this.dataGridView3.AllowUserToResizeRows = false;
            this.dataGridView3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dataGridView3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(51, 110);
            this.dataGridView3.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView3.MaximumSize = new System.Drawing.Size(3000, 1000);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.ReadOnly = true;
            this.dataGridView3.RowTemplate.Height = 24;
            this.dataGridView3.Size = new System.Drawing.Size(1085, 265);
            this.dataGridView3.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(51, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "外資賣超-->買超";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 400);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "外資買超-->賣超";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dateTimePicker2.Location = new System.Drawing.Point(51, 35);
            this.dateTimePicker2.Margin = new System.Windows.Forms.Padding(4);
            this.dateTimePicker2.MinDate = new System.DateTime(2017, 6, 2, 0, 0, 0, 0);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(249, 27);
            this.dateTimePicker2.TabIndex = 1;
            this.dateTimePicker2.ValueChanged += new System.EventHandler(this.dateTimePicker2_ValueChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Gainsboro;
            this.tabPage1.Controls.Add(this.TaiMA1);
            this.tabPage1.Controls.Add(this.dateTimePicker1);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.dataGridView2);
            this.tabPage1.Font = new System.Drawing.Font("新細明體", 10F);
            this.tabPage1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Size = new System.Drawing.Size(1213, 706);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "連續三個交易日外資買/賣超";
            // 
            // TaiMA1
            // 
            this.TaiMA1.AutoSize = true;
            this.TaiMA1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.TaiMA1.Dock = System.Windows.Forms.DockStyle.Right;
            this.TaiMA1.Location = new System.Drawing.Point(1120, 2);
            this.TaiMA1.Name = "TaiMA1";
            this.TaiMA1.Size = new System.Drawing.Size(90, 17);
            this.TaiMA1.TabIndex = 6;
            this.TaiMA1.Text = "MAVALUE";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dateTimePicker1.Location = new System.Drawing.Point(51, 35);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(4);
            this.dateTimePicker1.MinDate = new System.DateTime(2017, 6, 2, 0, 0, 0, 0);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(249, 27);
            this.dateTimePicker1.TabIndex = 0;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(51, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(195, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "連續三個交易日外資買超";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(51, 110);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.MaximumSize = new System.Drawing.Size(3000, 1000);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(1085, 265);
            this.dataGridView1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 400);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(195, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "連續三個交易日外資賣超";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeColumns = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(51, 420);
            this.dataGridView2.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView2.MaximumSize = new System.Drawing.Size(3000, 1000);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowTemplate.Height = 24;
            this.dataGridView2.Size = new System.Drawing.Size(1085, 265);
            this.dataGridView2.TabIndex = 3;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1221, 735);
            this.tabControl1.TabIndex = 6;
            // 
            // StockOperator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(1221, 735);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "StockOperator";
            this.Text = "StockOperator";
            this.Load += new System.EventHandler(this.StockOperator_Load);
            this.Resize += new System.EventHandler(this.StockOperator_Resize);
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.T86BuyGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T86SellGridView)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SellingShortCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellingShortView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BuyStockCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StockRecordView)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label TaiMA3;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DataGridView T86BuyGridView;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DataGridView T86SellGridView;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ComboBox SellingShortComboBox;
        private System.Windows.Forms.ComboBox StockCombobox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button InsertSellingShort;
        private System.Windows.Forms.NumericUpDown SellingShortCount;
        private System.Windows.Forms.TextBox SellingShortStrikePrice;
        private System.Windows.Forms.DateTimePicker SellingShortDate;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox SellingShortStockID;
        private System.Windows.Forms.TextBox SellingShortStockName;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridView SellingShortView;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button InsertStock;
        private System.Windows.Forms.NumericUpDown BuyStockCount;
        private System.Windows.Forms.TextBox BuyStrikePrice;
        private System.Windows.Forms.DateTimePicker BuyStockDate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox BuyStockID;
        private System.Windows.Forms.TextBox BuyStockName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button CallLoginBtn;
        private System.Windows.Forms.Label CheckLogin;
        private System.Windows.Forms.DataGridView StockRecordView;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label TaiMA1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Label TaiMA2;
    }
}

