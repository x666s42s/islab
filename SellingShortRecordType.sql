-- ================================
-- Create User-defined Table Type
-- ================================
USE StockOperator
GO

-- Create the data type
CREATE TYPE [dbo].[SellingShortRecord] AS TABLE 
(
	[UserID] [nvarchar](20) NOT NULL,
	[StockID] [nvarchar](20) NOT NULL,
	[StockName] [nvarchar](20) NOT NULL,
	[SellingShortDate] [datetime] NOT NULL,
	[SellingShortStrikePrice] [decimal](10, 2) NOT NULL,
	[SellingShortCount] [int] NOT NULL,
	[ShortCoveringDate] [datetime] NULL,
	[ShortCoveringStrikePrice] [decimal](10, 2) NULL,
	[ShortCoveringCount] [int] NOT NULL,
	[ProfitRate] [decimal](10, 2) NULL
)
GO


