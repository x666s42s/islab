CREATE PROCEDURE [dbo].[Insert_FGBuy]
      @insFGBuy FGBuyType READONLY
AS
BEGIN
     SET NOCOUNT ON;

      MERGE INTO FGBuy 
	  USING(
		  SELECT fgbuy_ID, Date, Rank, StockID, StockName, StrikePrice, UpAndDown, OverboughtCount, ForeignHoldingCount, ForeignHoldingWeight 
		  FROM @insFGBuy
	  )AS tempFGBuy
	  ON FGBuy.fgbuy_ID = tempFGBuy.fgbuy_ID
	  WHEN MATCHED THEN
	  UPDATE SET FGBuy.Date = tempFGBuy.Date, FGBuy.Rank = tempFGBuy.Rank, FGBuy.StockID = tempFGBuy.StockID, FGBuy.StockName = tempFGBuy.StockName, 
	  FGBuy.StrikePrice = tempFGBuy.StrikePrice, FGBuy.UpAndDown = tempFGBuy.UpAndDown, FGBuy.OverboughtCount = tempFGBuy.OverboughtCount, 
	  FGBuy.ForeignHoldingCount = tempFGBuy.ForeignHoldingCount, FGBuy.ForeignHoldingWeight = tempFGBuy.ForeignHoldingWeight
	  WHEN NOT MATCHED THEN
	  INSERT(fgbuy_ID, Date, Rank, StockID, StockName, StrikePrice, UpAndDown, OverboughtCount, ForeignHoldingCount, ForeignHoldingWeight) 
	  VALUES(tempFGBuy.fgbuy_ID, tempFGBuy.Date, tempFGBuy.Rank, tempFGBuy.StockID, tempFGBuy.StockName, tempFGBuy.StrikePrice, tempFGBuy.UpAndDown, tempFGBuy.OverboughtCount, tempFGBuy.ForeignHoldingCount, tempFGBuy.ForeignHoldingWeight);
END






