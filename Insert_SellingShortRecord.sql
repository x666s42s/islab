CREATE PROCEDURE [dbo].[Insert_SellingShortRecord]  
	@dt SellingShortRecordType READONLY
AS
BEGIN
	MERGE INTO SellingShortRecord
	  USING(
		SELECT *
		FROM @dt
	  ) AS temp
	  ON SellingShortRecord.UserID = temp.UserID AND SellingShortRecord.StockID = temp.StockID AND SellingShortRecord.SellingShortDate = temp.SellingShortDate AND SellingShortRecord.SellingShortStrikePrice = temp.SellingShortStrikePrice 

	  WHEN MATCHED THEN
	  UPDATE SET SellingShortRecord.SellingShortDate = temp.SellingShortDate, SellingShortRecord.SellingShortStrikePrice = temp.SellingShortStrikePrice, SellingShortRecord.SellingShortCount = temp.SellingShortCount, SellingShortRecord.ShortCoveringDate = temp.ShortCoveringDate, SellingShortRecord.ShortCoveringStrikePrice = temp.ShortCoveringStrikePrice, SellingShortRecord.ShortCoveringCount = temp.ShortCoveringCount, 
	  SellingShortRecord.[ProfitRate (%)] = ((temp.ShortCoveringStrikePrice - SellingShortRecord.SellingShortStrikePrice) / SellingShortRecord.SellingShortStrikePrice) * 100 

	  WHEN NOT MATCHED THEN
	  
	  INSERT(UserID, StockID, StockName, SellingShortDate, SellingShortStrikePrice, SellingShortCount, ShortCoveringDate, ShortCoveringStrikePrice, ShortCoveringCount, [ProfitRate (%)])
	  VALUES(temp.UserID, temp.StockID, temp.StockName, temp.SellingShortDate, temp.SellingShortStrikePrice, temp.SellingShortCount, temp.ShortCoveringDate, temp.ShortCoveringStrikePrice, temp.ShortCoveringCount, temp.ProfitRate);
END
GO
