use StockOperator
CREATE TYPE [dbo].[MSCIType] AS TABLE(
	[MSCI_ID] [nvarchar](20),
    [Date] [datetime] NOT NULL,
	[StockID] [nvarchar](10) NOT NULL,
	[StockName] [nvarchar](20),
	[StockPrice] [decimal](18, 2) NOT NULL,
	[UpAndDown] [nvarchar](20) NOT NULL,
	[UADRate] [nvarchar](20) NOT NULL,
	[Price] [nvarchar](20),
	[Weight] [decimal](18, 2) NOT NULL
)
GO





