/****** SSMS 中 SelectTopNRows 命令的指令碼  ******/
CREATE PROCEDURE [dbo].[Search_Change_FGBuyIntoFGSell] 
@dt Date
AS
BEGIN

	DECLARE @dt2 AS Date
	DECLARE @dt3 AS Date

	WHILE 1 = 1
	BEGIN
	IF (	
		SELECT count(*)
		  FROM [StockOperator].[dbo].[FGSell]
		  WHERE [Date] = @dt
		) <> 0
		BREAK
	ELSE 
		SET @dt = DATEADD(DAY,-1,@dt)
		CONTINUE
	END

	SET @dt2 = DATEADD(DAY,-1,@dt)

	WHILE 1 = 1
	BEGIN
	IF (	
		SELECT count(*)
		  FROM [StockOperator].[dbo].[FGBuy]
		  WHERE [Date] = @dt2
		) <> 0
		BREAK
	ELSE 
		SET @dt2 = DATEADD(DAY,-1,@dt2)
		CONTINUE
	END


	SELECT T2.Date, T1.Date, T1.StockID, T1.StockName, T1.StrikePrice, T2.OverboughtCount, T1.OversoldCount
	FROM (
		SELECT *
		FROM [dbo].[FGSell]
		WHERE [Date] = @dt
	) AS T1 INNER JOIN (
		SELECT *
		FROM [dbo].[FGBuy]
		WHERE [Date] = @dt2
	) AS T2 ON T1.StockID = T2.StockID 

	ORDER BY CONVERT (INT, T1.Rank)

END