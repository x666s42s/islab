CREATE PROCEDURE [dbo].[Insert_FGSell]
      @insFGSell FGSellType READONLY
AS
BEGIN
     SET NOCOUNT ON;

      MERGE INTO FGSell 
	  USING(
		  SELECT fgsell_ID, Date, Rank, StockID, StockName, StrikePrice, UpAndDown, OversoldCount, ForeignHoldingCount, ForeignHoldingWeight 
		  FROM @insFGSell
	  )AS tempFGSell
	  ON FGSell.fgsell_ID = tempFGSell.fgsell_ID
	  WHEN MATCHED THEN
	  UPDATE SET FGSell.Date = tempFGSell.Date, FGSell.Rank = tempFGSell.Rank, FGSell.StockID = tempFGSell.StockID, FGSell.StockName = tempFGSell.StockName, 
	  FGSell.StrikePrice = tempFGSell.StrikePrice, FGSell.UpAndDown = tempFGSell.UpAndDown, FGSell.OversoldCount = tempFGSell.OversoldCount, 
	  FGSell.ForeignHoldingCount = tempFGSell.ForeignHoldingCount, FGSell.ForeignHoldingWeight = tempFGSell.ForeignHoldingWeight
	  WHEN NOT MATCHED THEN
	  INSERT(fgsell_ID, Date, Rank, StockID, StockName, StrikePrice, UpAndDown, OversoldCount, ForeignHoldingCount, ForeignHoldingWeight) 
	  VALUES(tempFGSell.fgsell_ID, tempFGSell.Date, tempFGSell.Rank, tempFGSell.StockID, tempFGSell.StockName, tempFGSell.StrikePrice, tempFGSell.UpAndDown, tempFGSell.OversoldCount, tempFGSell.ForeignHoldingCount, tempFGSell.ForeignHoldingWeight);
END








