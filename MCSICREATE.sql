USE [StockOperator]
GO

/****** Object:  Table [dbo].[MSCI]    Script Date: 2017/5/21 下午 04:24:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MSCI](
	[MSCI_ID] [nvarchar](20) NOT NULL,
	[Date] [datetime] NOT NULL,
	[StockID] [nvarchar](10) NOT NULL,
	[StockName] [nvarchar](20) NULL,
	[StockPrice] [decimal](18, 2) NOT NULL,
	[UpAndDown] [nvarchar](20) NOT NULL,
	[UADRate] [nvarchar](20) NOT NULL,
	[Price] [nvarchar](20) NULL,
	[Weight] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_MCSI] PRIMARY KEY CLUSTERED 
(
	[MSCI_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO