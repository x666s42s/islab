﻿namespace StockFM
{
    partial class StockFM
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.showSellPieChart = new System.Windows.Forms.Button();
            this.showBuyPieChart = new System.Windows.Forms.Button();
            this.sellDaysGroupBox = new System.Windows.Forms.GroupBox();
            this.sixtyDaysSellRadiobtn = new System.Windows.Forms.RadioButton();
            this.twentyDaysSellRadiobtn = new System.Windows.Forms.RadioButton();
            this.fiveDaysSellRadiobtn = new System.Windows.Forms.RadioButton();
            this.buyDaysGroupBox = new System.Windows.Forms.GroupBox();
            this.sixtyDaysBuyRadiobtn = new System.Windows.Forms.RadioButton();
            this.twentyDaysBuyRadiobtn = new System.Windows.Forms.RadioButton();
            this.fiveDaysBuyRadiobtn = new System.Windows.Forms.RadioButton();
            this.sellComboBox = new System.Windows.Forms.ComboBox();
            this.buyComboBox = new System.Windows.Forms.ComboBox();
            this.TaiMA1 = new System.Windows.Forms.Label();
            this.stockDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.buyDataGridView = new System.Windows.Forms.DataGridView();
            this.sellDataGridView = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.SearchBtn = new System.Windows.Forms.Button();
            this.SearchBox = new System.Windows.Forms.TextBox();
            this.CheckBoxGroup = new System.Windows.Forms.GroupBox();
            this.ShowSumT86CheckBox = new System.Windows.Forms.CheckBox();
            this.ShowT86CheckBox = new System.Windows.Forms.CheckBox();
            this.SearchResultStockIDStockName = new System.Windows.Forms.Label();
            this.StockChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.searchStockDatePicker = new System.Windows.Forms.DateTimePicker();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.sellDaysGroupBox.SuspendLayout();
            this.buyDaysGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buyDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sellDataGridView)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.CheckBoxGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StockChart)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("微軟正黑體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1036, 682);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.showSellPieChart);
            this.tabPage1.Controls.Add(this.showBuyPieChart);
            this.tabPage1.Controls.Add(this.sellDaysGroupBox);
            this.tabPage1.Controls.Add(this.buyDaysGroupBox);
            this.tabPage1.Controls.Add(this.sellComboBox);
            this.tabPage1.Controls.Add(this.buyComboBox);
            this.tabPage1.Controls.Add(this.TaiMA1);
            this.tabPage1.Controls.Add(this.stockDateTimePicker);
            this.tabPage1.Controls.Add(this.buyDataGridView);
            this.tabPage1.Controls.Add(this.sellDataGridView);
            this.tabPage1.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tabPage1.Location = new System.Drawing.Point(4, 27);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage1.Size = new System.Drawing.Size(1028, 651);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "外資行為";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // showSellPieChart
            // 
            this.showSellPieChart.Location = new System.Drawing.Point(201, 357);
            this.showSellPieChart.Margin = new System.Windows.Forms.Padding(2);
            this.showSellPieChart.Name = "showSellPieChart";
            this.showSellPieChart.Size = new System.Drawing.Size(97, 25);
            this.showSellPieChart.TabIndex = 29;
            this.showSellPieChart.Text = "看比例圖";
            this.showSellPieChart.UseVisualStyleBackColor = true;
            this.showSellPieChart.Visible = false;
            this.showSellPieChart.Click += new System.EventHandler(this.showSellPieChart_Click);
            // 
            // showBuyPieChart
            // 
            this.showBuyPieChart.Location = new System.Drawing.Point(201, 49);
            this.showBuyPieChart.Margin = new System.Windows.Forms.Padding(2);
            this.showBuyPieChart.Name = "showBuyPieChart";
            this.showBuyPieChart.Size = new System.Drawing.Size(97, 25);
            this.showBuyPieChart.TabIndex = 28;
            this.showBuyPieChart.Text = "看比例圖";
            this.showBuyPieChart.UseVisualStyleBackColor = true;
            this.showBuyPieChart.Visible = false;
            this.showBuyPieChart.Click += new System.EventHandler(this.showBuyPieChart_Click);
            // 
            // sellDaysGroupBox
            // 
            this.sellDaysGroupBox.Controls.Add(this.sixtyDaysSellRadiobtn);
            this.sellDaysGroupBox.Controls.Add(this.twentyDaysSellRadiobtn);
            this.sellDaysGroupBox.Controls.Add(this.fiveDaysSellRadiobtn);
            this.sellDaysGroupBox.Location = new System.Drawing.Point(849, 341);
            this.sellDaysGroupBox.Name = "sellDaysGroupBox";
            this.sellDaysGroupBox.Size = new System.Drawing.Size(172, 42);
            this.sellDaysGroupBox.TabIndex = 27;
            this.sellDaysGroupBox.TabStop = false;
            this.sellDaysGroupBox.Text = "選擇天數";
            this.sellDaysGroupBox.Visible = false;
            // 
            // sixtyDaysSellRadiobtn
            // 
            this.sixtyDaysSellRadiobtn.AutoSize = true;
            this.sixtyDaysSellRadiobtn.Location = new System.Drawing.Point(112, 18);
            this.sixtyDaysSellRadiobtn.Margin = new System.Windows.Forms.Padding(2);
            this.sixtyDaysSellRadiobtn.Name = "sixtyDaysSellRadiobtn";
            this.sixtyDaysSellRadiobtn.Size = new System.Drawing.Size(56, 22);
            this.sixtyDaysSellRadiobtn.TabIndex = 26;
            this.sixtyDaysSellRadiobtn.TabStop = true;
            this.sixtyDaysSellRadiobtn.Text = "60日";
            this.sixtyDaysSellRadiobtn.UseVisualStyleBackColor = true;
            this.sixtyDaysSellRadiobtn.CheckedChanged += new System.EventHandler(this.sixtyDaysSellRadiobtn_CheckedChanged);
            // 
            // twentyDaysSellRadiobtn
            // 
            this.twentyDaysSellRadiobtn.AutoSize = true;
            this.twentyDaysSellRadiobtn.Location = new System.Drawing.Point(55, 18);
            this.twentyDaysSellRadiobtn.Margin = new System.Windows.Forms.Padding(2);
            this.twentyDaysSellRadiobtn.Name = "twentyDaysSellRadiobtn";
            this.twentyDaysSellRadiobtn.Size = new System.Drawing.Size(56, 22);
            this.twentyDaysSellRadiobtn.TabIndex = 25;
            this.twentyDaysSellRadiobtn.TabStop = true;
            this.twentyDaysSellRadiobtn.Text = "20日";
            this.twentyDaysSellRadiobtn.UseVisualStyleBackColor = true;
            this.twentyDaysSellRadiobtn.CheckedChanged += new System.EventHandler(this.twentyDaysSellRadiobtn_CheckedChanged);
            // 
            // fiveDaysSellRadiobtn
            // 
            this.fiveDaysSellRadiobtn.AutoSize = true;
            this.fiveDaysSellRadiobtn.Checked = true;
            this.fiveDaysSellRadiobtn.Location = new System.Drawing.Point(5, 18);
            this.fiveDaysSellRadiobtn.Margin = new System.Windows.Forms.Padding(2);
            this.fiveDaysSellRadiobtn.Name = "fiveDaysSellRadiobtn";
            this.fiveDaysSellRadiobtn.Size = new System.Drawing.Size(48, 22);
            this.fiveDaysSellRadiobtn.TabIndex = 24;
            this.fiveDaysSellRadiobtn.TabStop = true;
            this.fiveDaysSellRadiobtn.Text = "5日";
            this.fiveDaysSellRadiobtn.UseVisualStyleBackColor = true;
            this.fiveDaysSellRadiobtn.CheckedChanged += new System.EventHandler(this.fiveDaysSellRadiobtn_CheckedChanged);
            // 
            // buyDaysGroupBox
            // 
            this.buyDaysGroupBox.Controls.Add(this.sixtyDaysBuyRadiobtn);
            this.buyDaysGroupBox.Controls.Add(this.twentyDaysBuyRadiobtn);
            this.buyDaysGroupBox.Controls.Add(this.fiveDaysBuyRadiobtn);
            this.buyDaysGroupBox.Location = new System.Drawing.Point(849, 31);
            this.buyDaysGroupBox.Name = "buyDaysGroupBox";
            this.buyDaysGroupBox.Size = new System.Drawing.Size(172, 42);
            this.buyDaysGroupBox.TabIndex = 23;
            this.buyDaysGroupBox.TabStop = false;
            this.buyDaysGroupBox.Text = "選擇天數";
            this.buyDaysGroupBox.Visible = false;
            // 
            // sixtyDaysBuyRadiobtn
            // 
            this.sixtyDaysBuyRadiobtn.AutoSize = true;
            this.sixtyDaysBuyRadiobtn.Location = new System.Drawing.Point(112, 18);
            this.sixtyDaysBuyRadiobtn.Margin = new System.Windows.Forms.Padding(2);
            this.sixtyDaysBuyRadiobtn.Name = "sixtyDaysBuyRadiobtn";
            this.sixtyDaysBuyRadiobtn.Size = new System.Drawing.Size(56, 22);
            this.sixtyDaysBuyRadiobtn.TabIndex = 26;
            this.sixtyDaysBuyRadiobtn.TabStop = true;
            this.sixtyDaysBuyRadiobtn.Text = "60日";
            this.sixtyDaysBuyRadiobtn.UseVisualStyleBackColor = true;
            this.sixtyDaysBuyRadiobtn.CheckedChanged += new System.EventHandler(this.sixtyDaysBuyRadiobtn_CheckedChanged);
            // 
            // twentyDaysBuyRadiobtn
            // 
            this.twentyDaysBuyRadiobtn.AutoSize = true;
            this.twentyDaysBuyRadiobtn.Location = new System.Drawing.Point(55, 18);
            this.twentyDaysBuyRadiobtn.Margin = new System.Windows.Forms.Padding(2);
            this.twentyDaysBuyRadiobtn.Name = "twentyDaysBuyRadiobtn";
            this.twentyDaysBuyRadiobtn.Size = new System.Drawing.Size(56, 22);
            this.twentyDaysBuyRadiobtn.TabIndex = 25;
            this.twentyDaysBuyRadiobtn.TabStop = true;
            this.twentyDaysBuyRadiobtn.Text = "20日";
            this.twentyDaysBuyRadiobtn.UseVisualStyleBackColor = true;
            this.twentyDaysBuyRadiobtn.CheckedChanged += new System.EventHandler(this.twentyDaysBuyRadiobtn_CheckedChanged);
            // 
            // fiveDaysBuyRadiobtn
            // 
            this.fiveDaysBuyRadiobtn.AutoSize = true;
            this.fiveDaysBuyRadiobtn.Checked = true;
            this.fiveDaysBuyRadiobtn.Location = new System.Drawing.Point(5, 18);
            this.fiveDaysBuyRadiobtn.Margin = new System.Windows.Forms.Padding(2);
            this.fiveDaysBuyRadiobtn.Name = "fiveDaysBuyRadiobtn";
            this.fiveDaysBuyRadiobtn.Size = new System.Drawing.Size(48, 22);
            this.fiveDaysBuyRadiobtn.TabIndex = 24;
            this.fiveDaysBuyRadiobtn.TabStop = true;
            this.fiveDaysBuyRadiobtn.Text = "5日";
            this.fiveDaysBuyRadiobtn.UseVisualStyleBackColor = true;
            this.fiveDaysBuyRadiobtn.CheckedChanged += new System.EventHandler(this.fiveDaysBuyRadiobtn_CheckedChanged);
            // 
            // sellComboBox
            // 
            this.sellComboBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.sellComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.sellComboBox.FormattingEnabled = true;
            this.sellComboBox.Items.AddRange(new object[] {
            "外資連續賣超",
            "外資買超 -> 賣超",
            "外資賣超與法人賣超比較",
            "外資賣超總量排名"});
            this.sellComboBox.Location = new System.Drawing.Point(9, 358);
            this.sellComboBox.Margin = new System.Windows.Forms.Padding(2);
            this.sellComboBox.Name = "sellComboBox";
            this.sellComboBox.Size = new System.Drawing.Size(188, 26);
            this.sellComboBox.TabIndex = 20;
            this.sellComboBox.SelectedIndexChanged += new System.EventHandler(this.sellComboBox_SelectedIndexChanged);
            // 
            // buyComboBox
            // 
            this.buyComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.buyComboBox.FormattingEnabled = true;
            this.buyComboBox.Items.AddRange(new object[] {
            "外資連續買超",
            "外資賣超 -> 買超",
            "外資買超與法人買超比較",
            "外資買超總量排名"});
            this.buyComboBox.Location = new System.Drawing.Point(9, 49);
            this.buyComboBox.Margin = new System.Windows.Forms.Padding(2);
            this.buyComboBox.Name = "buyComboBox";
            this.buyComboBox.Size = new System.Drawing.Size(188, 26);
            this.buyComboBox.TabIndex = 19;
            this.buyComboBox.SelectedIndexChanged += new System.EventHandler(this.buyComboBox_SelectedIndexChanged);
            // 
            // TaiMA1
            // 
            this.TaiMA1.AutoSize = true;
            this.TaiMA1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.TaiMA1.Dock = System.Windows.Forms.DockStyle.Right;
            this.TaiMA1.Location = new System.Drawing.Point(950, 2);
            this.TaiMA1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.TaiMA1.Name = "TaiMA1";
            this.TaiMA1.Size = new System.Drawing.Size(76, 18);
            this.TaiMA1.TabIndex = 18;
            this.TaiMA1.Text = "MAVALUE";
            // 
            // stockDateTimePicker
            // 
            this.stockDateTimePicker.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.stockDateTimePicker.Font = new System.Drawing.Font("微軟正黑體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.stockDateTimePicker.Location = new System.Drawing.Point(9, 14);
            this.stockDateTimePicker.MinDate = new System.DateTime(2017, 6, 2, 0, 0, 0, 0);
            this.stockDateTimePicker.Name = "stockDateTimePicker";
            this.stockDateTimePicker.Size = new System.Drawing.Size(188, 27);
            this.stockDateTimePicker.TabIndex = 15;
            this.stockDateTimePicker.ValueChanged += new System.EventHandler(this.stockDateTimePicker_ValueChanged);
            // 
            // buyDataGridView
            // 
            this.buyDataGridView.AllowUserToAddRows = false;
            this.buyDataGridView.AllowUserToDeleteRows = false;
            this.buyDataGridView.AllowUserToResizeColumns = false;
            this.buyDataGridView.AllowUserToResizeRows = false;
            this.buyDataGridView.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buyDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.buyDataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.buyDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.buyDataGridView.Location = new System.Drawing.Point(9, 88);
            this.buyDataGridView.MaximumSize = new System.Drawing.Size(2250, 800);
            this.buyDataGridView.Name = "buyDataGridView";
            this.buyDataGridView.ReadOnly = true;
            this.buyDataGridView.RowTemplate.Height = 24;
            this.buyDataGridView.Size = new System.Drawing.Size(1012, 240);
            this.buyDataGridView.TabIndex = 16;
            // 
            // sellDataGridView
            // 
            this.sellDataGridView.AllowUserToAddRows = false;
            this.sellDataGridView.AllowUserToDeleteRows = false;
            this.sellDataGridView.AllowUserToResizeColumns = false;
            this.sellDataGridView.AllowUserToResizeRows = false;
            this.sellDataGridView.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.sellDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.sellDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sellDataGridView.Location = new System.Drawing.Point(9, 396);
            this.sellDataGridView.MaximumSize = new System.Drawing.Size(2250, 800);
            this.sellDataGridView.Name = "sellDataGridView";
            this.sellDataGridView.ReadOnly = true;
            this.sellDataGridView.RowTemplate.Height = 24;
            this.sellDataGridView.Size = new System.Drawing.Size(1012, 240);
            this.sellDataGridView.TabIndex = 17;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.SearchBtn);
            this.tabPage2.Controls.Add(this.SearchBox);
            this.tabPage2.Controls.Add(this.CheckBoxGroup);
            this.tabPage2.Controls.Add(this.SearchResultStockIDStockName);
            this.tabPage2.Controls.Add(this.StockChart);
            this.tabPage2.Controls.Add(this.searchStockDatePicker);
            this.tabPage2.Location = new System.Drawing.Point(4, 27);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage2.Size = new System.Drawing.Size(1028, 651);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "個股查詢";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.Location = new System.Drawing.Point(6, 58);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 19);
            this.label1.TabIndex = 1;
            this.label1.Text = "股票代號/名稱";
            // 
            // SearchBtn
            // 
            this.SearchBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SearchBtn.AutoSize = true;
            this.SearchBtn.BackColor = System.Drawing.Color.Transparent;
            this.SearchBtn.BackgroundImage = global::StockFM.Properties.Resources.search1600;
            this.SearchBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SearchBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SearchBtn.Location = new System.Drawing.Point(228, 58);
            this.SearchBtn.Margin = new System.Windows.Forms.Padding(2);
            this.SearchBtn.Name = "SearchBtn";
            this.SearchBtn.Size = new System.Drawing.Size(20, 20);
            this.SearchBtn.TabIndex = 18;
            this.SearchBtn.UseVisualStyleBackColor = false;
            this.SearchBtn.Click += new System.EventHandler(this.SearchBtn_Click);
            // 
            // SearchBox
            // 
            this.SearchBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SearchBox.AutoCompleteCustomSource.AddRange(new string[] {
            "1101",
            "1102",
            "1103",
            "1104",
            "1108",
            "1109",
            "1110",
            "1201",
            "1203",
            "1210",
            "1213",
            "1215",
            "1216",
            "1217",
            "1218",
            "1219",
            "1220",
            "1225",
            "1227",
            "1229",
            "1231",
            "1232",
            "1233",
            "1234",
            "1235",
            "1236",
            "1256",
            "1262",
            "1301",
            "1303",
            "1304",
            "1305",
            "1307",
            "1308",
            "1309",
            "1310",
            "1312",
            "1313",
            "1314",
            "1315",
            "1316",
            "1319",
            "1321",
            "1323",
            "1324",
            "1325",
            "1326",
            "1337",
            "1338",
            "1339",
            "1340",
            "1402",
            "1409",
            "1410",
            "1413",
            "1414",
            "1416",
            "1417",
            "1418",
            "1419",
            "1423",
            "1432",
            "1434",
            "1435",
            "1436",
            "1437",
            "1438",
            "1439",
            "1440",
            "1441",
            "1442",
            "1443",
            "1444",
            "1445",
            "1446",
            "1447",
            "1449",
            "1451",
            "1452",
            "1453",
            "1454",
            "1455",
            "1456",
            "1457",
            "1459",
            "1460",
            "1463",
            "1464",
            "1465",
            "1466",
            "1467",
            "1468",
            "1469",
            "1470",
            "1471",
            "1472",
            "1473",
            "1474",
            "1475",
            "1476",
            "1477",
            "1503",
            "1504",
            "1506",
            "1507",
            "1512",
            "1513",
            "1514",
            "1515",
            "1516",
            "1517",
            "1519",
            "1521",
            "1522",
            "1524",
            "1525",
            "1526",
            "1527",
            "1528",
            "1529",
            "1530",
            "1531",
            "1532",
            "1533",
            "1535",
            "1536",
            "1537",
            "1538",
            "1539",
            "1540",
            "1541",
            "1558",
            "1560",
            "1568",
            "1582",
            "1583",
            "1589",
            "1590",
            "1592",
            "1598",
            "1603",
            "1604",
            "1605",
            "1608",
            "1609",
            "1611",
            "1612",
            "1613",
            "1614",
            "1615",
            "1616",
            "1617",
            "1618",
            "1626",
            "1701",
            "1702",
            "1704",
            "1707",
            "1708",
            "1709",
            "1710",
            "1711",
            "1712",
            "1713",
            "1714",
            "1715",
            "1717",
            "1718",
            "1720",
            "1721",
            "1722",
            "1723",
            "1724",
            "1725",
            "1726",
            "1727",
            "1729",
            "1730",
            "1731",
            "1732",
            "1733",
            "1734",
            "1735",
            "1736",
            "1737",
            "1762",
            "1773",
            "1776",
            "1783",
            "1786",
            "1789",
            "1802",
            "1805",
            "1806",
            "1808",
            "1809",
            "1810",
            "1817",
            "1902",
            "1903",
            "1904",
            "1905",
            "1906",
            "1907",
            "1909",
            "2002",
            "2006",
            "2007",
            "2008",
            "2009",
            "2010",
            "2012",
            "2013",
            "2014",
            "2015",
            "2017",
            "2020",
            "2022",
            "2023",
            "2024",
            "2025",
            "2027",
            "2028",
            "2029",
            "2030",
            "2031",
            "2032",
            "2033",
            "2034",
            "2038",
            "2049",
            "2059",
            "2062",
            "2069",
            "2101",
            "2102",
            "2103",
            "2104",
            "2105",
            "2106",
            "2107",
            "2108",
            "2109",
            "2114",
            "2115",
            "2201",
            "2204",
            "2206",
            "2207",
            "2208",
            "2227",
            "2228",
            "2231",
            "2236",
            "2239",
            "2301",
            "2302",
            "2303",
            "2305",
            "2308",
            "2311",
            "2312",
            "2313",
            "2314",
            "2316",
            "2317",
            "2321",
            "2323",
            "2324",
            "2325",
            "2327",
            "2328",
            "2329",
            "2330",
            "2331",
            "2332",
            "2337",
            "2338",
            "2340",
            "2342",
            "2344",
            "2345",
            "2347",
            "2348",
            "2349",
            "2351",
            "2352",
            "2353",
            "2354",
            "2355",
            "2356",
            "2357",
            "2358",
            "2359",
            "2360",
            "2362",
            "2363",
            "2364",
            "2365",
            "2367",
            "2368",
            "2369",
            "2371",
            "2373",
            "2374",
            "2375",
            "2376",
            "2377",
            "2379",
            "2380",
            "2382",
            "2383",
            "2385",
            "2387",
            "2388",
            "2390",
            "2392",
            "2393",
            "2395",
            "2397",
            "2399",
            "2401",
            "2402",
            "2404",
            "2405",
            "2406",
            "2408",
            "2409",
            "2412",
            "2413",
            "2414",
            "2415",
            "2417",
            "2419",
            "2420",
            "2421",
            "2423",
            "2424",
            "2425",
            "2426",
            "2427",
            "2428",
            "2429",
            "2430",
            "2431",
            "2433",
            "2434",
            "2436",
            "2438",
            "2439",
            "2440",
            "2441",
            "2442",
            "2443",
            "2444",
            "2448",
            "2449",
            "2450",
            "2451",
            "2453",
            "2454",
            "2455",
            "2456",
            "2457",
            "2458",
            "2459",
            "2460",
            "2461",
            "2462",
            "2464",
            "2465",
            "2466",
            "2467",
            "2468",
            "2471",
            "2472",
            "2474",
            "2475",
            "2476",
            "2477",
            "2478",
            "2480",
            "2481",
            "2482",
            "2483",
            "2484",
            "2485",
            "2486",
            "2488",
            "2489",
            "2491",
            "2492",
            "2493",
            "2495",
            "2496",
            "2497",
            "2498",
            "2499",
            "2501",
            "2504",
            "2505",
            "2506",
            "2509",
            "2511",
            "2514",
            "2515",
            "2516",
            "2520",
            "2524",
            "2527",
            "2528",
            "2530",
            "2534",
            "2535",
            "2536",
            "2537",
            "2538",
            "2539",
            "2540",
            "2542",
            "2543",
            "2545",
            "2546",
            "2547",
            "2548",
            "2597",
            "2601",
            "2603",
            "2605",
            "2606",
            "2607",
            "2608",
            "2609",
            "2610",
            "2611",
            "2612",
            "2613",
            "2614",
            "2615",
            "2616",
            "2617",
            "2618",
            "2633",
            "2634",
            "2636",
            "2637",
            "2642",
            "2701",
            "2702",
            "2704",
            "2705",
            "2706",
            "2707",
            "2712",
            "2722",
            "2723",
            "2727",
            "2731",
            "2739",
            "2748",
            "2801",
            "2809",
            "2812",
            "2816",
            "2820",
            "2823",
            "2832",
            "2834",
            "2836",
            "2838",
            "2841",
            "2845",
            "2849",
            "2850",
            "2851",
            "2852",
            "2855",
            "2856",
            "2867",
            "2880",
            "2881",
            "2882",
            "2883",
            "2884",
            "2885",
            "2886",
            "2887",
            "2888",
            "2889",
            "2890",
            "2891",
            "2892",
            "2897",
            "2901",
            "2903",
            "2904",
            "2905",
            "2906",
            "2908",
            "2910",
            "2911",
            "2912",
            "2913",
            "2915",
            "2923",
            "2929",
            "2936",
            "3002",
            "3003",
            "3004",
            "3005",
            "3006",
            "3008",
            "3010",
            "3011",
            "3013",
            "3014",
            "3015",
            "3016",
            "3017",
            "3018",
            "3019",
            "3021",
            "3022",
            "3023",
            "3024",
            "3025",
            "3026",
            "3027",
            "3028",
            "3029",
            "3030",
            "3031",
            "3032",
            "3033",
            "3034",
            "3035",
            "3036",
            "3037",
            "3038",
            "3040",
            "3041",
            "3042",
            "3043",
            "3044",
            "3045",
            "3046",
            "3047",
            "3048",
            "3049",
            "3050",
            "3051",
            "3052",
            "3054",
            "3055",
            "3056",
            "3057",
            "3058",
            "3059",
            "3060",
            "3062",
            "3090",
            "3094",
            "3130",
            "3149",
            "3164",
            "3167",
            "3189",
            "3209",
            "3229",
            "3231",
            "3257",
            "3266",
            "3296",
            "3305",
            "3308",
            "3311",
            "3312",
            "3315",
            "3321",
            "3338",
            "3346",
            "3356",
            "3376",
            "3380",
            "3383",
            "3406",
            "3413",
            "3416",
            "3419",
            "3432",
            "3437",
            "3443",
            "3450",
            "3454",
            "3481",
            "3494",
            "3501",
            "3504",
            "3514",
            "3515",
            "3518",
            "3519",
            "3528",
            "3532",
            "3533",
            "3535",
            "3536",
            "3545",
            "3550",
            "3557",
            "3559",
            "3561",
            "3576",
            "3579",
            "3583",
            "3588",
            "3591",
            "3593",
            "3596",
            "3605",
            "3607",
            "3617",
            "3622",
            "3645",
            "3653",
            "3661",
            "3665",
            "3669",
            "3673",
            "3679",
            "3682",
            "3686",
            "3694",
            "3698",
            "3701",
            "3702",
            "3703",
            "3704",
            "3705",
            "3706",
            "3708",
            "4104",
            "4106",
            "4108",
            "4119",
            "4133",
            "4137",
            "4141",
            "4142",
            "4144",
            "4148",
            "4164",
            "4190",
            "4306",
            "4414",
            "4426",
            "4438",
            "4526",
            "4532",
            "4536",
            "4545",
            "4551",
            "4552",
            "4555",
            "4557",
            "4560",
            "4720",
            "4722",
            "4725",
            "4737",
            "4746",
            "4755",
            "4763",
            "4807",
            "4904",
            "4906",
            "4912",
            "4915",
            "4916",
            "4919",
            "4927",
            "4930",
            "4934",
            "4935",
            "4938",
            "4942",
            "4943",
            "4952",
            "4956",
            "4958",
            "4960",
            "4968",
            "4976",
            "4977",
            "4984",
            "4994",
            "4999",
            "5007",
            "5203",
            "5215",
            "5225",
            "5234",
            "5243",
            "5258",
            "5259",
            "5264",
            "5269",
            "5284",
            "5285",
            "5288",
            "5305",
            "5388",
            "5434",
            "5469",
            "5471",
            "5484",
            "5515",
            "5519",
            "5521",
            "5522",
            "5525",
            "5531",
            "5533",
            "5534",
            "5538",
            "5607",
            "5608",
            "5706",
            "5871",
            "5880",
            "5906",
            "5907",
            "6005",
            "6108",
            "6112",
            "6115",
            "6116",
            "6117",
            "6120",
            "6128",
            "6131",
            "6133",
            "6136",
            "6139",
            "6141",
            "6142",
            "6145",
            "6152",
            "6153",
            "6155",
            "6164",
            "6165",
            "6166",
            "6168",
            "6172",
            "6176",
            "6177",
            "6183",
            "6184",
            "6189",
            "6191",
            "6192",
            "6196",
            "6197",
            "6201",
            "6202",
            "6205",
            "6206",
            "6209",
            "6213",
            "6214",
            "6215",
            "6216",
            "6224",
            "6225",
            "6226",
            "6230",
            "6235",
            "6239",
            "6243",
            "6251",
            "6257",
            "6269",
            "6271",
            "6277",
            "6278",
            "6281",
            "6282",
            "6283",
            "6285",
            "6289",
            "6405",
            "6409",
            "6412",
            "6414",
            "6415",
            "6422",
            "6431",
            "6442",
            "6443",
            "6449",
            "6451",
            "6452",
            "6456",
            "6464",
            "6477",
            "6504",
            "6505",
            "6525",
            "6531",
            "6533",
            "6552",
            "6582",
            "6605",
            "8011",
            "8016",
            "8021",
            "8033",
            "8039",
            "8046",
            "8070",
            "8072",
            "8081",
            "8101",
            "8103",
            "8105",
            "8110",
            "8112",
            "8114",
            "8131",
            "8150",
            "8163",
            "8201",
            "8210",
            "8213",
            "8215",
            "8222",
            "8249",
            "8261",
            "8271",
            "8341",
            "8374",
            "8404",
            "8411",
            "8422",
            "8427",
            "8429",
            "8442",
            "8443",
            "8454",
            "8463",
            "8464",
            "8466",
            "8467",
            "8473",
            "8480",
            "8481",
            "8488",
            "8926",
            "8940",
            "8996",
            "9802",
            "9902",
            "9904",
            "9905",
            "9906",
            "9907",
            "9908",
            "9910",
            "9911",
            "9912",
            "9914",
            "9917",
            "9918",
            "9919",
            "9921",
            "9924",
            "9925",
            "9926",
            "9927",
            "9928",
            "9929",
            "9930",
            "9931",
            "9933",
            "9934",
            "9935",
            "9937",
            "9938",
            "9939",
            "9940",
            "9941",
            "9942",
            "9943",
            "9944",
            "9945",
            "9946",
            "9955",
            "9958",
            "台泥",
            "亞泥",
            "嘉泥",
            "環泥",
            "幸福",
            "信大",
            "東泥",
            "味全",
            "味王",
            "大成",
            "大飲",
            "卜蜂",
            "統一",
            "愛之味",
            "泰山",
            "福壽",
            "台榮",
            "福懋油",
            "佳格",
            "聯華",
            "聯華食",
            "大統益",
            "天仁",
            "黑松",
            "興泰",
            "宏亞",
            "鮮活果汁-KY",
            "綠悅-KY",
            "台塑",
            "南亞",
            "台聚",
            "華夏",
            "三芳",
            "亞聚",
            "台達化",
            "台苯",
            "國喬",
            "聯成",
            "中石化",
            "達新",
            "上曜",
            "東陽",
            "大洋",
            "永裕",
            "地球",
            "恆大",
            "台化",
            "再生-KY",
            "廣華-KY",
            "昭輝",
            "勝悅-KY",
            "遠東新",
            "新纖",
            "南染",
            "宏洲",
            "東和",
            "廣豐",
            "嘉裕",
            "東華",
            "新紡",
            "利華",
            "大魯閣",
            "福懋",
            "中福",
            "華友聯",
            "勤益控",
            "裕豐",
            "中和",
            "南紡",
            "大東",
            "名軒",
            "立益",
            "力麗",
            "大宇",
            "宏和",
            "力鵬",
            "佳和",
            "年興",
            "宏益",
            "大將",
            "台富",
            "集盛",
            "怡華",
            "宜進",
            "聯發",
            "宏遠",
            "強盛",
            "得力",
            "偉全",
            "聚隆",
            "南緯",
            "昶和",
            "理隆",
            "大統染",
            "首利",
            "三洋紡",
            "台南",
            "弘裕",
            "本盟",
            "儒鴻",
            "聚陽",
            "士電",
            "東元",
            "正道",
            "永大",
            "瑞利",
            "中興電",
            "亞力",
            "力山",
            "川飛",
            "利奇",
            "華城",
            "大億",
            "堤維西",
            "耿鼎",
            "江申",
            "日馳",
            "鑽全",
            "恩德",
            "樂士",
            "亞崴",
            "高林股",
            "勤美",
            "車王電",
            "中宇",
            "和大",
            "廣隆",
            "正峰新",
            "巨庭",
            "喬福",
            "錩泰",
            "伸興",
            "中砂",
            "倉佑",
            "信錦",
            "程泰",
            "永冠-KY",
            "亞德客-KY",
            "英瑞-KY",
            "岱宇",
            "華電",
            "聲寶",
            "華新",
            "華榮",
            "大亞",
            "中電",
            "宏泰",
            "台一",
            "三洋電",
            "大山",
            "億泰",
            "榮星",
            "合機",
            "艾美特-KY",
            "中化",
            "南僑",
            "榮化",
            "葡萄王",
            "東鹼",
            "和益",
            "東聯",
            "永光",
            "興農",
            "國化",
            "和桐",
            "萬洲",
            "長興",
            "中纖",
            "生達",
            "三晃",
            "台肥",
            "中碳",
            "台硝",
            "元禎",
            "永記",
            "中華化",
            "必翔",
            "花仙子",
            "美吾華",
            "毛寶",
            "五鼎",
            "杏輝",
            "日勝化",
            "喬山",
            "臺鹽",
            "中化生",
            "勝一",
            "展宇",
            "和康生",
            "科妍",
            "神隆",
            "台玻",
            "寶徠",
            "冠軍",
            "潤隆",
            "中釉",
            "和成",
            "凱撒衛",
            "台紙",
            "士紙",
            "正隆",
            "華紙",
            "寶隆",
            "永豐餘",
            "榮成",
            "中鋼",
            "東和鋼鐵",
            "燁興",
            "高興昌",
            "第一銅",
            "春源",
            "春雨",
            "中鋼構",
            "中鴻",
            "豐興",
            "官田鋼",
            "美亞",
            "聚亨",
            "燁輝",
            "志聯",
            "千興",
            "大成鋼",
            "威致",
            "盛餘",
            "彰源",
            "新光鋼",
            "新鋼",
            "佳大",
            "允強",
            "海光",
            "上銀",
            "川湖",
            "橋椿",
            "運錩",
            "南港",
            "泰豐",
            "台橡",
            "中橡",
            "正新",
            "建大",
            "厚生",
            "南帝",
            "華豐",
            "鑫永銓",
            "六暉-KY",
            "裕隆",
            "中華",
            "三陽工業",
            "和泰車",
            "台船",
            "裕日車",
            "劍麟",
            "為升",
            "百達-KY",
            "英利-KY",
            "光寶科",
            "麗正",
            "聯電",
            "全友",
            "台達電",
            "日月光",
            "金寶",
            "華通",
            "台揚",
            "楠梓電",
            "鴻海",
            "東訊",
            "中環",
            "仁寶",
            "矽品",
            "國巨",
            "廣宇",
            "華泰",
            "台積電",
            "精英",
            "友訊",
            "旺宏",
            "光罩",
            "光磊",
            "茂矽",
            "華邦電",
            "智邦",
            "聯強",
            "海悅",
            "錸德",
            "順德",
            "佳世達",
            "宏碁",
            "鴻準",
            "敬鵬",
            "英業達",
            "華碩",
            "廷鑫",
            "所羅門",
            "致茂",
            "藍天",
            "矽統",
            "倫飛",
            "昆盈",
            "燿華",
            "金像電",
            "菱生",
            "大同",
            "震旦行",
            "佳能",
            "智寶",
            "技嘉",
            "微星",
            "瑞昱",
            "虹光",
            "廣達",
            "台光電",
            "群光",
            "精元",
            "威盛",
            "云辰",
            "正崴",
            "億光",
            "研華",
            "友通",
            "映泰",
            "凌陽",
            "毅嘉",
            "漢唐",
            "浩鑫",
            "國碩",
            "南亞科",
            "友達",
            "中華電",
            "環科",
            "精技",
            "錩新",
            "圓剛",
            "仲琦",
            "新巨",
            "建準",
            "固緯",
            "隴華",
            "承啟",
            "鼎元",
            "三商電",
            "興勤",
            "銘旺科",
            "燦坤",
            "聯昌",
            "互盛電",
            "統懋",
            "偉詮電",
            "翔耀",
            "美律",
            "太空梭",
            "超豐",
            "新美齊",
            "新利虹",
            "友旺",
            "晶電",
            "京元電子",
            "神腦",
            "創見",
            "凌群",
            "聯發科",
            "全新",
            "奇力新",
            "飛宏",
            "義隆",
            "敦吉",
            "建通",
            "光群雷",
            "良得電",
            "盟立",
            "麗臺",
            "冠西電",
            "志聖",
            "華經",
            "資通",
            "立隆電",
            "可成",
            "華映",
            "鉅祥",
            "美隆電",
            "大毅",
            "敦陽科",
            "強茂",
            "連宇",
            "百容",
            "希華",
            "兆赫",
            "一詮",
            "漢平",
            "瑞軒",
            "吉祥全",
            "華新科",
            "揚博",
            "普安",
            "卓越",
            "怡利電",
            "宏達電",
            "東貝",
            "國建",
            "國產",
            "國揚",
            "太設",
            "全坤建",
            "太子",
            "龍邦",
            "中工",
            "新建",
            "冠德",
            "京城",
            "宏璟",
            "皇普",
            "華建",
            "宏盛",
            "達欣工",
            "宏普",
            "聯上發",
            "基泰",
            "櫻花建",
            "愛山林",
            "興富發",
            "皇昌",
            "皇翔",
            "根基",
            "日勝生",
            "華固",
            "潤弘",
            "益航",
            "長榮",
            "新興",
            "裕民",
            "榮運",
            "大榮",
            "陽明",
            "華航",
            "志信",
            "中航",
            "中櫃",
            "東森",
            "萬海",
            "山隆",
            "台航",
            "長榮航",
            "台灣高鐵",
            "漢翔",
            "台驊",
            "慧洋-KY",
            "宅配通",
            "萬企",
            "華園",
            "國賓",
            "六福",
            "第一店",
            "晶華",
            "遠雄來",
            "夏都",
            "美食-KY",
            "王品",
            "雄獅",
            "寒舍",
            "雲品",
            "彰銀",
            "京城銀",
            "台中銀",
            "旺旺保",
            "華票",
            "中壽",
            "台產",
            "臺企銀",
            "高雄銀",
            "聯邦銀",
            "台開",
            "遠東銀",
            "安泰銀",
            "新產",
            "中再保",
            "第一保",
            "統一證",
            "元富證",
            "三商壽",
            "華南金",
            "富邦金",
            "國泰金",
            "開發金",
            "玉山金",
            "元大金",
            "兆豐金",
            "台新金",
            "新光金",
            "國票金",
            "永豐金",
            "中信金",
            "第一金",
            "王道銀行",
            "欣欣",
            "遠百",
            "匯僑",
            "三商",
            "高林",
            "特力",
            "統領",
            "麗嬰房",
            "統一超",
            "農林",
            "潤泰全",
            "鼎固-KY",
            "淘帝-KY",
            "客思達-KY",
            "歐格",
            "健和興",
            "豐達科",
            "神基",
            "晶豪科",
            "大立光",
            "華立",
            "今皓",
            "晟銘電",
            "聯陽",
            "全漢",
            "嘉晶",
            "奇鋐",
            "同開",
            "亞光",
            "鴻名",
            "威強電",
            "信邦",
            "憶聲",
            "星通",
            "禾伸堂",
            "盛達",
            "增你強",
            "零壹",
            "德律",
            "佰鴻",
            "偉訓",
            "威健",
            "聯詠",
            "智原",
            "文曄",
            "欣興",
            "全台",
            "遠見",
            "揚智",
            "晶技",
            "科風",
            "健鼎",
            "台灣大",
            "建碁",
            "訊舟",
            "益登",
            "和鑫",
            "鈺德",
            "力特",
            "夆典",
            "立萬利",
            "蔚華科",
            "總太",
            "喬鼎",
            "立德",
            "華晶科",
            "銘異",
            "建漢",
            "日電貿",
            "聯傑",
            "一零四",
            "正達",
            "景岳",
            "大量",
            "景碩",
            "全科",
            "晟鈦",
            "緯創",
            "虹冠電",
            "昇陽",
            "勝德",
            "昇貿",
            "聯德",
            "閎暉",
            "弘憶股",
            "宣昶",
            "同泰",
            "泰碩",
            "麗清",
            "奇偶",
            "新日興",
            "明泰",
            "新世紀",
            "玉晶光",
            "京鼎",
            "融程電",
            "譁裕",
            "台端",
            "榮創",
            "創意",
            "聯鈞",
            "晶睿",
            "群創",
            "誠研",
            "維熹",
            "揚明光",
            "昱晶",
            "華擎",
            "柏騰",
            "綠能",
            "安馳",
            "台勝科",
            "嘉澤",
            "晶彩科",
            "誠創",
            "敦泰",
            "聯穎",
            "嘉威",
            "全智科",
            "昇陽光電",
            "新日光",
            "尚志",
            "辛耘",
            "通嘉",
            "艾笛森",
            "力銘",
            "智易",
            "宏致",
            "谷崧",
            "碩天",
            "洋華",
            "達邁",
            "健策",
            "世芯-KY",
            "貿聯-KY",
            "圓展",
            "TPK-KY",
            "新至陞",
            "亞太電",
            "達能",
            "海華",
            "隆達",
            "大眾控",
            "大聯大",
            "欣陸",
            "合勤控",
            "永信",
            "神達",
            "上緯投控",
            "佳醫",
            "雃博",
            "懷特",
            "旭富",
            "亞諾法",
            "麗豐-KY",
            "龍燈-KY",
            "國光生",
            "康聯-KY",
            "全宇生技-KY",
            "承業醫",
            "佐登-KY",
            "炎洲",
            "如興",
            "利勤",
            "廣越",
            "東台",
            "瑞智",
            "拓凱",
            "銘鈺",
            "智伸科",
            "力達-KY",
            "氣立",
            "永新-KY",
            "強信-KY",
            "德淵",
            "國精化",
            "信昌化",
            "華廣",
            "台耀",
            "三福化",
            "材料-KY",
            "日成-KY",
            "遠傳",
            "正文",
            "聯德控股-KY",
            "致伸",
            "事欣科",
            "新唐",
            "泰鼎-KY",
            "燦星網",
            "太極",
            "茂林-KY",
            "和碩",
            "嘉彰",
            "康控-KY",
            "凌通",
            "光鋐",
            "臻鼎-KY",
            "奇美材",
            "立積",
            "佳凌",
            "眾達-KY",
            "科納-KY",
            "傳奇",
            "鑫禾",
            "三星",
            "訊連",
            "科嘉-KY",
            "東科-KY",
            "達興材料",
            "乙盛-KY",
            "虹堡",
            "清惠",
            "鎧勝-KY",
            "祥碩",
            "jpp-KY",
            "界霖",
            "豐祥-KY",
            "敦南",
            "中磊",
            "崇越",
            "瀚宇博",
            "松翰",
            "慧友",
            "建國",
            "隆大",
            "工信",
            "遠雄",
            "順天",
            "鄉林",
            "皇鼎",
            "長虹",
            "東明-KY",
            "遠雄港",
            "四維航",
            "鳳凰",
            "中租-KY",
            "合庫金",
            "台南-KY",
            "大洋-KY",
            "群益證",
            "競國",
            "聚碩",
            "鎰勝",
            "彩晶",
            "迎廣",
            "達運",
            "上福",
            "悠克",
            "金橋",
            "富爾特",
            "亞翔",
            "柏承",
            "友勁",
            "勁永",
            "百一",
            "嘉聯益",
            "鈞寶",
            "華興",
            "捷泰",
            "凌華",
            "宏齊",
            "互億",
            "瑞儀",
            "達麗",
            "關貿",
            "大豐電",
            "豐藝",
            "精成科",
            "巨路",
            "帆宣",
            "佳必琪",
            "亞弘電",
            "盛群",
            "詮欣",
            "飛捷",
            "今國光",
            "聯茂",
            "精誠",
            "和椿",
            "居易",
            "聚鼎",
            "天瀚",
            "光鼎",
            "超眾",
            "華孚",
            "力成",
            "迅杰",
            "定穎",
            "矽格",
            "台郡",
            "同欣電",
            "宏正",
            "台表科",
            "全國電",
            "康舒",
            "淳安",
            "啟碁",
            "華上",
            "悅城",
            "旭隼",
            "群電",
            "樺漢",
            "矽力-KY",
            "君耀-KY",
            "光麗-KY",
            "光聖",
            "元晶",
            "鈺邦",
            "訊芯-KY",
            "康友-KY",
            "GIS-KY",
            "台數科",
            "安集",
            "南六",
            "台塑化",
            "捷敏-KY",
            "愛普",
            "晶心科",
            "易華電",
            "申豐",
            "帝寶",
            "台通",
            "矽創",
            "尖點",
            "雷虎",
            "台虹",
            "南電",
            "長華",
            "陞泰",
            "致新",
            "華冠",
            "瀚荃",
            "凌巨",
            "華東",
            "至上",
            "振樺電",
            "福懋科",
            "南茂",
            "達方",
            "無敵",
            "勤誠",
            "志超",
            "明基材",
            "寶一",
            "菱光",
            "富鼎",
            "宇瞻",
            "日友",
            "羅昇",
            "百和興業-KY",
            "福貞-KY",
            "可寧衛",
            "基勝-KY",
            "金麗-KY",
            "威宏-KY",
            "阿瘦",
            "富邦媒",
            "潤泰材",
            "億豐",
            "美喆-KY",
            "波力-KY",
            "山林水",
            "泰昇-KY",
            "政伸",
            "吉源-KY",
            "台汽電",
            "新天地",
            "高力",
            "鈺齊-KY",
            "台火",
            "寶成",
            "大華",
            "欣巴巴",
            "統一實",
            "大台北",
            "豐泰",
            "櫻花",
            "偉聯",
            "美利達",
            "中保",
            "欣天然",
            "康那香",
            "巨大",
            "福興",
            "新保",
            "新海",
            "泰銘",
            "中視",
            "秋雨",
            "中聯資源",
            "欣高",
            "中鼎",
            "成霖",
            "慶豐富",
            "全國",
            "百和",
            "宏全",
            "信義",
            "裕融",
            "茂順",
            "好樂迪",
            "新麗",
            "潤泰新",
            "三發地產",
            "佳龍",
            "世紀鋼"});
            this.SearchBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.SearchBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.SearchBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.SearchBox.Location = new System.Drawing.Point(121, 56);
            this.SearchBox.Margin = new System.Windows.Forms.Padding(2);
            this.SearchBox.MaxLength = 4;
            this.SearchBox.Name = "SearchBox";
            this.SearchBox.Size = new System.Drawing.Size(104, 27);
            this.SearchBox.TabIndex = 0;
            this.SearchBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SearchBox_KeyDown);
            // 
            // CheckBoxGroup
            // 
            this.CheckBoxGroup.Controls.Add(this.ShowSumT86CheckBox);
            this.CheckBoxGroup.Controls.Add(this.ShowT86CheckBox);
            this.CheckBoxGroup.Location = new System.Drawing.Point(659, 22);
            this.CheckBoxGroup.Name = "CheckBoxGroup";
            this.CheckBoxGroup.Size = new System.Drawing.Size(325, 45);
            this.CheckBoxGroup.TabIndex = 22;
            this.CheckBoxGroup.TabStop = false;
            this.CheckBoxGroup.Text = "圖表顯示";
            // 
            // ShowSumT86CheckBox
            // 
            this.ShowSumT86CheckBox.AutoSize = true;
            this.ShowSumT86CheckBox.Checked = true;
            this.ShowSumT86CheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowSumT86CheckBox.Location = new System.Drawing.Point(146, 19);
            this.ShowSumT86CheckBox.Name = "ShowSumT86CheckBox";
            this.ShowSumT86CheckBox.Size = new System.Drawing.Size(178, 23);
            this.ShowSumT86CheckBox.TabIndex = 22;
            this.ShowSumT86CheckBox.Text = "三大法人買賣超累積量";
            this.ShowSumT86CheckBox.UseVisualStyleBackColor = true;
            this.ShowSumT86CheckBox.CheckedChanged += new System.EventHandler(this.ShowSumT86CheckBox_CheckedChanged);
            // 
            // ShowT86CheckBox
            // 
            this.ShowT86CheckBox.AutoSize = true;
            this.ShowT86CheckBox.Checked = true;
            this.ShowT86CheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowT86CheckBox.Location = new System.Drawing.Point(6, 19);
            this.ShowT86CheckBox.Name = "ShowT86CheckBox";
            this.ShowT86CheckBox.Size = new System.Drawing.Size(133, 23);
            this.ShowT86CheckBox.TabIndex = 21;
            this.ShowT86CheckBox.Text = "三大法人買賣超";
            this.ShowT86CheckBox.UseVisualStyleBackColor = true;
            this.ShowT86CheckBox.CheckedChanged += new System.EventHandler(this.ShowT86CheckBox_CheckedChanged);
            // 
            // SearchResultStockIDStockName
            // 
            this.SearchResultStockIDStockName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SearchResultStockIDStockName.AutoSize = true;
            this.SearchResultStockIDStockName.Font = new System.Drawing.Font("微軟正黑體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SearchResultStockIDStockName.Location = new System.Drawing.Point(253, 56);
            this.SearchResultStockIDStockName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.SearchResultStockIDStockName.Name = "SearchResultStockIDStockName";
            this.SearchResultStockIDStockName.Size = new System.Drawing.Size(52, 25);
            this.SearchResultStockIDStockName.TabIndex = 19;
            this.SearchResultStockIDStockName.Text = "查詢";
            // 
            // StockChart
            // 
            chartArea1.AlignmentStyle = System.Windows.Forms.DataVisualization.Charting.AreaAlignmentStyles.Position;
            chartArea1.Name = "StockChart";
            chartArea1.Visible = false;
            chartArea2.AlignWithChartArea = "SumT86Chart";
            chartArea2.Name = "T86Chart";
            chartArea3.AlignWithChartArea = "T86Chart";
            chartArea3.Name = "SumT86Chart";
            this.StockChart.ChartAreas.Add(chartArea1);
            this.StockChart.ChartAreas.Add(chartArea2);
            this.StockChart.ChartAreas.Add(chartArea3);
            legend1.Enabled = false;
            legend1.Name = "StockPrice";
            legend2.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend2.LegendItemOrder = System.Windows.Forms.DataVisualization.Charting.LegendItemOrder.SameAsSeriesOrder;
            legend2.LegendStyle = System.Windows.Forms.DataVisualization.Charting.LegendStyle.Row;
            legend2.Name = "T86";
            legend3.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend3.Name = "SumT86";
            this.StockChart.Legends.Add(legend1);
            this.StockChart.Legends.Add(legend2);
            this.StockChart.Legends.Add(legend3);
            this.StockChart.Location = new System.Drawing.Point(9, 86);
            this.StockChart.Margin = new System.Windows.Forms.Padding(2);
            this.StockChart.Name = "StockChart";
            series1.BorderWidth = 3;
            series1.ChartArea = "T86Chart";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.IsValueShownAsLabel = true;
            series1.Legend = "T86";
            series1.MarkerBorderColor = System.Drawing.Color.Black;
            series1.MarkerColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            series1.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series1.Name = "外資";
            series1.SmartLabelStyle.AllowOutsidePlotArea = System.Windows.Forms.DataVisualization.Charting.LabelOutsidePlotAreaStyle.Yes;
            series1.SmartLabelStyle.CalloutLineAnchorCapStyle = System.Windows.Forms.DataVisualization.Charting.LineAnchorCapStyle.None;
            series1.SmartLabelStyle.Enabled = false;
            series2.BorderWidth = 3;
            series2.ChartArea = "T86Chart";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Legend = "T86";
            series2.MarkerBorderColor = System.Drawing.Color.Black;
            series2.MarkerColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            series2.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series2.Name = "投信";
            series2.SmartLabelStyle.AllowOutsidePlotArea = System.Windows.Forms.DataVisualization.Charting.LabelOutsidePlotAreaStyle.Yes;
            series2.SmartLabelStyle.CalloutLineAnchorCapStyle = System.Windows.Forms.DataVisualization.Charting.LineAnchorCapStyle.None;
            series2.SmartLabelStyle.Enabled = false;
            series3.BorderWidth = 3;
            series3.ChartArea = "T86Chart";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Color = System.Drawing.Color.Lime;
            series3.Legend = "T86";
            series3.MarkerBorderColor = System.Drawing.Color.Black;
            series3.MarkerColor = System.Drawing.Color.Lime;
            series3.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series3.Name = "自營商";
            series3.SmartLabelStyle.AllowOutsidePlotArea = System.Windows.Forms.DataVisualization.Charting.LabelOutsidePlotAreaStyle.Yes;
            series3.SmartLabelStyle.CalloutLineAnchorCapStyle = System.Windows.Forms.DataVisualization.Charting.LineAnchorCapStyle.None;
            series3.SmartLabelStyle.Enabled = false;
            series4.BorderWidth = 3;
            series4.ChartArea = "T86Chart";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            series4.Legend = "T86";
            series4.MarkerBorderColor = System.Drawing.Color.Black;
            series4.MarkerColor = System.Drawing.Color.Red;
            series4.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            series4.Name = "自營商避險";
            series4.SmartLabelStyle.AllowOutsidePlotArea = System.Windows.Forms.DataVisualization.Charting.LabelOutsidePlotAreaStyle.Yes;
            series4.SmartLabelStyle.CalloutLineAnchorCapStyle = System.Windows.Forms.DataVisualization.Charting.LineAnchorCapStyle.None;
            series4.SmartLabelStyle.Enabled = false;
            series5.BorderWidth = 3;
            series5.ChartArea = "StockChart";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Candlestick;
            series5.Legend = "StockPrice";
            series5.Name = "股價";
            series5.YValuesPerPoint = 4;
            series6.ChartArea = "SumT86Chart";
            series6.IsValueShownAsLabel = true;
            series6.Legend = "SumT86";
            series6.Name = "外資買賣超總和";
            series7.ChartArea = "SumT86Chart";
            series7.IsValueShownAsLabel = true;
            series7.Legend = "SumT86";
            series7.Name = "投信買賣超總和";
            series8.ChartArea = "SumT86Chart";
            series8.IsValueShownAsLabel = true;
            series8.Legend = "SumT86";
            series8.Name = "自營商買賣超總和";
            series9.ChartArea = "SumT86Chart";
            series9.IsValueShownAsLabel = true;
            series9.Legend = "SumT86";
            series9.Name = "自營商避險總和";
            this.StockChart.Series.Add(series1);
            this.StockChart.Series.Add(series2);
            this.StockChart.Series.Add(series3);
            this.StockChart.Series.Add(series4);
            this.StockChart.Series.Add(series5);
            this.StockChart.Series.Add(series6);
            this.StockChart.Series.Add(series7);
            this.StockChart.Series.Add(series8);
            this.StockChart.Series.Add(series9);
            this.StockChart.Size = new System.Drawing.Size(1012, 560);
            this.StockChart.TabIndex = 17;
            // 
            // searchStockDatePicker
            // 
            this.searchStockDatePicker.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.searchStockDatePicker.Location = new System.Drawing.Point(9, 14);
            this.searchStockDatePicker.MinDate = new System.DateTime(2017, 6, 2, 0, 0, 0, 0);
            this.searchStockDatePicker.Name = "searchStockDatePicker";
            this.searchStockDatePicker.Size = new System.Drawing.Size(188, 27);
            this.searchStockDatePicker.TabIndex = 16;
            this.searchStockDatePicker.ValueChanged += new System.EventHandler(this.searchStockDatePicker_ValueChanged);
            // 
            // StockFM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1036, 682);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "StockFM";
            this.ShowIcon = false;
            this.Text = "StockFM";
            this.Load += new System.EventHandler(this.StockFM_Load);
            this.Resize += new System.EventHandler(this.StockFM_Resize);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.sellDaysGroupBox.ResumeLayout(false);
            this.sellDaysGroupBox.PerformLayout();
            this.buyDaysGroupBox.ResumeLayout(false);
            this.buyDaysGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buyDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sellDataGridView)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.CheckBoxGroup.ResumeLayout(false);
            this.CheckBoxGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StockChart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ComboBox sellComboBox;
        private System.Windows.Forms.ComboBox buyComboBox;
        private System.Windows.Forms.Label TaiMA1;
        private System.Windows.Forms.DateTimePicker stockDateTimePicker;
        private System.Windows.Forms.DataGridView buyDataGridView;
        private System.Windows.Forms.DataGridView sellDataGridView;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox SearchBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker searchStockDatePicker;
        private System.Windows.Forms.DataVisualization.Charting.Chart StockChart;
        private System.Windows.Forms.Button SearchBtn;
        private System.Windows.Forms.Label SearchResultStockIDStockName;
        private System.Windows.Forms.GroupBox CheckBoxGroup;
        private System.Windows.Forms.CheckBox ShowSumT86CheckBox;
        private System.Windows.Forms.CheckBox ShowT86CheckBox;
        private System.Windows.Forms.GroupBox buyDaysGroupBox;
        private System.Windows.Forms.RadioButton fiveDaysBuyRadiobtn;
        private System.Windows.Forms.RadioButton sixtyDaysBuyRadiobtn;
        private System.Windows.Forms.RadioButton twentyDaysBuyRadiobtn;
        private System.Windows.Forms.GroupBox sellDaysGroupBox;
        private System.Windows.Forms.RadioButton sixtyDaysSellRadiobtn;
        private System.Windows.Forms.RadioButton twentyDaysSellRadiobtn;
        private System.Windows.Forms.RadioButton fiveDaysSellRadiobtn;
        private System.Windows.Forms.Button showSellPieChart;
        private System.Windows.Forms.Button showBuyPieChart;
    }
}

