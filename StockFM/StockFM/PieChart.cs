﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StockFM
{
    public partial class PieChartForm : Form
    {

        public PieChartForm(DataTable tempDt, string date, int checkTable, bool BuyOrSell)
        {
            InitializeComponent();

            int i = 0;
            foreach (DataRow a in tempDt.Rows)
            {
                PieChart.Series[0].Points.AddY(a[6].ToString());
                PieChart.Series[0].Points[i].LegendText = a["股票名稱"].ToString();
                i++;
                //                PieChart.Series[0].Points.AddXY(a[1].ToString(), a[5].ToString());
            }

            PieChart.Series[0].Label = "#PERCENT";
            PieChart.Series[0]["PieLabelStyle"] = "OUTSIDE";
            PieChart.Series[0].BorderColor = Color.Black;

            Font font = new Font("Times new Romen", 12);

            switch (checkTable)
            {
                case 0:
                    
                    if (BuyOrSell)
                    {
                        

                        PieChart.Titles.Add(date + " 外資連續三日買超股票張數比例圖").Font = font;

                    }
                    else
                    {
                        PieChart.Titles.Add(date + " 外資連續三日賣超股票張數比例圖").Font = font;

                    }
                    break;
                case 1:
                    if (BuyOrSell)
                    {
                        PieChart.Titles.Add(date + " 外資賣超轉買超股票張數比例圖").Font = font;
                        
                    }
                    else
                    {
                        PieChart.Titles.Add(date + " 外資買超轉賣超股票張數比例圖").Font = font;

                    }
                    break;
            }



        }


        #region Windows Form 控制項
        /// <summary>
        /// 控制畫面大小比例
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private float X;//當前窗體的寬度
        private float Y;//當前窗體的高度
        private void PieChartForm_Load(object sender, EventArgs e)
        {
            // TODO: 這行程式碼會將資料載入 'stockOperatorDataSet.InsDailyReport' 資料表。您可以視需要進行移動或移除。

            X = this.Width;//獲取窗體的寬度
            Y = this.Height;//獲取窗體的高度
            setTag(this);//調用方法
        }
        private void PieChartForm_Resize(object sender, EventArgs e)
        {
            float newx = (this.Width) / X; //窗體寬度縮放比例
            float newy = (this.Height) / Y;//窗體高度縮放比例
            setControls(newx, newy, this);//隨窗體改變控制項大小
        }
        private void setTag(Control cons)
        {
            foreach (Control con in cons.Controls)
            {
                con.Tag = con.Width + ":" + con.Height + ":" + con.Left + ":" + con.Top + ":" + con.Font.Size;
                if (con.Controls.Count > 0)
                    setTag(con);
            }
        }
        private void setControls(float newx, float newy, Control cons)
        {
            //遍歷窗體中的控制項，重新設置控制項的值
            foreach (Control con in cons.Controls)
            {
                if (con.Tag != null)
                {
                    var mytag = con.Tag.ToString().Split(':');//獲取控制項的Tag屬性值，並分割後存儲字元串數組
                    float a = System.Convert.ToSingle(mytag[0]) * newx;//根據窗體縮放比例確定控制項的值，寬度
                    con.Width = (int)a;//寬度
                    a = System.Convert.ToSingle(mytag[1]) * newy;//高度
                    con.Height = (int)(a);
                    a = System.Convert.ToSingle(mytag[2]) * newx;//左邊距離
                    con.Left = (int)(a);
                    a = System.Convert.ToSingle(mytag[3]) * newy;//上邊緣距離
                    con.Top = (int)(a);
                    Single currentSize = System.Convert.ToSingle(mytag[4]) * newy;//字體大小
                    con.Font = new Font(con.Font.Name, currentSize, con.Font.Style, con.Font.Unit);
                    if (con.Controls.Count > 0)
                    {
                        setControls(newx, newy, con);
                    }
                }
            }
        }
        #endregion

        private void PieChart_Click(object sender, EventArgs e)
        {

        }
    }
}
