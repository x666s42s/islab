﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Net;
using System.IO;


namespace StockFM
{
    public partial class StockFM : Form
    {

        DataTable threeDaysBuyOver = null;

        //List<bool> checkMSCI = null;

        DataTable FGSellChangeIntoFGBuy = null;

        DataTable threeDaysSellOver = null;

        DataTable FGBuyChangeIntoFGSell = null;



        public StockFM()
        {
            InitializeComponent();
            //getThreeDaysBuyOverData();
            getThreeDaysSellOverData();
            //getFGSellChangeIntoFGBuyData();
            //getFGBuyChangeIntoFGSellData();
            //getFGBuyOverDataCrossT86();
            //getFGSellOverDataCrossT86();
            Load_buyComboBox();
            Load_sellComboBox();
            searchTaiEx();
        }

        private void getThreeDaysBuyOverData()
        {

            //checkMSCI = new List<bool>();

            threeDaysBuyOver = new DataTable();

            DataTable temp = new DataTable();

            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand("Search_3Days_FGBuy"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@dt", stockDateTimePicker.Value);
                    con.Open();

                    temp.Load(cmd.ExecuteReader());

                    threeDaysBuyOver.Columns.Add("股票代號", typeof(string));
                    threeDaysBuyOver.Columns.Add("股票名稱", typeof(string));
                    threeDaysBuyOver.Columns.Add("成交價", typeof(double));
                    threeDaysBuyOver.Columns.Add("漲跌", typeof(double));
                    threeDaysBuyOver.Columns.Add("前個交易日外資買超張數", typeof(string));
                    threeDaysBuyOver.Columns.Add("上個交易日外資買超張數", typeof(string));
                    threeDaysBuyOver.Columns.Add(Convert.ToDateTime(temp.Rows[0]["lastDate"]).ToString("yyyy/MM/dd") + "\n外資買超張數", typeof(string));
                    threeDaysBuyOver.Columns.Add("最新交易日\n外資買超名次", typeof(int));
                    threeDaysBuyOver.Columns.Add("外資比重", typeof(string));
                    threeDaysBuyOver.Columns.Add("MSCI", typeof(bool));


                    foreach(DataRow dr in temp.Rows)
                    {
                        DataRow dataRow = threeDaysBuyOver.NewRow();
                        for (int i = 0; i < threeDaysBuyOver.Columns.Count -1; i++)
                        {
                            dataRow[((DataColumn)threeDaysBuyOver.Columns[i])] = dr[i];

                            if (dr["checkMSCI"].ToString() != "")
                            {
                                //checkMSCI.Add(true);
                                dataRow[((DataColumn)threeDaysBuyOver.Columns["MSCI"])] = true;
                            }
                            else
                            {
                                //checkMSCI.Add(false);
                                dataRow[((DataColumn)threeDaysBuyOver.Columns["MSCI"])] = false;
                            }
                        }
                        
                        
                        threeDaysBuyOver.Rows.Add(dataRow);

                    }

                    

                    con.Close();
                }
            }


            //threeDaysBuyOver.Columns[3].ColumnName = Convert.ToDateTime(threeDaysBuyOver.Rows[0][10]).ToString("yyyy/MM/dd") + "\n外資買超張數";
            //threeDaysBuyOver.Columns[4].ColumnName = Convert.ToDateTime(threeDaysBuyOver.Rows[0][9]).ToString("yyyy/MM/dd") + "\n外資買超張數";
            //threeDaysBuyOver.Columns[5].ColumnName = Convert.ToDateTime(threeDaysBuyOver.Rows[0][8]).ToString("yyyy/MM/dd") + "\n外資買超張數";

            //threeDaysBuyOver.Columns.Remove("Date");
            //threeDaysBuyOver.Columns.Remove("Date1");
            //threeDaysBuyOver.Columns.Remove("Date2");

            buyDataGridView.DataSource = threeDaysBuyOver;


        }

        private void getThreeDaysSellOverData()
        {
            threeDaysSellOver = new DataTable();

            DataTable temp = new DataTable();

            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand("Search_3Days_FGSell"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@dt", stockDateTimePicker.Value);
                    con.Open();
                    temp.Load(cmd.ExecuteReader());

                    threeDaysSellOver.Columns.Add("股票代號", typeof(string));
                    threeDaysSellOver.Columns.Add("股票名稱", typeof(string));
                    threeDaysSellOver.Columns.Add("成交價", typeof(double));
                    threeDaysSellOver.Columns.Add("漲跌", typeof(double));
                    threeDaysSellOver.Columns.Add("前個交易日外資賣超張數", typeof(string));
                    threeDaysSellOver.Columns.Add("上個交易日外資賣超張數", typeof(string));
                    threeDaysSellOver.Columns.Add(Convert.ToDateTime(temp.Rows[0]["lastDate"]).ToString("yyyy/MM/dd") + "\n外資賣超張數", typeof(string));
                    threeDaysSellOver.Columns.Add(Convert.ToDateTime(temp.Rows[0]["lastDate"]).ToString("yyyy/MM/dd") + "\n外資買超名次", typeof(int));
                    threeDaysSellOver.Columns.Add("外資比重", typeof(string));
                    threeDaysSellOver.Columns.Add("MSCI", typeof(bool));

                    foreach(DataRow dr in temp.Rows)
                    {
                        DataRow dataRow = threeDaysSellOver.NewRow();
                        for (int i = 0; i < threeDaysSellOver.Columns.Count - 1; i++)
                        {
                            dataRow[((DataColumn)threeDaysSellOver.Columns[i])] = dr[i];

                            if (dr["checkMSCI"].ToString() != "")
                            {
                                //checkMSCI.Add(true);
                                dataRow[((DataColumn)threeDaysSellOver.Columns["MSCI"])] = true;
                            }
                            else
                            {
                                //checkMSCI.Add(false);
                                dataRow[((DataColumn)threeDaysSellOver.Columns["MSCI"])] = false;
                            }
                        }
                        threeDaysSellOver.Rows.Add(dataRow);
                    }
                    con.Close();
                }
            }

            

            //threeDaysSellOver.Columns[3].ColumnName = Convert.ToDateTime(threeDaysSellOver.Rows[0][10]).ToString("yyyy/MM/dd") + "\n外資賣超張數";
            //threeDaysSellOver.Columns[4].ColumnName = Convert.ToDateTime(threeDaysSellOver.Rows[0][9]).ToString("yyyy/MM/dd") + "\n外資賣超張數";
            //threeDaysSellOver.Columns[5].ColumnName = Convert.ToDateTime(threeDaysSellOver.Rows[0][8]).ToString("yyyy/MM/dd") + "\n外資賣超張數";

            //threeDaysSellOver.Columns.RemoveAt(10);
            //threeDaysSellOver.Columns.RemoveAt(9);
            //threeDaysSellOver.Columns.RemoveAt(8);

            sellDataGridView.DataSource = threeDaysSellOver;
        }

        private void getFGSellChangeIntoFGBuyData()
        {
            FGSellChangeIntoFGBuy = new DataTable();
            DataTable temp = new DataTable();

            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand("Search_Change_FGSellIntoFGBuy"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@dt", stockDateTimePicker.Value);
                    con.Open();
                    temp.Load(cmd.ExecuteReader());
                    FGSellChangeIntoFGBuy.Columns.Add("股票代號", typeof(string));
                    FGSellChangeIntoFGBuy.Columns.Add("股票名稱", typeof(string));
                    FGSellChangeIntoFGBuy.Columns.Add("成交價", typeof(double));
                    FGSellChangeIntoFGBuy.Columns.Add("漲跌", typeof(double));
                    FGSellChangeIntoFGBuy.Columns.Add("日期", typeof(string));
                    FGSellChangeIntoFGBuy.Columns.Add("上個交易日外資賣超張數", typeof(string));
                    FGSellChangeIntoFGBuy.Columns.Add(Convert.ToDateTime(temp.Rows[0]["lastDate"]).ToString("yyyy/MM/dd") + "\n外資買超張數", typeof(string));
                    FGSellChangeIntoFGBuy.Columns.Add(Convert.ToDateTime(temp.Rows[0]["lastDate"]).ToString("yyyy/MM/dd") + "\n外資買超名次", typeof(int));
                    FGSellChangeIntoFGBuy.Columns.Add("外資比重", typeof(string));
                    FGSellChangeIntoFGBuy.Columns.Add("MSCI", typeof(bool));

                    foreach(DataRow dr in temp.Rows)
                    {
                        DataRow dataRow = FGSellChangeIntoFGBuy.NewRow();
                        for (int i = 0; i < FGSellChangeIntoFGBuy.Columns.Count - 1; i++)
                        {
                            dataRow[((DataColumn)FGSellChangeIntoFGBuy.Columns[i])] = dr[i];

                            if (dr["checkMSCI"].ToString() != "")
                            {
                                //checkMSCI.Add(true);
                                dataRow[((DataColumn)FGSellChangeIntoFGBuy.Columns["MSCI"])] = true;
                            }
                            else
                            {
                                //checkMSCI.Add(false);
                                dataRow[((DataColumn)FGSellChangeIntoFGBuy.Columns["MSCI"])] = false;
                            }
                        }
                        FGSellChangeIntoFGBuy.Rows.Add(dataRow);
                    }

                    con.Close();
                }
            }
            buyDataGridView.DataSource = FGSellChangeIntoFGBuy;
        }

        private void getFGBuyChangeIntoFGSellData()
        {
            FGBuyChangeIntoFGSell = new DataTable();
            DataTable temp = new DataTable();
            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand("Search_Change_FGBuyIntoFGSell"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@dt", stockDateTimePicker.Value);
                    con.Open();
                    temp.Load(cmd.ExecuteReader());

                    
                    FGBuyChangeIntoFGSell.Columns.Add("股票代號", typeof(string));
                    FGBuyChangeIntoFGSell.Columns.Add("股票名稱", typeof(string));
                    FGBuyChangeIntoFGSell.Columns.Add("成交價", typeof(double));
                    FGBuyChangeIntoFGSell.Columns.Add("漲跌", typeof(double));
                    FGBuyChangeIntoFGSell.Columns.Add("日期", typeof(string));
                    FGBuyChangeIntoFGSell.Columns.Add("上個交易日外資買超張數", typeof(string));
                    FGBuyChangeIntoFGSell.Columns.Add(Convert.ToDateTime(temp.Rows[0]["lastDate"]).ToString("yyyy/MM/dd") + "\n外資賣超張數", typeof(string));
                    FGBuyChangeIntoFGSell.Columns.Add(Convert.ToDateTime(temp.Rows[0]["lastDate"]).ToString("yyyy/MM/dd") + "\n外資賣超名次", typeof(int));
                    FGBuyChangeIntoFGSell.Columns.Add("外資比重", typeof(string));
                    FGBuyChangeIntoFGSell.Columns.Add("MSCI", typeof(bool));

                    

                    foreach(DataRow dr in temp.Rows)
                    {
                        DataRow dataRow = FGBuyChangeIntoFGSell.NewRow();
                        for (int i = 0; i < FGBuyChangeIntoFGSell.Columns.Count - 1; i++)
                        {
                            dataRow[((DataColumn)FGBuyChangeIntoFGSell.Columns[i])] = dr[i];

                            if (dr["checkMSCI"].ToString() != "")
                            {
                                //checkMSCI.Add(true);
                                dataRow[((DataColumn)FGBuyChangeIntoFGSell.Columns["MSCI"])] = true;
                            }
                            else
                            {
                                //checkMSCI.Add(false);
                                dataRow[((DataColumn)FGBuyChangeIntoFGSell.Columns["MSCI"])] = false;
                            }
                        }
                        FGBuyChangeIntoFGSell.Rows.Add(dataRow);
                    }
                    con.Close();
                }
            }
            sellDataGridView.DataSource = FGBuyChangeIntoFGSell;
        }

        private void getFGBuyOverDataCrossT86()
        {


            DataTable fgBuyOverCrossT86 = new DataTable();

            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand("Search_FGBuy_JOIN_T86"))
                {
                    
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@date", stockDateTimePicker.Value);

                    con.Open();
                    fgBuyOverCrossT86.Load(cmd.ExecuteReader());
                    con.Close();
                }
            }



            buyDataGridView.DataSource = fgBuyOverCrossT86;

        }

        private void getFGSellOverDataCrossT86()
        {


            DataTable fgSellOverCrossT86 = new DataTable();

            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand("Search_FGSell_JOIN_T86"))
                {
                    

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@date", stockDateTimePicker.Value);

                    con.Open();
                    fgSellOverCrossT86.Load(cmd.ExecuteReader());
                    con.Close();
                }
            }



            sellDataGridView.DataSource = fgSellOverCrossT86;
        }

        private void searchTaiEx()
        {
            DateTime date = stockDateTimePicker.Value;

            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand("Search_TaiEx"))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    con.Open();
                    DataTable tempTaiEx = new DataTable();


                    string dateS = date.ToString("yyyyMMdd");

                    cmd.Parameters.AddWithValue("@date1", dateS);

                    tempTaiEx.Load(cmd.ExecuteReader());

                    con.Close();

                    ArrayList listDate = new ArrayList();
                    ArrayList listTaiEx = new ArrayList();

                    foreach (DataRow tempRow in tempTaiEx.Rows)
                    {
                        listDate.Add(Convert.ToDateTime(tempRow["Date"]).ToString("yyyy年MM月dd日"));
                        listTaiEx.Add(Convert.ToDouble(tempRow["TaiEx"]));
                    }

                    double todayMA = 0;
                    double lastdayMA = 0;

                    for (int i = 0; i < 5; i++)
                    {
                        todayMA += Convert.ToDouble(listTaiEx[i]);

                        lastdayMA += Convert.ToDouble(listTaiEx[i + 1]);
                    }

                    double todayTaiEx = Convert.ToDouble(listTaiEx[0]);

                    double lastdayTaiEx = Convert.ToDouble(listTaiEx[1]);

                    TaiMA1.Text = listDate[0] + " 當日台股大盤："+ listTaiEx[0] + " 大盤漲跌幅：" + (todayTaiEx - lastdayTaiEx).ToString(".00") + " 大盤五日MA趨勢：" + ((todayMA / 5) - (lastdayMA / 5)).ToString(".00");

                    
                }
            }
        }

        private void searchNDaysBuySumT86(int days)
        {
            DataTable nDaysBuySumT86 = new DataTable();

            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand("Search_NDaysFGBuySumT86"))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@date", stockDateTimePicker.Value);
                    cmd.Parameters.AddWithValue("@days", days);

                    con.Open();

                    nDaysBuySumT86.Columns.Add("股票代號",typeof(string));
                    nDaysBuySumT86.Columns.Add("股票名稱", typeof(string));
                    nDaysBuySumT86.Columns.Add("外資" + days + "日買超總量排名", typeof(int));
                    nDaysBuySumT86.Columns.Add("外資" + days + "日買超總量", typeof(int));
                    nDaysBuySumT86.Columns.Add("投信" + days + "日買賣超總量", typeof(int));
                    nDaysBuySumT86.Columns.Add("自營商" + days + "日買賣超總量", typeof(int));
                    nDaysBuySumT86.Columns.Add("自營商" + days + "日買賣超總量(避險)", typeof(int));

                    SqlDataReader dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        DataRow dataRow = nDaysBuySumT86.NewRow();
                        for (int i = 0; i < nDaysBuySumT86.Columns.Count; i++)
                        {
                            dataRow[((DataColumn)nDaysBuySumT86.Columns[i])] = dr[i];
                        }
                        nDaysBuySumT86.Rows.Add(dataRow);
                    }

                    con.Close();
                }
            }



            buyDataGridView.DataSource = nDaysBuySumT86;
        }

        private void searchNDaysSellSumT86(int days)
        {
            DataTable nDaysSellSumT86 = new DataTable();

            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand("Search_NDaysFGSellSumT86"))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@date", stockDateTimePicker.Value);
                    cmd.Parameters.AddWithValue("@days", days);

                    con.Open();

                    nDaysSellSumT86.Columns.Add("股票代號", typeof(string));
                    nDaysSellSumT86.Columns.Add("股票名稱", typeof(string));
                    nDaysSellSumT86.Columns.Add("外資" + days + "日賣超總量排名", typeof(int));
                    nDaysSellSumT86.Columns.Add("外資" + days + "日賣超總量", typeof(int));
                    nDaysSellSumT86.Columns.Add("投信" + days + "日買賣超總量", typeof(int));
                    nDaysSellSumT86.Columns.Add("自營商" + days + "日買賣超總量", typeof(int));
                    nDaysSellSumT86.Columns.Add("自營商" + days + "日買賣超總量(避險)", typeof(int));

                    SqlDataReader dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        DataRow dataRow = nDaysSellSumT86.NewRow();
                        for (int i = 0; i < nDaysSellSumT86.Columns.Count; i++)
                        {
                            dataRow[((DataColumn)nDaysSellSumT86.Columns[i])] = dr[i];
                        }
                        nDaysSellSumT86.Rows.Add(dataRow);
                    }

                    con.Close();
                }
            }



            sellDataGridView.DataSource = nDaysSellSumT86;
        }

        //折線圖
        private void searchResult(DateTime date, String text)
        {
            DataTable tempTable = new DataTable();

            int i;

            for(i = 0; i < StockChart.Series.Count ; i++)
            {
                StockChart.Series[i].Points.Clear();
            }

            
                        
            if (int.TryParse(text, out i))
            {
                string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
                using (SqlConnection con = new SqlConnection(consString))
                {
                    using (SqlCommand cmd = new SqlCommand("Show_T86_By_StockID"))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = con;
                        cmd.Parameters.AddWithValue("@date", date);
                        cmd.Parameters.AddWithValue("@stockID", text);
                        con.Open();
                        tempTable.Load(cmd.ExecuteReader());
                        con.Close();
                    }

                    List<DateTime> Date = new List<DateTime>();
                    List<int> FG = new List<int>();
                    List<int> Dom = new List<int>();
                    List<int> DealerS = new List<int>();
                    List<int> DealerH = new List<int>();

                    foreach (DataRow tempRow in tempTable.Rows)
                    {
                        Date.Add(Convert.ToDateTime(tempRow[0]));
                        FG.Add(Convert.ToInt32(tempRow[3]));
                        Dom.Add(Convert.ToInt32(tempRow[4]));
                        DealerS.Add(Convert.ToInt32(tempRow[5]));
                        DealerH.Add(Convert.ToInt32(tempRow[6]));
                    }

                    StockChart.ChartAreas["T86Chart"].AxisX.LabelStyle.Format = "yyyy-MM-dd";
                    StockChart.ChartAreas["T86Chart"].AxisX.Interval = 1;


                    for (i = 0; i < tempTable.Rows.Count; i++)
                    {
                        StockChart.Series[0].Points.AddXY(Date[i].ToShortDateString(), FG[i]);
                        StockChart.Series[1].Points.AddXY(Date[i].ToShortDateString(), Dom[i]);
                        StockChart.Series[2].Points.AddXY(Date[i].ToShortDateString(), DealerS[i]);
                        StockChart.Series[3].Points.AddXY(Date[i].ToShortDateString(), DealerH[i]);
                    }

                    
                    


                }

            }
            else
            {
                string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
                using (SqlConnection con = new SqlConnection(consString))
                {
                    using (SqlCommand cmd = new SqlCommand("Show_T86_By_StockName"))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = con;
                        cmd.Parameters.AddWithValue("@date", date);
                        cmd.Parameters.AddWithValue("@stockName", text);
                        con.Open();
                        tempTable.Load(cmd.ExecuteReader());
                        con.Close();
                    }

                    List<DateTime> Date = new List<DateTime>();
                    List<int> FG = new List<int>();
                    List<int> Dom = new List<int>();
                    List<int> DealerS = new List<int>();
                    List<int> DealerH = new List<int>();

                    foreach (DataRow tempRow in tempTable.Rows)
                    {
                        Date.Add(Convert.ToDateTime(tempRow[0]));
                        FG.Add(Convert.ToInt32(tempRow[3]));
                        Dom.Add(Convert.ToInt32(tempRow[4]));
                        DealerS.Add(Convert.ToInt32(tempRow[5]));
                        DealerH.Add(Convert.ToInt32(tempRow[6]));
                    }

                    StockChart.ChartAreas[1].AxisX.LabelStyle.Format = "yyyy-MM-dd";
                    StockChart.ChartAreas[1].AxisX.Interval = 1;

                    for (i = 0; i < tempTable.Rows.Count; i++)
                    {
                        StockChart.Series[0].Points.AddXY(Date[i].ToShortDateString(), FG[i]);
                        StockChart.Series[1].Points.AddXY(Date[i].ToShortDateString(), Dom[i]);
                        StockChart.Series[2].Points.AddXY(Date[i].ToShortDateString(), DealerS[i]);
                        StockChart.Series[3].Points.AddXY(Date[i].ToShortDateString(), DealerH[i]);

                    }


                    

                }
            }
        }

        private void stockDateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            searchTaiEx();

            switch (buyComboBox.SelectedIndex)
            {
                case 0:
                    getThreeDaysBuyOverData();
                break;
                case 1:
                    getFGSellChangeIntoFGBuyData();
                break;
                case 2:
                    getFGBuyOverDataCrossT86();
                break;
                case 3:
                    buyDaysGroupBox.Visible = true;
                    if (fiveDaysBuyRadiobtn.Checked)
                    {
                        searchNDaysBuySumT86(5);
                    }
                    else if (twentyDaysBuyRadiobtn.Checked)
                    {
                        searchNDaysBuySumT86(20);
                    }
                    else if (sixtyDaysBuyRadiobtn.Checked)
                    {
                        searchNDaysBuySumT86(60);
                    }
                    break;
                
            }

            switch (sellComboBox.SelectedIndex)
            {
                case 0:
                    getThreeDaysSellOverData();
                    break;
                case 1:
                    getFGBuyChangeIntoFGSellData();
                    break;
                case 2:
                    getFGSellOverDataCrossT86();
                    break;
                case 3:
                    sellDaysGroupBox.Visible = true;
                    if (fiveDaysSellRadiobtn.Checked)
                    {
                        searchNDaysSellSumT86(5);
                    }
                    else if (twentyDaysSellRadiobtn.Checked)
                    {
                        searchNDaysSellSumT86(20);
                    }
                    else if (sixtyDaysSellRadiobtn.Checked)
                    {
                        searchNDaysSellSumT86(60);
                    }
                    break;

            }


        }

        #region Windows Form 控制項
        /// <summary>
        /// 控制畫面大小比例
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private float X;//當前窗體的寬度
        private float Y;//當前窗體的高度
        private void StockFM_Load(object sender, EventArgs e)
        {
            // TODO: 這行程式碼會將資料載入 'stockOperatorDataSet.InsDailyReport' 資料表。您可以視需要進行移動或移除。

            X = this.Width;//獲取窗體的寬度
            Y = this.Height;//獲取窗體的高度
            setTag(this);//調用方法
        }
        private void StockFM_Resize(object sender, EventArgs e)
        {
            float newx = (this.Width) / X; //窗體寬度縮放比例
            float newy = (this.Height) / Y;//窗體高度縮放比例
            setControls(newx, newy, this);//隨窗體改變控制項大小
        }
        private void setTag(Control cons)
        {
            foreach (Control con in cons.Controls)
            {
                con.Tag = con.Width + ":" + con.Height + ":" + con.Left + ":" + con.Top + ":" + con.Font.Size;
                if (con.Controls.Count > 0)
                    setTag(con);
            }
        }
        private void setControls(float newx, float newy, Control cons)
        {
            //遍歷窗體中的控制項，重新設置控制項的值
            foreach (Control con in cons.Controls)
            {
                if (con.Tag != null)
                {
                    var mytag = con.Tag.ToString().Split(':');//獲取控制項的Tag屬性值，並分割後存儲字元串數組
                    float a = System.Convert.ToSingle(mytag[0]) * newx;//根據窗體縮放比例確定控制項的值，寬度
                    con.Width = (int)a;//寬度
                    a = System.Convert.ToSingle(mytag[1]) * newy;//高度
                    con.Height = (int)(a);
                    a = System.Convert.ToSingle(mytag[2]) * newx;//左邊距離
                    con.Left = (int)(a);
                    a = System.Convert.ToSingle(mytag[3]) * newy;//上邊緣距離
                    con.Top = (int)(a);
                    Single currentSize = System.Convert.ToSingle(mytag[4]) * newy;//字體大小
                    con.Font = new Font(con.Font.Name, currentSize, con.Font.Style, con.Font.Unit);
                    if (con.Controls.Count > 0)
                    {
                        setControls(newx, newy, con);
                    }
                }
            }
        }
        #endregion

        public void Load_buyComboBox()
        {
            buyComboBox.SelectedIndex = 0;

            getThreeDaysBuyOverData();
        }

        public void Load_sellComboBox()
        {
            sellComboBox.SelectedIndex = 0;

            getThreeDaysSellOverData();
        }

        private void buyComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (buyComboBox.SelectedIndex)
            {
                case 0:
                    buyDaysGroupBox.Visible = false;
                    showBuyPieChart.Visible = true;
                    getThreeDaysBuyOverData();
                    break;
                case 1:
                    buyDaysGroupBox.Visible = false;
                    showBuyPieChart.Visible = true;
                    getFGSellChangeIntoFGBuyData();                
                    break;
                case 2:
                    buyDaysGroupBox.Visible = false;
                    showBuyPieChart.Visible = false;
                    getFGBuyOverDataCrossT86();
                    break;
                case 3:
                    buyDaysGroupBox.Visible = true;
                    showBuyPieChart.Visible = false;
                    if (fiveDaysBuyRadiobtn.Checked)
                    {
                        searchNDaysBuySumT86(5);
                    }
                    else if (twentyDaysBuyRadiobtn.Checked)
                    {
                        searchNDaysBuySumT86(20);
                    }
                    else if (sixtyDaysBuyRadiobtn.Checked)
                    {
                        searchNDaysBuySumT86(60);
                    }                    
                    break;

            }

            
        }

        private void sellComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (sellComboBox.SelectedIndex)
            {
                case 0:
                    sellDaysGroupBox.Visible = false;
                    showSellPieChart.Visible = true;
                    getThreeDaysSellOverData();
                    break;
                case 1:
                    sellDaysGroupBox.Visible = false;
                    showSellPieChart.Visible = true;
                    getFGBuyChangeIntoFGSellData();
                    break;
                case 2:
                    sellDaysGroupBox.Visible = false;
                    showSellPieChart.Visible = false;
                    getFGSellOverDataCrossT86();
                    break;
                case 3:
                    sellDaysGroupBox.Visible = true;
                    showSellPieChart.Visible = false;
                    if (fiveDaysSellRadiobtn.Checked)
                    {
                        searchNDaysSellSumT86(5);
                    }
                    else if (twentyDaysSellRadiobtn.Checked)
                    {
                        searchNDaysSellSumT86(20);
                    }
                    else if (sixtyDaysSellRadiobtn.Checked)
                    {
                        searchNDaysSellSumT86(60);
                    }
                    break;
            }
        }

        private void searchStockDatePicker_ValueChanged(object sender, EventArgs e)
        {
            if(SearchBox.Text != "")
            {
                searchResult(searchStockDatePicker.Value, SearchBox.Text);
                //getSTOCK_DAY(searchStockDatePicker.Value, SearchBox.Text);
                SearchSumT86(searchStockDatePicker.Value, SearchBox.Text);
                SearchForeignHoldingWeight(searchStockDatePicker.Value, SearchBox.Text);
            }
            
        }

        private void SearchBtn_Click(object sender, EventArgs e)
        {
            searchResult(searchStockDatePicker.Value, SearchBox.Text);
            //getSTOCK_DAY(searchStockDatePicker.Value, SearchBox.Text);
            SearchSumT86(searchStockDatePicker.Value, SearchBox.Text);
            SearchForeignHoldingWeight(searchStockDatePicker.Value, SearchBox.Text);
        }

        private void SearchForeignHoldingWeight(DateTime date, string text)
        {
            DataTable tempTable = new DataTable();
            int i;
            if (int.TryParse(text, out i))
            {
                string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
                using (SqlConnection con = new SqlConnection(consString))
                {
                    using (SqlCommand cmd = new SqlCommand("Show_ForeignHoldingWeight_By_StockID"))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = con;
                        cmd.Parameters.AddWithValue("@date", date);
                        cmd.Parameters.AddWithValue("@stockID", text);
                        con.Open();
                        tempTable.Load(cmd.ExecuteReader());
                        con.Close();
                    }
                   
                    if (tempTable.Rows.Count != 0)
                    {
                        SearchResultStockIDStockName.Text = tempTable.Rows[0][1] + "-" + tempTable.Rows[0][2] + "-外資比重：" + tempTable.Rows[0][3];
                    }
                    else
                    {
                        SearchResultStockIDStockName.Text = "尚無資料";
                    }

                }
            }
            else
            {
                string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
                using (SqlConnection con = new SqlConnection(consString))
                {
                    using (SqlCommand cmd = new SqlCommand("Show_ForeignHoldingWeight_By_StockName"))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = con;
                        cmd.Parameters.AddWithValue("@date", date);
                        cmd.Parameters.AddWithValue("@stockName", text);
                        con.Open();
                        tempTable.Load(cmd.ExecuteReader());
                        con.Close();
                    }

                    if (tempTable.Rows.Count != 0)
                    {
                        SearchResultStockIDStockName.Text = tempTable.Rows[0][1] + "-" + tempTable.Rows[0][2] + "-外資比重：" + tempTable.Rows[0][3];
                    }
                    else
                    {
                        SearchResultStockIDStockName.Text = "尚無資料";
                    }

                }
            }
        }

        private void SearchBox_KeyDown(object sender, KeyEventArgs e)
        {
            int i;
            if(int.TryParse(SearchBox.Text, out i) && SearchBox.Text.Length == 4)
            {
                searchResult(searchStockDatePicker.Value, SearchBox.Text);
                //getSTOCK_DAY(searchStockDatePicker.Value, SearchBox.Text);
                SearchSumT86(searchStockDatePicker.Value, SearchBox.Text);
                SearchForeignHoldingWeight(searchStockDatePicker.Value, SearchBox.Text);
            }
            else if (SearchBox.Text.Length > 1 && !int.TryParse(SearchBox.Text, out i))
            {
                searchResult(searchStockDatePicker.Value, SearchBox.Text);
                //getSTOCK_DAY(searchStockDatePicker.Value, SearchBox.Text);
                SearchSumT86(searchStockDatePicker.Value, SearchBox.Text);
                SearchForeignHoldingWeight(searchStockDatePicker.Value, SearchBox.Text);
            }           

        }

        //長條圖
        private void SearchSumT86(DateTime date, String text)
        {

            DataTable temp = new DataTable();
            DataRow tempdr = temp.NewRow();

            int i;

            if (int.TryParse(text,out i))
            {
                string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
                using (SqlConnection con = new SqlConnection(consString))
                {
                    using (SqlCommand cmd = new SqlCommand("Show_SUM_T86_By_StockID"))
                    {


                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = con;
                        cmd.Parameters.AddWithValue("@date", date);
                        cmd.Parameters.AddWithValue("@stockID", text);
                        con.Open();
                        temp.Load(cmd.ExecuteReader());
                        con.Close();
                        if (temp.Rows.Count > 1)
                        {

                            tempdr = temp.Rows[0];

                            StockChart.Series["外資買賣超總和"].Points.AddXY("5天", tempdr[0]);
                            StockChart.Series["投信買賣超總和"].Points.AddXY("5天", tempdr[1]);
                            StockChart.Series["自營商買賣超總和"].Points.AddXY("5天", tempdr[2]);
                            StockChart.Series["自營商避險總和"].Points.AddXY("5天", tempdr[3]);

                            tempdr = temp.Rows[1];

                            StockChart.Series["外資買賣超總和"].Points.AddXY("20天", tempdr[0]);
                            StockChart.Series["投信買賣超總和"].Points.AddXY("20天", tempdr[1]);
                            StockChart.Series["自營商買賣超總和"].Points.AddXY("20天", tempdr[2]);
                            StockChart.Series["自營商避險總和"].Points.AddXY("20天", tempdr[3]);

                            tempdr = temp.Rows[2];

                            StockChart.Series["外資買賣超總和"].Points.AddXY("60天", tempdr[0]);
                            StockChart.Series["投信買賣超總和"].Points.AddXY("60天", tempdr[1]);
                            StockChart.Series["自營商買賣超總和"].Points.AddXY("60天", tempdr[2]);
                            StockChart.Series["自營商避險總和"].Points.AddXY("60天", tempdr[3]);

                        }

                    }
                }
            }
            else
            {
                string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
                using (SqlConnection con = new SqlConnection(consString))
                {
                    using (SqlCommand cmd = new SqlCommand("Show_SUM_T86_By_StockName"))
                    {


                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = con;
                        cmd.Parameters.AddWithValue("@date", date);
                        cmd.Parameters.AddWithValue("@stockName", text);
                        con.Open();
                        temp.Load(cmd.ExecuteReader());
                        con.Close();
                        if (temp.Rows.Count > 1)
                        {

                            tempdr = temp.Rows[0];

                            StockChart.Series["外資買賣超總和"].Points.AddXY("5天", tempdr[0]);
                            StockChart.Series["投信買賣超總和"].Points.AddXY("5天", tempdr[1]);
                            StockChart.Series["自營商買賣超總和"].Points.AddXY("5天", tempdr[2]);
                            StockChart.Series["自營商避險總和"].Points.AddXY("5天", tempdr[3]);

                            tempdr = temp.Rows[1];

                            StockChart.Series["外資買賣超總和"].Points.AddXY("20天", tempdr[0]);
                            StockChart.Series["投信買賣超總和"].Points.AddXY("20天", tempdr[1]);
                            StockChart.Series["自營商買賣超總和"].Points.AddXY("20天", tempdr[2]);
                            StockChart.Series["自營商避險總和"].Points.AddXY("20天", tempdr[3]);

                            tempdr = temp.Rows[2];

                            StockChart.Series["外資買賣超總和"].Points.AddXY("60天", tempdr[0]);
                            StockChart.Series["投信買賣超總和"].Points.AddXY("60天", tempdr[1]);
                            StockChart.Series["自營商買賣超總和"].Points.AddXY("60天", tempdr[2]);
                            StockChart.Series["自營商避險總和"].Points.AddXY("60天", tempdr[3]);

                        }

                    }
                }
            }

            

        }

        //private void getSTOCK_DAY(DateTime date, String text)
        //{
        //    String LastUrl = "http://www.twse.com.tw/exchangeReport/STOCK_DAY?response=csv&date=" + date.AddMonths(-1).ToString("yyyyMMdd") + "&stockNo=" + text;

        //    String Url = "http://www.twse.com.tw/exchangeReport/STOCK_DAY?response=csv&date="+ date.ToString("yyyyMMdd") + "&stockNo=" + text;
                        
        //    HttpWebRequest StockDayRequest = (HttpWebRequest)WebRequest.Create(LastUrl);
        //    HttpWebResponse StockDayResponse = (HttpWebResponse)StockDayRequest.GetResponse();

        //    Encoding encoding = Encoding.GetEncoding("big5");

        //    StreamReader StockDayStreamReader = new StreamReader(StockDayResponse.GetResponseStream(), encoding);
        //    string StockDayResult = StockDayStreamReader.ReadToEnd();

        //    StockDayRequest = (HttpWebRequest)WebRequest.Create(Url);
        //    StockDayResponse = (HttpWebResponse)StockDayRequest.GetResponse();
        //    StockDayStreamReader = new StreamReader(StockDayResponse.GetResponseStream(), encoding);

        //    StockDayResult += StockDayStreamReader.ReadToEnd();

        //    string[] StockDayRows = StockDayResult.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

        //    DateTime tryDate;

        //    List<stockPriceData> data = new List<stockPriceData>();


        //    StockChart.Series[4]["OpenCloseStyle"] = "Triangle";

        //    // Show both open and close marks
        //    StockChart.Series[4]["ShowOpenClose"] = "Both";

        //    // Set point width
        //    StockChart.Series[4]["PointWidth"] = "0.6";

        //    // Set colors bars
        //    StockChart.Series[4]["PriceUpColor"] = "Red"; // <<== use text indexer for series
        //    StockChart.Series[4]["PriceDownColor"] = "Green";


        //    StockChart.ChartAreas["StockChart"].AxisX.LabelStyle.Format = "yyyy-MM-dd";
        //    StockChart.ChartAreas["StockChart"].AxisX.Interval = 1;


        //    float max = 0;
        //    float min = 0;

        //    List<float> heightest = new List<float>();
        //    List<float> lowest = new List<float>();

        //    float interval = 0;

        //    //string test = "";

        //    foreach (string row in StockDayRows)
        //    {
        //        string[] cell = row.Split(new string[] { "\",\"" }, StringSplitOptions.None);
        //        cell[0] = cell[0].Replace("\"", "");
        //        cell[cell.Length - 1] = cell[cell.Length - 1].Replace("\",", "");

        //        if(DateTime.TryParse(cell[0], out tryDate))
        //        {

        //            max = float.Parse(cell[4]);
        //            min = float.Parse(cell[5]);

        //            data.Add(new stockPriceData(DateTime.Parse(cell[0]).AddYears(1911),float.Parse(cell[3]), float.Parse(cell[4]), float.Parse(cell[5]), float.Parse(cell[6])));
        //            //test += Convert.ToDateTime(cell[0]).AddYears(1911).ToString("yyyy/MM/dd") + "\n";

        //            heightest.Add(float.Parse(cell[4]));
        //            lowest.Add(float.Parse(cell[5]));

        //            //if (float.Parse(cell[5]) < min)
        //            //{
        //            //    min = float.Parse(cell[5]);
        //            //}

        //        }
        //    }
        //    //MessageBox.Show(test);



        //    for(int i = 0; i < heightest.Count; i++)
        //    {
        //        if(heightest[i] > max)
        //        {
        //            max = heightest[i];
        //        }

        //        if (lowest[i] < min)
        //        {
        //            min = lowest[i];
        //        }
        //    }
            

        //    //MessageBox.Show(max + " " + min);

        //    interval = (max - min) ;

        //    StockChart.ChartAreas["StockChart"].AxisY.LabelStyle.Format = ".00";

        //    StockChart.ChartAreas["StockChart"].AxisY.Interval = interval;

        //    StockChart.ChartAreas["StockChart"].AxisY.Maximum = max + (interval * 0.6);
        //    StockChart.ChartAreas["StockChart"].AxisY.Minimum = min - (interval * 0.6);

        //    for (int i = 0; i < data.Count; i++)
        //    {
        //        StockChart.Series[4].Points.AddXY( data[i].date.ToString("yyyy/MM/dd"), data[i].HeightestPrice);
        //        StockChart.Series[4].Points[i].YValues[1] = data[i].LowestPrice;
        //        StockChart.Series[4].Points[i].YValues[2] = data[i].OpenPrice;
        //        StockChart.Series[4].Points[i].YValues[3] = data[i].ClosePrice;
        //        StockChart.Series[4].Points[i].ToolTip = date.ToString("yyyy/MM/dd") + " 高點:" + data[i].HeightestPrice + " 開盤:" + data[i].OpenPrice + " 收盤:" + data[i].ClosePrice + " 低點:" + data[i].LowestPrice;
        //    }            


        //}

        //class stockPriceData
        //{
        //    public DateTime date;
        //    public float HeightestPrice;
        //    public float LowestPrice;
        //    public float OpenPrice;
        //    public float ClosePrice;
        //    public stockPriceData(DateTime d, float o, float h, float l,  float c) { date = d; OpenPrice = o; HeightestPrice = h; LowestPrice = l; ClosePrice = c; }
        //}


        private void ShowT86CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ShowT86CheckBox.Checked == true)
            {
                StockChart.ChartAreas["T86Chart"].Visible = true;
                StockChart.Legends["T86"].Enabled = true;
            }
            else
            {
                StockChart.ChartAreas["T86Chart"].Visible = false;
                StockChart.Legends["T86"].Enabled = false;
            }
        }

        private void ShowSumT86CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ShowSumT86CheckBox.Checked == true)
            {
                StockChart.ChartAreas["SumT86Chart"].Visible = true;
                StockChart.Legends["SumT86"].Enabled = true;
            }
            else
            {
                StockChart.ChartAreas["SumT86Chart"].Visible = false;
                StockChart.Legends["SumT86"].Enabled = false;
            }
        }

        private void fiveDaysBuyRadiobtn_CheckedChanged(object sender, EventArgs e)
        {
            if (fiveDaysBuyRadiobtn.Checked)
            {
                searchNDaysBuySumT86(5);
            }
        }

        private void twentyDaysBuyRadiobtn_CheckedChanged(object sender, EventArgs e)
        {
            if (twentyDaysBuyRadiobtn.Checked)
            {
                searchNDaysBuySumT86(20);
            }
        }

        private void sixtyDaysBuyRadiobtn_CheckedChanged(object sender, EventArgs e)
        {
            if (sixtyDaysBuyRadiobtn.Checked)
            {
                searchNDaysBuySumT86(60);
            }
        }

        private void fiveDaysSellRadiobtn_CheckedChanged(object sender, EventArgs e)
        {
            if (fiveDaysSellRadiobtn.Checked)
            {
                searchNDaysSellSumT86(5);
            }
        }

        private void twentyDaysSellRadiobtn_CheckedChanged(object sender, EventArgs e)
        {
            if (twentyDaysSellRadiobtn.Checked)
            {
                searchNDaysSellSumT86(20);
            }
        }

        private void sixtyDaysSellRadiobtn_CheckedChanged(object sender, EventArgs e)
        {
            if (sixtyDaysSellRadiobtn.Checked)
            {
                searchNDaysSellSumT86(60);
            }
        }

        private void showBuyPieChart_Click(object sender, EventArgs e)
        {
            switch (buyComboBox.SelectedIndex)
            {
                case 0:
                    Form pieChart = new PieChartForm(threeDaysBuyOver, stockDateTimePicker.Value.ToString("yyyy/MM/dd"), 0, true);
                    pieChart.ShowDialog();
                    break;
                case 1:
                    pieChart = new PieChartForm(FGSellChangeIntoFGBuy, stockDateTimePicker.Value.ToString("yyyy/MM/dd"), 1, true);
                    pieChart.ShowDialog();
                    break;
            }



        }

        private void showSellPieChart_Click(object sender, EventArgs e)
        {

            switch (sellComboBox.SelectedIndex)
            {
                case 0:
                    Form pieChart = new PieChartForm(threeDaysSellOver, stockDateTimePicker.Value.ToString("yyyy/MM/dd"), 0, false);
                    pieChart.ShowDialog();
                    break;
                case 1:
                    pieChart = new PieChartForm(FGBuyChangeIntoFGSell, stockDateTimePicker.Value.ToString("yyyy/MM/dd"), 1, false);
                    pieChart.ShowDialog();
                    break;
            }

        }

       
    }
}
