use StockOperator
CREATE TYPE [dbo].[FGSellType] AS TABLE(
	[fgsell_ID] [nvarchar](20) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Rank] [nvarchar](10) NOT NULL,
	[StockID] [nvarchar](10) NOT NULL,
	[StockName] [nvarchar](20) NOT NULL,
	[StrikePrice] [nvarchar](20) NOT NULL,
	[UpAndDown] [nvarchar](20) NOT NULL,
	[OversoldCount] [nvarchar](20) NOT NULL,
	[ForeignHoldingCount] [nvarchar](20) NOT NULL,
	[ForeignHoldingWeight] [decimal](18, 2) NOT NULL
)
GO





