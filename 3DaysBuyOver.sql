/****** SSMS 中 SelectTopNRows 命令的指令碼  ******/

CREATE PROCEDURE [dbo].[Search_3Days_FGBuy]
      @dt Date
AS
BEGIN

	DECLARE @dt2 AS Date
	DECLARE @dt3 AS Date

	WHILE 1 = 1
	BEGIN
	IF (	
		SELECT count(*)
		  FROM [StockOperator].[dbo].[FGBuy]
		  WHERE [Date] = @dt
		) <> 0
		BREAK
	ELSE 
		SET @dt = DATEADD(DAY,-1,@dt)
		CONTINUE
	END

	SET @dt2 = DATEADD(DAY,-1,@dt)

	WHILE 1 = 1
	BEGIN
	IF (	
		SELECT count(*)
		  FROM [StockOperator].[dbo].[FGBuy]
		  WHERE [Date] = @dt2
		) <> 0
		BREAK
	ELSE 
		SET @dt2 = DATEADD(DAY,-1,@dt2)
		CONTINUE
	END

	SET @dt3 = DATEADD(DAY,-1,@dt2)

	WHILE 1 = 1
	BEGIN
	IF (	
		SELECT count(*)
		  FROM [StockOperator].[dbo].[FGBuy]
		  WHERE [Date] = @dt3
		) <> 0
		BREAK
	ELSE 
		SET @dt3 = DATEADD(DAY,-1,@dt3)
		CONTINUE
	END


	SELECT T3.Date, T2.Date, T1.Date, T1.StockID, T1.StockName, T1.StrikePrice, T3.OverboughtCount, T2.OverboughtCount, T1.OverboughtCount
	FROM (
		SELECT *
		FROM [dbo].[FGBuy]
		WHERE [Date] = @dt
	) AS T1 INNER JOIN (
		SELECT *
		FROM [dbo].[FGBuy]
		WHERE [Date] = @dt2
	) AS T2 ON T1.StockID = T2.StockID 
	 INNER JOIN (
		SELECT *
		FROM [dbo].[FGBuy]
		WHERE [Date] = @dt3
	) AS T3 ON T2.StockID = T3.StockID
	ORDER BY CONVERT (INT, T1.Rank)

END