CREATE PROCEDURE [dbo].[Insert_MSCI]
      @insMSCI MSCIType READONLY
AS
BEGIN
     SET NOCOUNT ON;

      MERGE INTO MSCI 
	  USING(
		  SELECT MSCI_ID, Date, StockID, StockName,StockPrice,UpAndDown,UADRate,Price,Weight 
		  FROM @insMSCI
	  )AS tempMSCI 
	  ON MSCI.MSCI_ID = tempMSCI.MSCI_ID
	  WHEN MATCHED THEN
	  UPDATE SET MSCI.Date = tempMSCI.Date, MSCI.StockID = tempMSCI.StockID, MSCI.StockName = tempMSCI.StockName, MSCI.StockPrice = tempMSCI.StockPrice,
	  MSCI.UpAndDown = tempMSCI.UpAndDown, MSCI.UADRate = tempMSCI.UADRate, MSCI.Price = tempMSCI.Price, MSCI.Weight = tempMSCI.Weight
	  WHEN NOT MATCHED THEN
	  INSERT(MSCI_ID, Date, StockID, StockName,StockPrice,UpAndDown,UADRate,Price,Weight) 
	  VALUES(tempMSCI.MSCI_ID, tempMSCI.Date, tempMSCI.StockID, tempMSCI.StockName,tempMSCI.StockPrice,tempMSCI.UpAndDown,tempMSCI.UADRate,tempMSCI.Price,tempMSCI.Weight);
END






