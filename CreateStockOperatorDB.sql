USE [StockOperator]
GO
/****** Object:  UserDefinedTableType [dbo].[FGBuyType]    Script Date: 2017/6/21 下午 03:48:24 ******/
CREATE TYPE [dbo].[FGBuyType] AS TABLE(
	[fgbuy_ID] [nvarchar](20) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Rank] [nvarchar](10) NOT NULL,
	[StockID] [nvarchar](10) NOT NULL,
	[StockName] [nvarchar](20) NOT NULL,
	[StrikePrice] [decimal](18, 2) NOT NULL,
	[UpAndDown] [nvarchar](20) NOT NULL,
	[OverboughtCount] [nvarchar](20) NOT NULL,
	[ForeignHoldingCount] [nvarchar](20) NOT NULL,
	[ForeignHoldingWeight] [decimal](18, 2) NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[FGSellType]    Script Date: 2017/6/21 下午 03:48:24 ******/
CREATE TYPE [dbo].[FGSellType] AS TABLE(
	[fgsell_ID] [nvarchar](20) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Rank] [nvarchar](10) NOT NULL,
	[StockID] [nvarchar](10) NOT NULL,
	[StockName] [nvarchar](20) NOT NULL,
	[StrikePrice] [decimal](18, 2) NOT NULL,
	[UpAndDown] [nvarchar](20) NOT NULL,
	[OversoldCount] [nvarchar](20) NOT NULL,
	[ForeignHoldingCount] [nvarchar](20) NOT NULL,
	[ForeignHoldingWeight] [decimal](18, 2) NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[MSCIType]    Script Date: 2017/6/21 下午 03:48:24 ******/
CREATE TYPE [dbo].[MSCIType] AS TABLE(
	[MSCI_ID] [nvarchar](20) NULL,
	[Date] [datetime] NOT NULL,
	[StockID] [nvarchar](10) NOT NULL,
	[StockName] [nvarchar](20) NULL,
	[StockPrice] [decimal](18, 2) NOT NULL,
	[UpAndDown] [nvarchar](20) NOT NULL,
	[UADRate] [nvarchar](20) NOT NULL,
	[Price] [nvarchar](20) NULL,
	[Weight] [decimal](18, 2) NOT NULL
)
GO
/****** Object:  Table [dbo].[FGBuy]    Script Date: 2017/6/21 下午 03:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FGBuy](
	[fgbuy_ID] [nvarchar](20) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Rank] [nvarchar](10) NOT NULL,
	[StockID] [nvarchar](10) NOT NULL,
	[StockName] [nvarchar](20) NOT NULL,
	[StrikePrice] [decimal](18, 2) NOT NULL,
	[UpAndDown] [nvarchar](20) NOT NULL,
	[OverboughtCount] [nvarchar](20) NOT NULL,
	[ForeignHoldingCount] [nvarchar](20) NOT NULL,
	[ForeignHoldingWeight] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_FGBuy] PRIMARY KEY CLUSTERED 
(
	[fgbuy_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FGSell]    Script Date: 2017/6/21 下午 03:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FGSell](
	[fgsell_ID] [nvarchar](20) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Rank] [nvarchar](10) NOT NULL,
	[StockID] [nvarchar](10) NOT NULL,
	[StockName] [nvarchar](20) NOT NULL,
	[StrikePrice] [decimal](18, 2) NOT NULL,
	[UpAndDown] [nvarchar](20) NOT NULL,
	[OversoldCount] [nvarchar](20) NOT NULL,
	[ForeignHoldingCount] [nvarchar](20) NOT NULL,
	[ForeignHoldingWeight] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_FGSell] PRIMARY KEY CLUSTERED 
(
	[fgsell_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MSCI]    Script Date: 2017/6/21 下午 03:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MSCI](
	[MSCI_ID] [nvarchar](20) NOT NULL,
	[Date] [datetime] NOT NULL,
	[StockID] [nvarchar](10) NOT NULL,
	[StockName] [nvarchar](20) NULL,
	[StockPrice] [decimal](18, 2) NOT NULL,
	[UpAndDown] [nvarchar](20) NOT NULL,
	[UADRate] [nvarchar](20) NOT NULL,
	[Price] [nvarchar](20) NULL,
	[Weight] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_MCSI] PRIMARY KEY CLUSTERED 
(
	[MSCI_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  StoredProcedure [dbo].[Insert_FGBuy]    Script Date: 2017/6/21 下午 03:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Insert_FGBuy]
      @insFGBuy FGBuyType READONLY
AS
BEGIN
     SET NOCOUNT ON;

      MERGE INTO FGBuy 
	  USING(
		  SELECT fgbuy_ID, Date, Rank, StockID, StockName, StrikePrice, UpAndDown, OverboughtCount, ForeignHoldingCount, ForeignHoldingWeight 
		  FROM @insFGBuy
	  )AS tempFGBuy
	  ON FGBuy.fgbuy_ID = tempFGBuy.fgbuy_ID
	  WHEN MATCHED THEN
	  UPDATE SET FGBuy.Date = tempFGBuy.Date, FGBuy.Rank = tempFGBuy.Rank, FGBuy.StockID = tempFGBuy.StockID, FGBuy.StockName = tempFGBuy.StockName, 
	  FGBuy.StrikePrice = tempFGBuy.StrikePrice, FGBuy.UpAndDown = tempFGBuy.UpAndDown, FGBuy.OverboughtCount = tempFGBuy.OverboughtCount, 
	  FGBuy.ForeignHoldingCount = tempFGBuy.ForeignHoldingCount, FGBuy.ForeignHoldingWeight = tempFGBuy.ForeignHoldingWeight
	  WHEN NOT MATCHED THEN
	  INSERT(fgbuy_ID, Date, Rank, StockID, StockName, StrikePrice, UpAndDown, OverboughtCount, ForeignHoldingCount, ForeignHoldingWeight) 
	  VALUES(tempFGBuy.fgbuy_ID, tempFGBuy.Date, tempFGBuy.Rank, tempFGBuy.StockID, tempFGBuy.StockName, tempFGBuy.StrikePrice, tempFGBuy.UpAndDown, tempFGBuy.OverboughtCount, tempFGBuy.ForeignHoldingCount, tempFGBuy.ForeignHoldingWeight);
END







GO
/****** Object:  StoredProcedure [dbo].[Insert_FGSell]    Script Date: 2017/6/21 下午 03:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Insert_FGSell]
      @insFGSell FGSellType READONLY
AS
BEGIN
     SET NOCOUNT ON;

      MERGE INTO FGSell 
	  USING(
		  SELECT fgsell_ID, Date, Rank, StockID, StockName, StrikePrice, UpAndDown, OversoldCount, ForeignHoldingCount, ForeignHoldingWeight 
		  FROM @insFGSell
	  )AS tempFGSell
	  ON FGSell.fgsell_ID = tempFGSell.fgsell_ID
	  WHEN MATCHED THEN
	  UPDATE SET FGSell.Date = tempFGSell.Date, FGSell.Rank = tempFGSell.Rank, FGSell.StockID = tempFGSell.StockID, FGSell.StockName = tempFGSell.StockName, 
	  FGSell.StrikePrice = tempFGSell.StrikePrice, FGSell.UpAndDown = tempFGSell.UpAndDown, FGSell.OversoldCount = tempFGSell.OversoldCount, 
	  FGSell.ForeignHoldingCount = tempFGSell.ForeignHoldingCount, FGSell.ForeignHoldingWeight = tempFGSell.ForeignHoldingWeight
	  WHEN NOT MATCHED THEN
	  INSERT(fgsell_ID, Date, Rank, StockID, StockName, StrikePrice, UpAndDown, OversoldCount, ForeignHoldingCount, ForeignHoldingWeight) 
	  VALUES(tempFGSell.fgsell_ID, tempFGSell.Date, tempFGSell.Rank, tempFGSell.StockID, tempFGSell.StockName, tempFGSell.StrikePrice, tempFGSell.UpAndDown, tempFGSell.OversoldCount, tempFGSell.ForeignHoldingCount, tempFGSell.ForeignHoldingWeight);
END









GO
/****** Object:  StoredProcedure [dbo].[Insert_MSCI]    Script Date: 2017/6/21 下午 03:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Insert_MSCI]
      @insMSCI MSCIType READONLY
AS
BEGIN
     SET NOCOUNT ON;

      MERGE INTO MSCI 
	  USING(
		  SELECT MSCI_ID, Date, StockID, StockName,StockPrice,UpAndDown,UADRate,Price,Weight 
		  FROM @insMSCI
	  )AS tempMSCI 
	  ON MSCI.MSCI_ID = tempMSCI.MSCI_ID
	  WHEN MATCHED THEN
	  UPDATE SET MSCI.Date = tempMSCI.Date, MSCI.StockID = tempMSCI.StockID, MSCI.StockName = tempMSCI.StockName, MSCI.StockPrice = tempMSCI.StockPrice,
	  MSCI.UpAndDown = tempMSCI.UpAndDown, MSCI.UADRate = tempMSCI.UADRate, MSCI.Price = tempMSCI.Price, MSCI.Weight = tempMSCI.Weight
	  WHEN NOT MATCHED THEN
	  INSERT(MSCI_ID, Date, StockID, StockName,StockPrice,UpAndDown,UADRate,Price,Weight) 
	  VALUES(tempMSCI.MSCI_ID, tempMSCI.Date, tempMSCI.StockID, tempMSCI.StockName,tempMSCI.StockPrice,tempMSCI.UpAndDown,tempMSCI.UADRate,tempMSCI.Price,tempMSCI.Weight);
END







GO
