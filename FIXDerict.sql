/****** SSMS 中 SelectTopNRows 命令的指令碼  ******/
UPDATE [dbo].[FGBuy] SET [StrikePrice] = REPLACE ([StrikePrice] , '---', NULL ) WHERE [StrikePrice] LIKE '%---%'
UPDATE [dbo].[FGBuy] SET [StrikePrice] = REPLACE ([StrikePrice] , ',', '' ) WHERE [StrikePrice] LIKE '%,%'

UPDATE [dbo].[FGBuy] SET [UpAndDown] = REPLACE ([UpAndDown] , '---', NULL ) WHERE [UpAndDown] LIKE '%---%'
UPDATE [dbo].[FGBuy] SET [UpAndDown] = REPLACE ([UpAndDown] , '△', '' ) WHERE [UpAndDown] LIKE '%△%'
UPDATE [dbo].[FGBuy] SET [UpAndDown] = REPLACE ([UpAndDown] , '▲', '' ) WHERE [UpAndDown] LIKE '%▲%'
UPDATE [dbo].[FGBuy] SET [UpAndDown] = REPLACE ([UpAndDown] , '▽', '-' ) WHERE [UpAndDown] LIKE '%▽%'
UPDATE [dbo].[FGBuy] SET [UpAndDown] = REPLACE ([UpAndDown] , '▼', '-' ) WHERE [UpAndDown] LIKE '%▼%'

UPDATE [dbo].[FGSell] SET [StrikePrice] = REPLACE ([StrikePrice] , '---', NULL ) WHERE [StrikePrice] LIKE '%---%'
UPDATE [dbo].[FGSell] SET [StrikePrice] = REPLACE ([StrikePrice] , ',', '' ) WHERE [StrikePrice] LIKE '%,%'

UPDATE [dbo].[FGSell] SET [UpAndDown] = REPLACE ([UpAndDown] , '---', NULL ) WHERE [UpAndDown] LIKE '%---%'
UPDATE [dbo].[FGSell] SET [UpAndDown] = REPLACE ([UpAndDown] , '△', '' ) WHERE [UpAndDown] LIKE '%△%'
UPDATE [dbo].[FGSell] SET [UpAndDown] = REPLACE ([UpAndDown] , '▲', '' ) WHERE [UpAndDown] LIKE '%▲%'
UPDATE [dbo].[FGSell] SET [UpAndDown] = REPLACE ([UpAndDown] , '▽', '-' ) WHERE [UpAndDown] LIKE '%▽%'
UPDATE [dbo].[FGSell] SET [UpAndDown] = REPLACE ([UpAndDown] , '▼', '-' ) WHERE [UpAndDown] LIKE '%▼%'