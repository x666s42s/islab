﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;

namespace StockCatcher
{
    class Program
    {
        string currentDate = DateTime.Now.ToString("yyyy/MM/dd");

        [STAThread()]
        static void Main(string[] args)
        {
            Program a = new Program();

            Console.Title = "StockCatcher";

            Console.WriteLine("啟動StockCatcher...");


            switch (a.getMSCI())
            {
                case 0:
                    Console.WriteLine("MSCI成分股 匯入成功");
                    break;
                case 1:
                    Console.WriteLine("MSCI成分股 尚未更新");
                    break;
                case 2:
                    Console.WriteLine("MSCI成分股 回傳資料錯誤");
                    break;
            }

            switch (a.getfgbuy())
            {
                case 0:
                    Console.WriteLine("外資買超 匯入成功");
                    break;
                case 1:
                    Console.WriteLine("外資買超 尚未更新");
                    break;
                case 2:
                    Console.WriteLine("外資買超 回傳資料錯誤");
                    break;
            }


            switch (a.getfgsell())
            {
                case 0:
                    Console.WriteLine("外資賣超 匯入成功");
                    break;
                case 1:
                    Console.WriteLine("外資賣超 尚未更新");
                    break;
                case 2:
                    Console.WriteLine("外資賣超 回傳資料錯誤");
                    break;
            }

            if (a.getTaiEx())
            {
                Console.WriteLine("大盤資料 匯入成功");
            }
            else
            {
                Console.WriteLine("大盤資料 尚未更新");
            }

            if(DateTime.Now > new DateTime(2017,12,17))
            {
                if (a.getNewT86())
                {
                    Console.WriteLine("新T86 匯入成功");
                }
                else
                {
                    Console.WriteLine("新T86 尚未更新");
                }
            }
            else
            {
                if (a.getT86())
                {
                    Console.WriteLine("T86 匯入成功");
                }
                else
                {
                    Console.WriteLine("T86 尚未更新");
                }
            }


            //Console.WriteLine(a.getTaiEx().ToString());

            //Console.WriteLine("按任意鍵結束....");

            //Console.ReadKey();


        }


        private string GetWebContent(string Url)

        {

            string strResult = "";

            try

            {

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);

                //聲明一個HttpWebRequest請求

                request.Timeout = 30000;

                //設置連接逾時時間

                request.Headers.Set("Accept-Language", "zh-TW");

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                Stream streamReceive = response.GetResponseStream();

                Encoding encoding = Encoding.GetEncoding("utf-8");

                StreamReader streamReader = new StreamReader(streamReceive, encoding);

                strResult = streamReader.ReadToEnd();

            }

            catch

            {
               

            }

            return strResult;

        }

        private string GetWebContentBig5(string Url)

        {

            string strResult = "";

            try

            {

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);

                //聲明一個HttpWebRequest請求

                request.Timeout = 30000;

                //設置連接逾時時間

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                request.Headers.Set("Accept-Language", "zh-TW");

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                Stream streamReceive = response.GetResponseStream();

                Encoding encoding = Encoding.GetEncoding("big5");

                StreamReader streamReader = new StreamReader(streamReceive, encoding);

                strResult = streamReader.ReadToEnd();

            }

            catch(WebException e)
            {

            }

            return strResult;

        }

        private Boolean checkAnnoDominiDate(string webDate)
        {
            String year = currentDate.Substring(0, 4);
            String month = currentDate.Substring(5, 2);
            String day = currentDate.Substring(8, 2);

            Boolean result = webDate == year + "-" + month + "-" + day;
            return result;
        }

        private Boolean checkRepublicEraDate(string webDate)
        {
            String year = (Convert.ToInt32(currentDate.Substring(0, 4)) - 1911).ToString();
            String month = currentDate.Substring(5, 2);
            String day = currentDate.Substring(8, 2);

            Boolean result = webDate == year + "/" + month + "/" + day;
            return result;
        }

        private int getMSCI()
        {
            //要抓取的URL位址

            string Url = "https://histock.tw/stock/mscitaiwan.aspx";


            string strWebContent = GetWebContent(Url);

            if(strWebContent != "")
            {
                int iBodyStart = strWebContent.IndexOf("<body", 0);
                int iStart = strWebContent.IndexOf("更新日期", iBodyStart);
                int iEnd = strWebContent.IndexOf("MSCI摩根台指成分股權重表", iStart);
                int iTableStart = strWebContent.IndexOf("<table", iEnd);
                int iTableEnd = strWebContent.IndexOf("</table>", iTableStart);

                string updateDate = strWebContent.Substring(iStart + 5, iEnd - iStart - 16);


                if (checkAnnoDominiDate(updateDate))
                {
                    string strWeb = strWebContent.Substring(iTableStart, iTableEnd - iTableStart + 8);

                    WebBrowser webb = new WebBrowser();

                    webb.Navigate("about:blank");

                    HtmlDocument htmldoc = webb.Document.OpenNew(true);

                    htmldoc.Write(strWeb);

                    HtmlElementCollection htmlTR = htmldoc.GetElementsByTagName("tr");

                    int count = 0;

                    ArrayList tempData = new ArrayList();

                    DataTable msciDt = new DataTable();

                    DataRow msciRow;

                    msciDt.Columns.Add("MSCI_ID", typeof(String));

                    msciDt.Columns.Add("Date", typeof(String));

                    msciDt.Columns.Add("StockID", typeof(Int32));

                    msciDt.Columns.Add("StockName", typeof(String));
                    msciDt.Columns.Add("StockPrice", typeof(Double));
                    msciDt.Columns.Add("UpAndDown", typeof(String));
                    msciDt.Columns.Add("UADRate", typeof(String));
                    msciDt.Columns.Add("Price", typeof(String));
                    msciDt.Columns.Add("Weight", typeof(Double));


                    foreach (HtmlElement tr in htmlTR)
                    {
                        HtmlElementCollection htmlTD = tr.GetElementsByTagName("TD");

                        foreach (HtmlElement td in htmlTD)
                        {
                            tempData.Add(td.InnerText);
                        }

                        if (count > 0)
                        {
                            msciRow = msciDt.NewRow();
                            msciRow["MSCI_ID"] = currentDate + tempData[0];
                            msciRow["Date"] = currentDate;
                            msciRow["StockID"] = tempData[0];
                            msciRow["StockName"] = tempData[1];
                            msciRow["StockPrice"] = tempData[2];
                            msciRow["UpAndDown"] = tempData[3];
                            msciRow["UADRate"] = tempData[4];
                            msciRow["Price"] = tempData[5];
                            msciRow["Weight"] = tempData[6];

                            msciDt.Rows.Add(msciRow);

                            //textBox1.Text += currentDate + " " +tempData[0] + " " + tempData[1] + " " + tempData[2] + " " + tempData[3] + " " + tempData[4] + " " + tempData[5] + " " + tempData[6] + Environment.NewLine;
                        }

                        count++;
                        tempData.Clear();
                        //string stockNO = tr.GetElementsByTagName("td")[0].InnerText;
                        //string stockName = tr.GetElementsByTagName("td")[1].InnerText;
                        //string stockPrice = tr.GetElementsByTagName("td")[2].InnerText;
                        //string stockUpAndDown = tr.GetElementsByTagName("td")[3].InnerText;
                        //string stockUpAndDownRate = tr.GetElementsByTagName("td")[4].InnerText;
                        //string Price = tr.GetElementsByTagName("td")[5].InnerText;
                        //string Weights = tr.GetElementsByTagName("td")[6].InnerText;

                        //textBox1.Text += stockNO + " " + stockName + " " + stockPrice + " " + stockUpAndDown + " " + stockUpAndDownRate + " " + Price + " " + Weights;


                    }

                    if (msciDt.Rows.Count > 0)
                    {
                        string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
                        using (SqlConnection con = new SqlConnection(consString))
                        {
                            using (SqlCommand cmd = new SqlCommand("Insert_MSCI"))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Connection = con;
                                cmd.Parameters.AddWithValue("@insMSCI", msciDt);
                                con.Open();
                                cmd.ExecuteNonQuery();
                                con.Close();
                            }
                        }
                        return 0; //更新成功
                    }                   
                }//checkdate
                return 1; //尚未更新
            }

            

            return 2; //回傳資料錯誤

        }

        private int getfgbuy()
        {
            string Url = "https://tw.stock.yahoo.com/d/i/fgbuy_tse100.html";

            string strWebContent = GetWebContentBig5(Url);

            if (strWebContent != "")
            {
                //int iBodyStart = strWebContent.IndexOf("<body", 0);
                int iStart = strWebContent.IndexOf("content start", 0);

                int iTableStart = strWebContent.IndexOf("<table", iStart);
                int iTableEnd = strWebContent.IndexOf("</table>", iStart);

                int iEnd = strWebContent.IndexOf("content end", iStart);

                int iLastTableStart = strWebContent.IndexOf("<table", iTableEnd);
                int iLastTableEnd = strWebContent.IndexOf("</table>", iLastTableStart);

                int iLLastTableStart = strWebContent.IndexOf("<table", iLastTableEnd);
                int iLLastTableEnd = strWebContent.IndexOf("</table>", iLLastTableStart);

                int iDateStart = strWebContent.IndexOf("資料日期：", iStart);
                int iDateEnd = strWebContent.IndexOf("</td>", iDateStart);

                string updateDate = strWebContent.Substring(iDateStart + 5, iDateEnd - iDateStart - 5);



                //textBox2.Text = checkRepublicEraDate(updateDate).ToString();

                if (checkRepublicEraDate(updateDate))
                {
                    string strWeb = strWebContent.Substring(iLLastTableStart, iLLastTableEnd - iLLastTableStart);


                    WebBrowser webb = new WebBrowser();

                    webb.Navigate("about:blank");

                    HtmlDocument htmldoc = webb.Document.OpenNew(true);

                    htmldoc.Write(strWeb);

                    HtmlElementCollection htmlTR = htmldoc.GetElementsByTagName("tr");

                    int count = 0;

                    ArrayList tempData = new ArrayList();

                    DataTable fgbuyDt = new DataTable();

                    DataRow fgbuyRow;

                    fgbuyDt.Columns.Add("fgbuy_ID", typeof(String));
                    fgbuyDt.Columns.Add("Date", typeof(String));
                    fgbuyDt.Columns.Add("Rank", typeof(String));
                    fgbuyDt.Columns.Add("StockID", typeof(String));
                    fgbuyDt.Columns.Add("StockName", typeof(String));
                    fgbuyDt.Columns.Add("StrikePrice", typeof(String));
                    fgbuyDt.Columns.Add("UpAndDown", typeof(String));
                    fgbuyDt.Columns.Add("OverboughtCount", typeof(String));
                    fgbuyDt.Columns.Add("ForeignHoldingCount", typeof(String));
                    fgbuyDt.Columns.Add("ForeignHoldingWeight", typeof(Double));

                    foreach (HtmlElement tr in htmlTR)
                    {
                        HtmlElementCollection htmlTD = tr.GetElementsByTagName("TD");

                        foreach (HtmlElement td in htmlTD)
                        {
                            tempData.Add(td.InnerText);
                        }
                        if (count > 0)
                        {

                            fgbuyRow = fgbuyDt.NewRow();
                            fgbuyRow["fgbuy_ID"] = currentDate + tempData[1].ToString().Substring(0, 4);
                            fgbuyRow["Date"] = currentDate;
                            fgbuyRow["Rank"] = tempData[0];
                            fgbuyRow["StockID"] = tempData[1].ToString().Substring(0, 4);
                            fgbuyRow["StockName"] = tempData[1].ToString().Substring(4);

                            

                            if (tempData[2].ToString() == "---")
                            {
                                tempData[2] = null;

                                
                                //Console.WriteLine(tempData[2].ToString());
                            }
                            else
                            {
                                tempData[2] = tempData[2].ToString().Replace(",","");
  
                            }

                            fgbuyRow["StrikePrice"] = tempData[2];

                            //Console.WriteLine(fgbuyRow["StrikePrice"]);
                            if (tempData[3].ToString()[0] == '△' || tempData[3].ToString()[0] == '▲')
                            {
                                tempData[3] = tempData[3].ToString().Replace("△", "").Replace("▲", "").Replace(",", "");
                                //Console.WriteLine( tempData[3].ToString().Replace("△", "").Replace("▲", "").Replace(",",""));
                                

                            }
                            else if (tempData[3].ToString()[0] == '▽' || tempData[3].ToString()[0] == '▼')
                            {
                                tempData[3] = tempData[3].ToString().Replace("▽", "-").Replace("▼", "-").Replace(",", "");
                                //Console.WriteLine(tempData[3].ToString().Replace("▽", "-").Replace("▼", "-").Replace(",", ""));

                            }
                            else if (tempData[3] == "---")
                            {
                                tempData[3] = null;

                                //Console.WriteLine(tempData[3].ToString());

                            }
                            else
                            {
                                tempData[3] = 0;
                            }

                            //Console.WriteLine(fgbuyRow["UpAndDown"]);


                            fgbuyRow["UpAndDown"] = tempData[3];
                            fgbuyRow["OverboughtCount"] = tempData[4];
                            fgbuyRow["ForeignHoldingCount"] = tempData[5];
                            fgbuyRow["ForeignHoldingWeight"] = tempData[6].ToString().Substring(0, tempData[6].ToString().Length - 1);

                            fgbuyDt.Rows.Add(fgbuyRow);

                        }


                        count++;
                        tempData.Clear();

                    }

                    if (fgbuyDt.Rows.Count > 0)
                    {
                        string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
                        using (SqlConnection con = new SqlConnection(consString))
                        {
                            using (SqlCommand cmd = new SqlCommand("Insert_FGBuy"))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Connection = con;
                                cmd.Parameters.AddWithValue("@insFGBuy", fgbuyDt);
                                con.Open();
                                cmd.ExecuteNonQuery();
                                con.Close();
                            }
                        }
                        return 0 ;
                    }               

                }//checkdate
                return 1;
            }
            

            return 2;

        }

        private int getfgsell()
        {
            string Url = "https://tw.stock.yahoo.com/d/i/fgsell_tse100.html";

            string strWebContent = GetWebContentBig5(Url);

            if (strWebContent != "")
            {
                //int iBodyStart = strWebContent.IndexOf("<body", 0);
                int iStart = strWebContent.IndexOf("content start", 0);

                int iTableStart = strWebContent.IndexOf("<table", iStart);
                int iTableEnd = strWebContent.IndexOf("</table>", iStart);

                int iEnd = strWebContent.IndexOf("content end", iStart);

                int iLastTableStart = strWebContent.IndexOf("<table", iTableEnd);
                int iLastTableEnd = strWebContent.IndexOf("</table>", iLastTableStart);

                int iLLastTableStart = strWebContent.IndexOf("<table", iLastTableEnd);
                int iLLastTableEnd = strWebContent.IndexOf("</table>", iLLastTableStart);

                int iDateStart = strWebContent.IndexOf("資料日期：", iStart);
                int iDateEnd = strWebContent.IndexOf("</td>", iDateStart);

                string updateDate = strWebContent.Substring(iDateStart + 5, iDateEnd - iDateStart - 5);

                //textBox2.Text = checkRepublicEraDate(updateDate).ToString();

                if (checkRepublicEraDate(updateDate))
                {
                    string strWeb = strWebContent.Substring(iLLastTableStart, iLLastTableEnd - iLLastTableStart);


                    WebBrowser webb = new WebBrowser();

                    webb.Navigate("about:blank");

                    HtmlDocument htmldoc = webb.Document.OpenNew(true);

                    htmldoc.Write(strWeb);

                    HtmlElementCollection htmlTR = htmldoc.GetElementsByTagName("tr");

                    int count = 0;

                    ArrayList tempData = new ArrayList();

                    DataTable fgsellDt = new DataTable();

                    DataRow fgsellRow;

                    fgsellDt.Columns.Add("fgsell_ID", typeof(String));
                    fgsellDt.Columns.Add("Date", typeof(String));
                    fgsellDt.Columns.Add("Rank", typeof(String));
                    fgsellDt.Columns.Add("StockID", typeof(String));
                    fgsellDt.Columns.Add("StockName", typeof(String));
                    fgsellDt.Columns.Add("StrikePrice", typeof(String));
                    fgsellDt.Columns.Add("UpAndDown", typeof(String));
                    fgsellDt.Columns.Add("OversoldCount", typeof(String));
                    fgsellDt.Columns.Add("ForeignHoldingCount", typeof(String));
                    fgsellDt.Columns.Add("ForeignHoldingWeight", typeof(Double));

                    foreach (HtmlElement tr in htmlTR)
                    {
                        HtmlElementCollection htmlTD = tr.GetElementsByTagName("TD");

                        foreach (HtmlElement td in htmlTD)
                        {
                            tempData.Add(td.InnerText);
                        }
                        if (count > 0)
                        {
                            fgsellRow = fgsellDt.NewRow();
                            fgsellRow["fgsell_ID"] = currentDate + tempData[1].ToString().Substring(0, 4);
                            fgsellRow["Date"] = currentDate;
                            fgsellRow["Rank"] = tempData[0];
                            fgsellRow["StockID"] = tempData[1].ToString().Substring(0, 4);
                            fgsellRow["StockName"] = tempData[1].ToString().Substring(4);

                            if (tempData[2].ToString() == "---")
                            {
                                tempData[2] = null;
                            }
                            else
                            {
                                tempData[2] = tempData[2].ToString().Replace(",", "");

                            }

                            fgsellRow["StrikePrice"] = tempData[2];

                            if (tempData[3].ToString()[0] == '△' || tempData[3].ToString()[0] == '▲')
                            {
                                tempData[3] = tempData[3].ToString().Replace("△", "").Replace("▲", "").Replace(",","");
                            }
                            else if (tempData[3].ToString()[0] == '▽' || tempData[3].ToString()[0] == '▼')
                            {
                                tempData[3] = tempData[3].ToString().Replace("▽", "-").Replace("▼", "-").Replace(",","");
                            }
                            else if (tempData[3] == "---")
                            {
                                tempData[3] = null;

                                //Console.WriteLine(tempData[3].ToString());

                            }
                            else
                            {
                                tempData[3] = 0;
                            }

                            fgsellRow["UpAndDown"] = tempData[3];
                            fgsellRow["OversoldCount"] = tempData[4];
                            fgsellRow["ForeignHoldingCount"] = tempData[5];
                            fgsellRow["ForeignHoldingWeight"] = tempData[6].ToString().Substring(0, tempData[6].ToString().Length - 1);

                            fgsellDt.Rows.Add(fgsellRow);
                        }


                        count++;
                        tempData.Clear();

                    }

                    if (fgsellDt.Rows.Count > 0)
                    {
                        string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
                        using (SqlConnection con = new SqlConnection(consString))
                        {
                            using (SqlCommand cmd = new SqlCommand("Insert_FGSell"))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Connection = con;
                                cmd.Parameters.AddWithValue("@insFGSell", fgsellDt);
                                con.Open();
                                cmd.ExecuteNonQuery();
                                con.Close();
                            }
                        }
                        return 0;
                    }                   
                }//checkdate
                return 1;
            }

            return 2;
        }

        private bool getTaiEx()
        {
            string currentDate = DateTime.Now.ToString("yyyyMMdd");

            string Url = "http://www.twse.com.tw/exchangeReport/MI_INDEX?response=html&type=MS&date=" + currentDate;

            string strWebContent = GetWebContent(Url);
                       
            int iTdStart = strWebContent.IndexOf("發行量加權股價指數", 0);

            if (iTdStart > 0)
            {
                int iTdEnd = strWebContent.IndexOf("</td>", iTdStart);
                int TaiExStart = strWebContent.IndexOf("<td>", iTdEnd);
                int TaiExEnd = strWebContent.IndexOf("</td>", TaiExStart);

                string result = strWebContent.Substring(TaiExStart + 4, TaiExEnd - TaiExStart - 4);

                double tempTaiEx = double.Parse(result.Replace(",", ""));

                DataTable TaiExDt = new DataTable();

                TaiExDt.Columns.Add("Date",typeof(String));
                TaiExDt.Columns.Add("TaiEx", typeof(Decimal));

                TaiExDt.Rows.Add(currentDate , tempTaiEx);

                if (TaiExDt.Rows.Count > 0)
                {
                    string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
                    using (SqlConnection con = new SqlConnection(consString))
                    {
                        using (SqlCommand cmd = new SqlCommand("Insert_TaiEx"))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Connection = con;
                            cmd.Parameters.AddWithValue("@insTAIEX", TaiExDt);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                }

                return true;
            }

            return false;
            
        }


        private bool getNewT86()
        {
            string date = DateTime.Now.ToString("yyyyMMdd");

            //string date = "20171220";

            string Url = "http://www.tse.com.tw/fund/T86?response=csv&date=" + date + "&selectType=ALLBUT0999";

            HttpWebRequest T86Request = (HttpWebRequest)WebRequest.Create(Url);
            HttpWebResponse T86Response = (HttpWebResponse)T86Request.GetResponse();

            Encoding encoding = Encoding.GetEncoding("big5");

            StreamReader T86StreamReader = new StreamReader(T86Response.GetResponseStream(), encoding);
            string result = T86StreamReader.ReadToEnd();

            if(result.Contains("年") && result.Contains("月") && result.Contains("日"))
            {
                string[] rows = result.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

                string test = "";

                DataTable dt = new DataTable();

                dt.Columns.Add("InsID");
                dt.Columns.Add("Date");
                dt.Columns.Add("StockID");
                dt.Columns.Add("StockName");
                dt.Columns.Add("FGTotal");
                dt.Columns.Add("DomTotal");
                dt.Columns.Add("DealerTotalSelf");
                dt.Columns.Add("DealerTatalHedge");
                dt.Columns.Add("InsTotal");

                DataRow dr;

                foreach (string row in rows)
                {

                    string[] cell = row.Split(new string[] { "\",\"" }, StringSplitOptions.None);
                    cell[0] = cell[0].Replace("\"", "");
                    cell[cell.Length - 1] = cell[cell.Length - 1].Replace("\",", "");

                    int n;
                    if(cell.Count<string>() >= 16 && int.TryParse(cell[0],out n) && cell[0].Length < 5)
                    {
                        dr = dt.NewRow();
                        dr["InsID"] = date + cell[0];
                        dr["Date"] = date;
                        dr["StockID"] = cell[0];
                        dr["StockName"] = cell[1];
                        dr["FGTotal"] = (Convert.ToInt32(cell[4].Replace(",","")) + Convert.ToInt32(cell[7].Replace(",", ""))).ToString("N0");
                        dr["DomTotal"] = cell[10];
                        dr["DealerTotalSelf"] = cell[14];
                        dr["DealerTatalHedge"] = cell[17];
                        dr["InsTotal"] = cell[18];

                        //Console.WriteLine(dr["InsID"] + " " + dr["Date"] + " " + dr["StockID"] + " " + dr["StockName"] + dr["FGTotal"]);

                        dt.Rows.Add(dr);
                        //for (int i = 0; i < 9; i++)
                        //{
                        //    Console.WriteLine(dt.Rows.ToString());
                        //}

                    }          
                
                }

                string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
                using (SqlConnection con = new SqlConnection(consString))
                {
                    using (SqlCommand cmd = new SqlCommand("Insert_InsDailyReport"))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = con;
                        cmd.Parameters.AddWithValue("@insInsDailyReport", dt);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
                return true; //更新成功
            }
            
                        
            return false;

        }


        private bool getT86()
        {
            string date = DateTime.Now.ToString("yyyyMMdd");

            //string date = "20171220";

            string Url = "http://www.tse.com.tw/fund/T86?response=csv&date=" + date + "&selectType=ALLBUT0999";

            HttpWebRequest T86Request = (HttpWebRequest)WebRequest.Create(Url);
            HttpWebResponse T86Response = (HttpWebResponse)T86Request.GetResponse();

            Encoding encoding = Encoding.GetEncoding("big5");

            StreamReader T86StreamReader = new StreamReader(T86Response.GetResponseStream(), encoding);
            string result = T86StreamReader.ReadToEnd();

            if (result.Contains("年") && result.Contains("月") && result.Contains("日"))
            {
                string[] rows = result.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

                string test = "";

                DataTable dt = new DataTable();

                dt.Columns.Add("InsID");
                dt.Columns.Add("Date");
                dt.Columns.Add("StockID");
                dt.Columns.Add("StockName");
                dt.Columns.Add("FGTotal");
                dt.Columns.Add("DomTotal");
                dt.Columns.Add("DealerTotalSelf");
                dt.Columns.Add("DealerTatalHedge");
                dt.Columns.Add("InsTotal");

                DataRow dr;

                foreach (string row in rows)
                {

                    string[] cell = row.Split(new string[] { "\",\"" }, StringSplitOptions.None);
                    cell[0] = cell[0].Replace("\"", "");
                    cell[cell.Length - 1] = cell[cell.Length - 1].Replace("\",", "");

                    int n;
                    if (cell.Count<string>() >= 16 && int.TryParse(cell[0], out n) && cell[0].Length < 5)
                    {
                        dr = dt.NewRow();
                        dr["InsID"] = date + cell[0];
                        dr["Date"] = date;
                        dr["StockID"] = cell[0];
                        dr["StockName"] = cell[1];
                        dr["FGTotal"] = cell[4];
                        dr["DomTotal"] = cell[7];
                        dr["DealerTotalSelf"] = cell[11];
                        dr["DealerTatalHedge"] = cell[14];
                        dr["InsTotal"] = cell[15];

                        //Console.WriteLine(dr["InsID"] + " " + dr["Date"] + " " + dr["StockID"] + " " + dr["StockName"] + dr["FGTotal"] + " " + dr["DomTotal"] + dr["DealerTotalSelf"] + dr["DealerTatalHedge"] + dr["InsTotal"]);

                        dt.Rows.Add(dr);
                        //for (int i = 0; i < 9; i++)
                        //{
                        //    Console.WriteLine(dt.Rows.ToString());
                        //}

                    }

                }

                string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
                using (SqlConnection con = new SqlConnection(consString))
                {
                    using (SqlCommand cmd = new SqlCommand("Insert_InsDailyReport"))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = con;
                        cmd.Parameters.AddWithValue("@insInsDailyReport", dt);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
                return true; //更新成功
            }


            return false;

        }

    }
}
